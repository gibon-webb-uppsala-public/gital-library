/*
 * WEBPACK CONFIG
 *
 * Author : Gustav Gesar
 * Version: 3.0.0
 *
 */

// Plugins
const MiniCssExtractPlugin = require( 'mini-css-extract-plugin' );
const BrowserSyncPlugin = require( 'browser-sync-webpack-plugin' );
const RemoveEmptyScriptsPlugin = require( 'webpack-remove-empty-scripts' );
const path = require( 'path' );
require( 'dotenv' ).config();

// Paths
const paths = {
	styles: path.resolve( '/assets/styles' ),
	scripts: path.resolve( '/assets/scripts' ),
	node_modules: path.resolve( '/node_modules' ),
};

// Config
module.exports = ( env, argv ) => {
	let config = {
		entry: {
			// Styles
			[`${paths.styles}/gital.library`]: [ `${paths.styles}/src/gital.library.scss` ],
			[`${paths.styles}/gital.library.admin`]: [ `${paths.styles}/src/gital.library.admin.scss` ],
			[`${paths.styles}/gital.library.login`]: [ `${paths.styles}/src/gital.library.login.scss` ],

			// Scripts
			[`${paths.scripts}/gital.library`]: [ `${paths.scripts}/src/gital.library.js` ],
			[`${paths.scripts}/gital.library.google-maps`]: [ `${paths.scripts}/src/gital.library.google-maps.js` ],
			[`${paths.scripts}/gital.library.login`]: [ `${paths.scripts}/src/gital.library.login.js` ],
		},
		output: {
			path: path.resolve( __dirname ),
			filename: '[name].min.js',
			sourceMapFilename: '[file].map',
		},
		module: {
			rules: [
				{
					// JS
					test: /\.js$/,
					use: {
						loader: 'babel-loader',
						options: {
							presets: [ '@babel/preset-env' ],
						},
					},
				},
				{
					// CSS
					test: /\.css$/,

					// Loaders are applying from bottom to top
					use: [
						{
							// Get all transformed CSS and extracts it into separate single bundled file
							loader: MiniCssExtractPlugin.loader,
						},
						{
							// Resolves url() and @imports inside CSS
							loader: 'css-loader',
							options: {
								sourceMap: true,
								url: false,
							},
						},
					],
				},
				{
					// Sass
					test: /\.(sa|sc)ss$/,

					// Loaders are applying from bottom to top
					use: [
						{
							// Get all transformed CSS and extracts it into separate single bundled file
							loader: MiniCssExtractPlugin.loader,
						},
						{
							// Resolves url() and @imports inside CSS
							loader: 'css-loader',
							options: {
								sourceMap: true,
								url: false,
							},
						},
						{
							// Apply postCSS fixes like autoprefixer
							loader: 'postcss-loader',
							options: {
								sourceMap: true,
								postcssOptions: {
									plugins: [
										require( 'autoprefixer' )( {
											grid: false,
											flexbox: false,
										} ),
									],
								},
							},
						},
						{
							// Complie SASS to standard CSS
							loader: 'sass-loader',
							options: {
								implementation: require( 'sass' ),
								sourceMap: true,
							},
						},
					],
				},
				{
					// SVG
					test: /\.(svg)$/,
					type: 'asset/inline',
				},
			],
		},
		plugins: [
			new RemoveEmptyScriptsPlugin(),
			new MiniCssExtractPlugin( {
				filename: '[name].min.css',
			} ),
			new BrowserSyncPlugin( {
				host: 'localhost',
				port: 3000,
				proxy: process.env.WP_HOME,
			} ),
		],
		optimization: {
			removeAvailableModules: false,
			removeEmptyChunks: false,
			splitChunks: false,
		},
		resolve: {
			symlinks: false,
		},
		stats: 'minimal',
	};

	if ( argv.mode === 'development' ) {
		config.devtool = 'source-map';
	}
	return config;
};
