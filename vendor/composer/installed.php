<?php return array(
    'root' => array(
        'name' => 'gibon-webb-uppsala-public/gital-library',
        'pretty_version' => '3.16.1',
        'version' => '3.16.1.0',
        'reference' => null,
        'type' => 'wordpress-plugin',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'dev' => true,
    ),
    'versions' => array(
        'dealerdirect/phpcodesniffer-composer-installer' => array(
            'pretty_version' => 'v1.0.0',
            'version' => '1.0.0.0',
            'reference' => '4be43904336affa5c2f70744a348312336afd0da',
            'type' => 'composer-plugin',
            'install_path' => __DIR__ . '/../dealerdirect/phpcodesniffer-composer-installer',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
        'gibon-webb-uppsala-public/gital-library' => array(
            'pretty_version' => '3.16.1',
            'version' => '3.16.1.0',
            'reference' => null,
            'type' => 'wordpress-plugin',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'phpcompatibility/php-compatibility' => array(
            'pretty_version' => '9.3.5',
            'version' => '9.3.5.0',
            'reference' => '9fb324479acf6f39452e0655d2429cc0d3914243',
            'type' => 'phpcodesniffer-standard',
            'install_path' => __DIR__ . '/../phpcompatibility/php-compatibility',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
        'phpcompatibility/phpcompatibility-paragonie' => array(
            'pretty_version' => '1.3.3',
            'version' => '1.3.3.0',
            'reference' => '293975b465e0e709b571cbf0c957c6c0a7b9a2ac',
            'type' => 'phpcodesniffer-standard',
            'install_path' => __DIR__ . '/../phpcompatibility/phpcompatibility-paragonie',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
        'phpcompatibility/phpcompatibility-wp' => array(
            'pretty_version' => '2.1.5',
            'version' => '2.1.5.0',
            'reference' => '01c1ff2704a58e46f0cb1ca9d06aee07b3589082',
            'type' => 'phpcodesniffer-standard',
            'install_path' => __DIR__ . '/../phpcompatibility/phpcompatibility-wp',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
        'phpcsstandards/phpcsextra' => array(
            'pretty_version' => '1.2.1',
            'version' => '1.2.1.0',
            'reference' => '11d387c6642b6e4acaf0bd9bf5203b8cca1ec489',
            'type' => 'phpcodesniffer-standard',
            'install_path' => __DIR__ . '/../phpcsstandards/phpcsextra',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
        'phpcsstandards/phpcsutils' => array(
            'pretty_version' => '1.0.12',
            'version' => '1.0.12.0',
            'reference' => '87b233b00daf83fb70f40c9a28692be017ea7c6c',
            'type' => 'phpcodesniffer-standard',
            'install_path' => __DIR__ . '/../phpcsstandards/phpcsutils',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
        'squizlabs/php_codesniffer' => array(
            'pretty_version' => '3.10.2',
            'version' => '3.10.2.0',
            'reference' => '86e5f5dd9a840c46810ebe5ff1885581c42a3017',
            'type' => 'library',
            'install_path' => __DIR__ . '/../squizlabs/php_codesniffer',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
        'symfony/polyfill-php80' => array(
            'pretty_version' => 'v1.30.0',
            'version' => '1.30.0.0',
            'reference' => '77fa7995ac1b21ab60769b7323d600a991a90433',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-php80',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
        'symfony/polyfill-php81' => array(
            'pretty_version' => 'v1.30.0',
            'version' => '1.30.0.0',
            'reference' => '3fb075789fb91f9ad9af537c4012d523085bd5af',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-php81',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
        'symfony/polyfill-php82' => array(
            'pretty_version' => 'v1.30.0',
            'version' => '1.30.0.0',
            'reference' => '77ff49780f56906788a88974867ed68bc49fae5b',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-php82',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
        'wp-coding-standards/wpcs' => array(
            'pretty_version' => '3.1.0',
            'version' => '3.1.0.0',
            'reference' => '9333efcbff231f10dfd9c56bb7b65818b4733ca7',
            'type' => 'phpcodesniffer-standard',
            'install_path' => __DIR__ . '/../wp-coding-standards/wpcs',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
        'yahnis-elsts/plugin-update-checker' => array(
            'pretty_version' => 'v5.4',
            'version' => '5.4.0.0',
            'reference' => 'e8e53e6d98e37fa7c895c93417f52e3775494715',
            'type' => 'library',
            'install_path' => __DIR__ . '/../yahnis-elsts/plugin-update-checker',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
    ),
);
