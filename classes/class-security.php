<?php
/**
 * Security
 *
 * @package Gital Library
 */

namespace gital_library;

if ( ! class_exists( 'Security' ) ) {
	/**
	 * Security
	 *
	 * Adds security functions
	 *
	 * @author Gustav Gesar <gustav.gesar@gibon.se>
	 *
	 * @version 1.2.2
	 * @since 2.14.0
	 */
	class Security extends Singleton {
		/**
		 * Settings
		 *
		 * @var object $settings Settings object.
		 */
		protected $settings;

		public function init() {
			$this->settings = Settings::get_instance();

			if ( ! $this->settings->get( 'disable_authenticate_users_in_rest' ) ) {
				add_filter( 'rest_pre_dispatch', array( $this, 'authenticate_users_in_rest' ), 10, 3 );
			}
			if ( ! $this->settings->get( 'disable_generator_removal' ) ) {
				remove_action( 'wp_head', 'wp_generator' );
				if ( is_wc_activated() ) {
					remove_action( 'wp_head', 'wc_generator' );
				}
			}
			if ( $this->settings->get( 'disallow_file_mods_for_non_super_admins' ) ) {
				add_filter( 'file_mod_allowed', array( $this, 'disallow_file_mods_for_non_super_admins' ), 10, 2 );
			}
			if ( ! $this->settings->get( 'disable_sanitize_filename' ) ) {
				add_filter( 'sanitize_file_name', 'gital_library\sanitize_file_name', 10, 1 );
			}
		}

		/**
		 * Disallow file mods for non super admins
		 *
		 * @param bool   $file_mods_allowed The current state.
		 * @param string $context The current context.
		 *
		 * @return bool The new state.
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.0.0
		 * @since 2.37.0
		 */
		public function disallow_file_mods_for_non_super_admins( $file_mods_allowed, $context ) {
			if ( is_super_admin() ) {
				return true;
			}

			return false;
		}

		/**
		 * Authenticate users in REST API
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @source https://wildwolf.name/how-to-restrict-access-to-user-rest-api-in-wordpress
		 *
		 * @version 1.0.0
		 * @since 2.14.0
		 */
		public function authenticate_users_in_rest( $result, \WP_REST_Server $srv, \WP_REST_Request $request ) {
			$method = $request->get_method();
			$path   = $request->get_route();
			if ( ( 'GET' === $method || 'HEAD' === $method ) && preg_match( '!^/wp/v2/users(?:$|/)!i', $path ) ) {
				if ( ! current_user_can( 'list_users' ) ) {
					return new \WP_Error( 'rest_forbidden', __( 'Sorry, you are not allowed to do that.' ), array( 'status' => rest_authorization_required_code() ) );
				}
			}
			return $result;
		}
	}
}
