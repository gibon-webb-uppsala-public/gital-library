<?php
/**
 * Support
 *
 * @package Gital Library
 */

namespace gital_library;

if ( ! class_exists( 'Support' ) ) {
	/**
	 * Support
	 *
	 * Adds the support page
	 *
	 * @author Gustav Gesar <gustav.gesar@gibon.se>
	 *
	 * @version 1.1.0
	 * @since 1.6.0
	 */
	class Support extends Singleton {
		/**
		 * Settings
		 *
		 * @var object $settings Settings object.
		 */
		protected $settings;

		public function init() {
			$this->settings = Settings::get_instance();

			add_action( 'admin_menu', array( $this, 'support_page_register' ) );
		}

		/**
		 * Register support page
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.0.0
		 * @since 1.2.0
		 */
		public function support_page_register() {
			add_menu_page(
				__( 'Support', 'gital-library' ),
				__( 'Support', 'gital-library' ),
				'manage_options',
				'gibon-webb-support',
				array( $this, 'support_page_render' ),
				'dashicons-format-chat',
				3
			);
		}

		/**
		 * Render the support page
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.2.0
		 * @since 1.2.0
		 */
		public function support_page_render() {
			require_once $this->settings->get( 'path_views' ) . 'support-form.php';

			if ( isset( $_POST['support'] ) ) {
				if ( ! is_email( $_POST['email'] ) ) {
					wp_safe_redirect( the_permalink() . '?page=gibon-webb-support&success=false', 301 );
					exit;
				}

				$to           = 'webb@gibon.se';
				$subject      = 'Nytt supportärende från ' . get_bloginfo( 'name' );
				$content      = 'Från: ' . esc_html( $_POST['name'] ) . ' [' . $_POST['email'] . ']' . "\r\n\r\n" . 'Kunden vill att vi hanterar detta såhär: ' . $_POST['handle'] . "\r\n\r\n" . esc_html( $_POST['description'] );
				$referral_url = str_replace( array( 'http://', 'https://', '/wp', 'www.' ), '', get_bloginfo( 'wpurl' ) );
				$headers      = 'From: ' . get_bloginfo( 'name' ) . ' <noreply@' . $referral_url . '>' . "\r\n" . 'Reply-To: ' . $_POST['email'] . "\r\n";
				$headers     .= 'MIME-Version: 1.0' . "\r\n";
				$headers     .= 'Content-Type: text/plain; charset=UTF-8' . "\r\n";

				if ( wp_mail( $to, $subject, $content, $headers ) ) {
					wp_safe_redirect( the_permalink() . '?page=gibon-webb-support&success=true', 301 );
					exit;
				} else {
					wp_safe_redirect( the_permalink() . '?page=gibon-webb-support&success=false', 301 );
					exit;
				}
			}
		}
	}
}
