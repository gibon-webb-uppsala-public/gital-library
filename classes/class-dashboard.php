<?php
/**
 * Dashboard
 *
 * @package Gital Library
 */

namespace gital_library;

if ( ! class_exists( 'Dashboard' ) ) {
	/**
	 * Dashboard
	 *
	 * Adds a dashboard widget and cleans up the default widgets
	 *
	 * @author Gustav Gesar <gustav.gesar@gibon.se>
	 *
	 * @version 1.0.0
	 * @since 1.6.0
	 */
	class Dashboard extends Singleton {
		/**
		 * Settings
		 *
		 * @var object $settings Settings object.
		 */
		protected $settings;

		/**
		 * The managed hosting instance.
		 *
		 * @var array
		 */
		private object $managed_hosting;

		public function init() {
			$this->settings        = Settings::get_instance();
			$this->managed_hosting = Managed_Hosting::get_instance();
			add_action( 'wp_dashboard_setup', array( $this, 'dashboard_widget' ), 20 );
			add_action( 'wp_dashboard_setup', array( $this, 'control_managed_hosting' ) );
		}

		/**
		 * Register Gibon Webb Widget and remove clutter widgets
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.0.0
		 * @since 1.2.0
		 */
		public function dashboard_widget() {
			if ( ! $this->settings->get( 'show_widgets' ) ) {
				// Remove Welcome panel.
				remove_action( 'welcome_panel', 'wp_welcome_panel' );

				// Remove all Dashboard widgets.
				global $wp_meta_boxes;
				unset( $wp_meta_boxes['dashboard'] );
			}

			// Add custom dashbboard widget.
			add_meta_box(
				'gital_dashboard_widget',
				__( 'Welcome to admin', 'gital-library' ),
				array( $this, 'dashboard_widget_content' ),
				'dashboard',
				'normal',
				'high'
			);
		}

		/**
		 * Define the widget content
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.3.0
		 * @since 1.2.0
		 */
		public function dashboard_widget_content() {
			$content = '<p>' . __( 'Welcome to the backend of your site. If you need any help, do not hesitate to contact us.', 'gital-library' ) . '</p>';
			if ( ! $this->managed_hosting->control_if_disabled() ) {
				if ( is_allowed_server() ) {
					$content .= '<p class="g-lib-mh-status"><strong>' . __( 'Managed Hosting status', 'gital-library' ) . ': </strong><span class="g-lib-mh-status__icon g-lib-mh-status__icon--active"></span></p>';
					if ( get_option( 'g_lib_mh_updated_date' ) && get_option( 'g_lib_mh_updated_name' ) ) {
						$content .= '<p>' . __( 'Last managed', 'gital-library' ) . ' ' . date( 'Y.m.d', get_option( 'g_lib_mh_updated_date' ) ) . ' ' . __( 'by', 'gital-library' ) . ' ' . get_option( 'g_lib_mh_updated_name' ) . '</p>';
					}
					$storage  = Storage::get_instance();
					$content .= $storage->get_html();
				} else {
					$content = '<p class="g-lib-mh-status"><strong>' . __( 'Managed Hosting status', 'gital-library' ) . ': </strong><span class="g-lib-mh-status__icon g-lib-mh-status__icon--inactive"></span></p>';
				}
				$content .= '<hr>';
			}

			$content .= '<p>' . __( 'E-Mail:', 'gital-library' ) . ' ' . $this->settings->get( 'contact_email' )
				. '<br>' . __( 'Phone:', 'gital-library' ) . ' ' . $this->settings->get( 'contact_phone' );

			$referral_url = str_replace( array( 'http://', 'https://', '/wp' ), '', get_bloginfo( 'wpurl' ) );
			$support_url  = get_admin_url( null, 'admin.php?page=gibon-webb-support' );
			$contact_url  = 'https://www.gibon.se/vara-kontor/gibon-uppsala/?utm_source=' . $referral_url . '&utm_medium=referral';

			$button = new Button( __( 'Support', 'gital-library' ), '', 'g-button--first', $support_url, false, 'white', 'standard' );
			$button->add_button( __( 'Contact us', 'gital-library' ), '', 'g-button--second', $contact_url, true, 'white', 'standard' );
			$button->add_wrapper( 'left', 'g-margin-top' );
			$content .= $button->get_button();

			$content .= '<img src="' . $this->settings->get( 'url_gibon_logo' ) . '">';

			echo $content;
		}

		/**
		 * Control managed hosting
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.1.0
		 * @since 1.19.0
		 */
		public function control_managed_hosting() {
			if ( ! $this->managed_hosting->control_if_disabled() && ! is_allowed_server() ) {
				add_action( 'admin_notices', array( $this, 'render_notice' ) );
			}
		}

		/**
		 * Render the manage hosting notice
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.0.0
		 * @since 1.19.0
		 */
		public function render_notice() {
			echo $this->managed_hosting->render_notice();
		}
	}
}
