<?php
/**
 * Patches
 *
 * @package Gital Library
 */

namespace gital_library;

if ( ! class_exists( 'Patches' ) ) {
	/**
	 * Patches
	 *
	 * @author Gustav Gesar <gustav.gesar@gibon.se>
	 *
	 * @version 1.0.0
	 * @since 3.45.0
	 */
	class Patches extends Singleton {
		/**
		 * The settings object
		 *
		 * @var Object
		 */
		private object $settings;

		public function init() {
			$this->settings = Settings::get_instance();

			if ( $this->settings->get( 'correct_theme' ) ) {
				if ( version_compare( $this->settings->get( 'theme_version' ), '3.38.0', '<=' ) ) {
					add_action( 'wp_enqueue_scripts', array( $this, 'patch_t_3_38_0' ) );
				}
				if ( version_compare( $this->settings->get( 'theme_version' ), '3.45.0', '<=' ) ) {
					add_action( 'wp_enqueue_scripts', array( $this, 'patch_t_3_45_0' ) );
				}
				if ( version_compare( $this->settings->get( 'theme_version' ), '3.46.0', '<=' ) ) {
					add_action( 'wp_enqueue_scripts', array( $this, 'patch_t_3_46_0' ) );
				}
			}

			if ( $this->settings->get( 'blocks_installed' ) ) {
				if ( version_compare( $this->settings->get( 'blocks_version' ), '3.18.0', '<=' ) ) {
					add_action( 'wp_enqueue_scripts', array( $this, 'patch_b_3_18_0' ) );
				}
			}
		}

		public function patch_t_3_38_0() {
			wp_register_style( 'g_lib_patch_t_3_38_0', $this->settings->get( 'url_patches' ) . '/gital.library.patch_t_3_38_0.min.css', array(), '1.0.0' );
			wp_enqueue_style( 'g_lib_patch_t_3_38_0' );
		}

		public function patch_t_3_45_0() {
			wp_register_style( 'g_lib_patch_t_3_45_0', $this->settings->get( 'url_patches' ) . '/gital.library.patch_t_3_45_0.min.css', array(), '1.0.0' );
			wp_enqueue_style( 'g_lib_patch_t_3_45_0' );
		}

		public function patch_t_3_46_0() {
			wp_register_style( 'g_lib_patch_t_3_46_0', $this->settings->get( 'url_patches' ) . '/gital.library.patch_t_3_46_0.min.css', array(), '1.0.0' );
			wp_enqueue_style( 'g_lib_patch_t_3_46_0' );
		}

		public function patch_b_3_18_0() {
			wp_register_style( 'g_lib_patch_b_3_18_0', $this->settings->get( 'url_patches' ) . '/gital.library.patch_b_3_18_0.min.css', array(), '1.0.0' );
			wp_enqueue_style( 'g_lib_patch_b_3_18_0' );
		}
	}
}
