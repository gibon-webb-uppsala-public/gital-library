<?php
/**
 * Google Maps
 *
 * @package Gital Library
 */

namespace gital_library;

if ( ! class_exists( 'Google_Maps' ) ) {
	/**
	 * Google_Maps
	 *
	 * Adds Google Maps functionality
	 *
	 * @author Gustav Gesar <gustav.gesar@gibon.se>
	 *
	 * @version 2.0.2
	 * @since 1.6.0
	 */
	class Google_Maps extends Singleton {
		/**
		 * Settings
		 *
		 * @var object $settings Settings object.
		 */
		protected $settings;

		public function init() {
			$this->settings = Settings::get_instance();

			add_action( 'acf/init', array( $this, 'map_init' ) );
			add_action( 'wp_enqueue_scripts', array( $this, 'map_resources' ) );
			add_filter( 'litespeed_optimize_js_excludes', array( $this, 'set_exclude_rules' ) );
			add_filter( 'litespeed_optm_js_defer_exc', array( $this, 'set_defer_rules' ) );
		}

		/**
		 * Adds the Google Maps script
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 2.0.1
		 * @since 1.6.0
		 */
		public function map_resources() {
			// Control if the Google Maps script exists in the theme (for overriding the styling).
			if ( file_exists( get_template_directory() . '/assets/scripts/gital.library.google-maps.min.js' ) ) {
				wp_register_script( 'g_lib_google_maps_script', get_template_directory_uri() . '/assets/scripts/gital.library.google-maps.min.js', array(), '1.0.0', true );
			} else {
				wp_register_script( 'g_lib_google_maps_script', $this->settings->get( 'url_assets' ) . '/scripts/gital.library.google-maps.min.js', array(), '1.0.1', true );
			}
			wp_register_script( 'g_vendor_google_maps_external_script', 'https://maps.googleapis.com/maps/api/js?key=' . GOOGLE_MAPS_API . '&callback=g_init_google_maps', array( 'g_lib_google_maps_script' ), '1.0.0', true );

			wp_enqueue_script( 'g_lib_google_maps_script' );
			wp_enqueue_script( 'g_vendor_google_maps_external_script' );
		}

		/**
		 * Adds the Google Maps API Key to ACF
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.0.0
		 * @since 1.6.0
		 */
		public function map_init() {
			acf_update_setting( 'google_api_key', GOOGLE_MAPS_API );
		}

		/**
		 * Set defer rules for the internal Google Maps script
		 *
		 * @param array $list The list of scripts to exclude from defer.
		 *
		 * @return array $list The list of scripts to exclude from defer with the internal Google Maps script included.
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.0.0
		 * @since 2.36.6
		 */
		public function set_defer_rules( $list ) {
			array_push( $list, 'gital.library.google-maps.min.js' );
			$list = array_unique( array_filter( $list ) );
			return $list;
		}

		/**
		 * Set optimization rules for the Google Maps scripts
		 *
		 * @param array $list The list of scripts to exclude from optimization.
		 *
		 * @return array $list The list of scripts to exclude from optimization with the Google Maps scripts included.
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.0.0
		 * @since 2.36.7
		 */
		public function set_exclude_rules( $list ) {
			array_push( $list, 'gital.library.google-maps.min.js', 'g_init_google_maps' );
			$list = array_unique( array_filter( $list ) );
			return $list;
		}
	}
}
