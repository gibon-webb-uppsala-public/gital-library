<?php
/**
 * Cache Handling WordPress
 *
 * @package Gital Library
 */

namespace gital_library;

if ( ! class_exists( 'Cache_Handling' ) ) {
	/**
	 * Manages cache with LiteSpeed
	 *
	 * @author Gustav Gesar <gustav.gesar@gibon.se>
	 *
	 * @version 1.0.0
	 * @since 2.63.0
	 */
	class Cache_Handling extends Singleton {
		/**
		 * Init
		 *
		 * @return void
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.0.0
		 */
		public function init() {
			add_action( 'acf/options_page/save', array( $this, 'purge_all' ) );
		}

		/**
		 * Purge the Litespeed cache if Litespeed cache is activated
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.0.0
		 */
		public function purge_all() {
			if ( class_exists( '\LiteSpeed\Purge' ) ) {
				do_action( 'litespeed_purge_all' );
			}
		}
	}
}
