<?php
/**
 * Duplicate post
 *
 * @package Gital Library
 */

namespace gital_library;

if ( ! class_exists( 'Duplicate_Post' ) ) {
	/**
	 * Duplicate post
	 *
	 * @author Gustav Gesar <gustav.gesar@gibon.se>
	 *
	 * @version 1.1.1
	 * @since 2.44.0
	 */
	class Duplicate_Post extends Singleton {
		public function init() {
			add_action( 'admin_action_g_lib_duplicate_post_as_draft', array( $this, 'duplicate_post_as_draft' ) );

			foreach ( get_post_types() as $post_type ) {
				add_filter( $post_type . '_row_actions', array( $this, 'duplicate_post_link' ), 10, 2 );
			}
		}

		/**
		 * Duplicate post as draft
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.0.1
		 * @since 1.0.0
		 */
		public function duplicate_post_as_draft() {
			if ( ! ( isset( $_GET['post'] ) || isset( $_POST['post'] ) || ( isset( $_REQUEST['action'] ) && 'duplicate_post_as_draft' === $_REQUEST['action'] ) ) ) {
				wp_die( 'No post to duplicate has been supplied!' );
			}

			if ( ! isset( $_GET['duplicate_nonce'] ) || ! wp_verify_nonce( sanitize_key( $_GET['duplicate_nonce'] ), basename( __FILE__ ) ) ) {
				return;
			}

			$source_post_id = ( isset( $_GET['post'] ) ? absint( $_GET['post'] ) : absint( $_POST['post'] ) );
			$source_post    = get_post( $source_post_id );

			$current_user    = wp_get_current_user();
			$new_post_author = $current_user->ID;

			if ( isset( $source_post ) && null !== $source_post ) {

				$new_post_data = array(
					'comment_status' => $source_post->comment_status,
					'ping_status'    => $source_post->ping_status,
					'post_author'    => $new_post_author,
					'post_content'   => $source_post->post_content,
					'post_excerpt'   => $source_post->post_excerpt,
					'post_parent'    => $source_post->post_parent,
					'post_password'  => $source_post->post_password,
					'post_status'    => 'draft',
					'post_title'     => $source_post->post_title,
					'post_type'      => $source_post->post_type,
					'to_ping'        => $source_post->to_ping,
					'menu_order'     => $source_post->menu_order,
				);

				$new_post_id = wp_insert_post( $new_post_data );

				$taxonomies = get_object_taxonomies( $source_post->post_type );
				foreach ( $taxonomies as $taxonomy ) {
					$source_post_terms = wp_get_object_terms( $source_post_id, $taxonomy, array( 'fields' => 'slugs' ) );
					wp_set_object_terms( $new_post_id, $source_post_terms, $taxonomy, false );
				}

				$source_post_meta_keys = get_post_custom_keys( $source_post_id );
				if ( ! empty( $source_post_meta_keys ) ) {
					foreach ( $source_post_meta_keys as $source_post_meta_key ) {
						$source_post_meta_values = get_post_custom_values( $source_post_meta_key, $source_post_id );
						foreach ( $source_post_meta_values as $source_post_meta_value ) {
							add_post_meta( $new_post_id, $source_post_meta_key, $source_post_meta_value );
						}
					}
				}

				wp_safe_redirect( admin_url( 'post.php?action=edit&post=' . $new_post_id ) );
				exit;
			} else {
				wp_die( 'Post creation failed, could not find original post: ' . esc_html( $source_post_id ) );
			}
		}

		/**
		 * Add the duplicate link to action list for post_row_actions
		 *
		 * @param array  $actions The post_row_actions.
		 * @param object $post The post object.
		 *
		 * @return array The post_row_actions.
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.1.0
		 * @since 1.0.0
		 */
		public function duplicate_post_link( $actions, $post ) {
			if ( 'acf-field-group' === $post->post_type ) {
				return $actions;
			}
			if ( current_user_can( 'edit_posts' ) ) {
				$actions['duplicate'] = '<a href="' . wp_nonce_url( 'admin.php?action=g_lib_duplicate_post_as_draft&post=' . $post->ID, basename( __FILE__ ), 'duplicate_nonce' ) . '" title="' . esc_html__( 'Duplicate this item', 'gital-library' ) . '" rel="permalink">' . esc_html__( 'Duplicate', 'gital-library' ) . '</a>';
			}
			return $actions;
		}
	}
}
