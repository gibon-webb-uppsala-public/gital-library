<?php
/**
 * SEO
 *
 * @package Gital Library
 */

namespace gital_library;

if ( ! class_exists( 'SEO' ) ) {
	/**
	 * SEO
	 *
	 * Adds SEO features
	 *
	 * @author Gustav Gesar <gustav.gesar@gibon.se>
	 *
	 * @version 1.1.0
	 * @since 2.31.0
	 */
	class SEO extends Singleton {
		/**
		 * Settings
		 *
		 * @var object $settings Settings object.
		 */
		protected $settings;

		public function init() {
			$this->settings = Settings::get_instance();

			if ( in_array( 'wp-seopress/seopress.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ), true ) ) {
				add_filter( 'seopress_content_analysis_content', array( $this, 'content_analysis_content' ), 10, 1 );
			}
		}

		/**
		 * Content analysis content
		 *
		 * @param string $content The content.
		 *
		 * @param string $content The altered content.
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.0.0
		 */
		public function content_analysis_content( $content ) {
			$fields_to_analyze = array_merge(
				array(
					'title',
					'text',
					'question',
					'answer',
					'url',
				),
				$this->settings->get( 'seo_analyse_content_fields' )
			);

			$post_blocks = parse_blocks( $content );

			$post_blocks_data = $this->fetch_data( $post_blocks );

			foreach ( $post_blocks_data as $key => $value ) {
				$include = false;
				foreach ( $fields_to_analyze as $field_to_analyze ) {
					if ( str_contains( $key, $field_to_analyze ) ) {
						$include = true;
					}
				}
				if ( substr( $key, 0, 1 ) === '_' || ! is_string( $value ) ) {
					$include = false;
				}
				if ( $include ) {
					$content .= ' ' . $value;
				}
			}

			return $content;
		}

		/**
		 * Fetch data
		 *
		 * @param array $post_blocks The post blocks.
		 *
		 * @return array The post blocks data.
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.0.0
		 */
		protected function fetch_data( $post_blocks ) {
			$post_blocks_data = array();
			foreach ( $post_blocks as $post_block ) {
				if ( array_key_exists( 'blockName', $post_block ) && str_contains( $post_block['blockName'], 'gital' ) ) {
					if ( array_key_exists( 'attrs', $post_block ) && array_key_exists( 'data', $post_block['attrs'] ) ) {
						foreach ( $post_block['attrs']['data'] as $key => $value ) {
							if ( is_array( $value ) ) {
								foreach ( $value as $sub_key => $sub_value ) {
									$post_blocks_data[ $sub_key ] = $sub_value;
								}
							} else {
								$post_blocks_data[ $key ] = $value;
							}
						}
					}
				}
				if ( array_key_exists( 'innerBlocks', $post_block ) && ! empty( $post_block['innerBlocks'] ) ) {
					$post_blocks_data = array_merge( $post_blocks_data, fetch_data( $post_block['innerBlocks'] ) );
				}
			}
			return $post_blocks_data;
		}
	}
}
