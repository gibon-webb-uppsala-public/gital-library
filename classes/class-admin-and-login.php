<?php
/**
 * Styling for the wp-admin and the wp-login
 *
 * @package Gital Library
 */

namespace gital_library;

if ( ! class_exists( 'Admin_And_Login' ) ) {
	/**
	 * Admin And Login
	 *
	 * Styling for the wp-admin and the wp-login
	 *
	 * @author Gustav Gesar <gustav.gesar@gibon.se>
	 *
	 * @version 1.5.0
	 * @since 1.6.0
	 */
	class Admin_And_Login extends Singleton {
		/**
		 * Settings
		 *
		 * @var object $settings Settings object.
		 */
		protected $settings;

		/**
		 * Init
		 *
		 * @return void
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.0.0
		 */
		public function init() {
			$this->settings = Settings::get_instance();

			if ( ! $this->settings->get( 'disable_admin_styling' ) ) {
				add_action( 'admin_head', array( $this, 'admin_styling' ) );
				add_action( 'wp_before_admin_bar_render', array( $this, 'admin_logo' ) );
				add_action(
					'add_admin_bar_menus',
					function () {
						remove_action( 'admin_bar_menu', 'wp_admin_bar_wp_menu' );
					}
				);
				add_action( 'admin_bar_menu', array( $this, 'admin_menu_bar_content' ) );
				add_filter( 'admin_footer_text', array( $this, 'admin_footer_text' ) );
				add_action( 'customize_register', array( $this, 'disable_custom_css' ), 20 );
				add_action( 'acf/init', array( $this, 'hide_acf_if_not_gibonadmin' ) );
			}

			if ( ! $this->settings->get( 'disable_login_styling' ) ) {
				add_action( 'login_enqueue_scripts', array( $this, 'login_styling' ) );
				add_filter( 'login_message', array( $this, 'login_message' ) );
			}
		}

		/**
		 * Minify CSS
		 *
		 * @param string $css Unminified css.
		 *
		 * @return string Minified css.
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @since 1.6.0
		 * @version 1.0.0
		 */
		private function minify_css( $css ) {
			$css = preg_replace(
				array(
					'!/\*[^*]*\*+([^/][^*]*\*+)*/!',
					'/\s{2,}/',
					'/\s*([{}|:;,])\s+/',
					'/(\s|;|\})}/',
				),
				array(
					'',
					' ',
					'$1',
					'}',
				),
				trim( $css )
			);
			$css = str_replace( array( ': ', ';}' ), array( ':', '}' ), $css );
			return $css;
		}

		/**
		 * Styles the admin area
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 2.0.0
		 * @since 1.6.0
		 */
		public function admin_styling() {
			wp_register_style( 'g_lib_admin_style', $this->settings->get( 'url_assets' ) . '/styles/gital.library.admin.min.css', array(), '1.2' );
			wp_enqueue_style( 'g_lib_admin_style' );

			$admin_css_rules = array();
			$current_screen  = get_current_screen();

			if ( ! is_super_admin() ) {
				if ( $this->settings->get( 'hide_admin_menu_items' ) ) {
					$admin_css_rules[] = '#toplevel_page_litespeed,#toplevel_page_meowapps-main-menu,#adminmenu a[href="options-general.php?page=litespeed-cache-options"],#menu-plugins,#menu-tools{display:none;}';
				} else {
					$admin_css_rules[] = 'tr[data-slug^="gital"].deactivate{display:none;}';
				}

				if ( ! $this->settings->get( 'enable_image_editor' ) ) {
					$admin_css_rules[] = '.attachment-details .edit-attachment,[onclick*="imageEdit.open"],[href*="action=edit&image-editor"]{display:none !important;}';
				}

				if ( ! empty( $this->settings->get( 'additional_admin_css' ) ) ) {
					$admin_css_rules[] = wp_strip_all_tags( $this->settings->get( 'additional_admin_css' ) );
				}

				if ( 'dashboard' === $current_screen->base ) {
					$admin_css_rules[] = '#screen-meta-links { display: none; }';
				}
			}

			if ( ! $this->settings->get( 'enable_block_patterns' ) ) {
				$admin_css_rules[] = '#menu-appearance a[href*="edit.php?post_type=wp_block"],.block-editor-inserter__tabs .block-editor-inserter__tablist-and-close-button{display:none !important;}';
			}

			if ( is_plugin_active( 'ninja-forms/ninja-forms.php' ) ) {
				$admin_css_rules[] = 'a.toplevel_page_ninja-forms .wp-menu-name, tr[data-plugin="ninja-forms/ninja-forms.php"] .plugin-title strong {font-size: 0.01px;color: transparent;} a.toplevel_page_ninja-forms .wp-menu-name:before, tr[data-plugin="ninja-forms/ninja-forms.php"] .plugin-title strong:before {content: "' . esc_html__( 'Forms', 'gital-library' ) . '";font-size: 14px;color: #f3f3f2;} a.toplevel_page_ninja-forms .wp-menu-name:before {display: block;color: #f3f3f2;} tr[data-plugin="ninja-forms/ninja-forms.php"] .plugin-title strong:before {display: inline-block;color: #000;} a.toplevel_page_ninja-forms.wp-has-current-submenu .wp-menu-name:before {color: #515151;}';
			}

			$admin_css = implode( '', $admin_css_rules );
			wp_add_inline_style( 'g_lib_admin_style', self::minify_css( $admin_css ) );
		}

		/**
		 * Disable the custom CSS
		 *
		 * @param object $wp_customize WP_Theme Object.
		 *
		 * @return void
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.0.0
		 */
		public function disable_custom_css( $wp_customize ) {
			if ( ! is_super_admin() && $this->settings->get( 'hide_admin_menu_items' ) ) {
				$wp_customize->remove_section( 'custom_css' );
			}
		}

		/**
		 * Adds the Gibon Logo to the admin area
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.0.0
		 * @since 1.6.0
		 */
		public function admin_logo() {
			require_once $this->settings->get( 'path_views' ) . 'admin-logo.php';
		}

		/**
		 * Styles the login area
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.1.0
		 * @since 1.6.0
		 */
		public function login_styling() {
			wp_register_style( 'g_lib_login_style', $this->settings->get( 'url_assets' ) . '/styles/gital.library.login.min.css', array(), '1.0.0' );
			wp_enqueue_style( 'g_lib_login_style' );
			wp_register_script( 'g_lib_login_script', $this->settings->get( 'url_assets' ) . '/scripts/gital.library.login.min.js', array( 'jquery' ), '1.0.0', true );
			wp_enqueue_script( 'g_lib_login_script' );
			$username_placeholder = __( 'Username / E-Mail', 'gital-library' );
			$password_placeholder = __( 'Password', 'gital-library' );
			wp_add_inline_script( 'g_lib_login_script', 'gUpdateLogin("' . $username_placeholder . '", "' . $password_placeholder . '")' );
		}

		/**
		 * Adds a message to the login form
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.1.0
		 * @since 1.6.0
		 */
		public function login_message() {
			$content = '<p>' . __( 'E-Mail:', 'gital-library' ) . ' ' . $this->settings->get( 'contact_email' )
			. '<br>' . __( 'Phone:', 'gital-library' ) . ' ' . $this->settings->get( 'contact_phone' );

			$button = new Button( __( 'Support', 'gital-library' ), '', 'g-button--first', $this->settings->get( 'external_support_url' ), true, 'white', 'small' );
			$button->add_button( __( 'Contact Gibon', 'gital-library' ), '', 'g-button--second', $this->settings->get( 'contact_uppsala_url' ), true, 'white', 'small' );
			$button->add_wrapper( 'left', 'g-margin-top g-margin-bottom' );
			$content .= $button->get_button();

			return $content;
		}

		/**
		 * Edits the text in the dashboard footer
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.0.0
		 * @since 1.17.0
		 */
		public function admin_footer_text() {
			echo '<span id="footer-thankyou">Powered by <a href="' . $this->settings->get( 'contact_uppsala_url' ) . '" target="_blank">Gibon Webb</a></span>';
		}

		/**
		 * Only show ACF settings if the user is gibonadmin
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.0.0
		 * @since 1.6.0
		 */
		public function hide_acf_if_not_gibonadmin() {
			if ( ! is_super_admin() ) {
				add_filter( 'acf/settings/show_admin', '__return_false' );
			}
		}

		/**
		 * Adds menu items to the gibon menu bar
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @param Object $wp_admin_bar The admin bar.
		 *
		 * @version 1.0.0
		 * @since 1.17.0
		 */
		public function admin_menu_bar_content( $wp_admin_bar ) {
			$wp_admin_bar->add_menu(
				array(
					'id'    => 'wp-logo',
					'title' => '<span class="ab-icon"></span>',
					'href'  => $this->settings->get( 'hosting_url' ),
					'meta'  => array(
						'title' => __( 'Gibon Webb', 'gital-library' ),
					),
				)
			);
			$wp_admin_bar->add_menu(
				array(
					'parent' => 'wp-logo-external',
					'id'     => 'support',
					'title'  => __( 'Web support', 'gital-library' ),
					'href'   => $this->settings->get( 'support_url' ),
				)
			);
			$wp_admin_bar->add_menu(
				array(
					'parent' => 'wp-logo-external',
					'id'     => 'contact_webb',
					'title'  => __( 'Contact Gibon Uppsala', 'gital-library' ),
					'href'   => $this->settings->get( 'contact_uppsala_url' ),
				)
			);
			$wp_admin_bar->add_menu(
				array(
					'parent' => 'wp-logo-external',
					'id'     => 'contact',
					'title'  => __( 'Contact other Gibon office', 'gital-library' ),
					'href'   => $this->settings->get( 'contact_url' ),
				)
			);
		}
	}
}
