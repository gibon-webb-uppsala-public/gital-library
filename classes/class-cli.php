<?php
/**
 * Cli functions
 *
 * @package Gital Library
 */

namespace gital_library;

if ( ! class_exists( 'Cli' ) ) {
	/**
	 * CLI Functions
	 *
	 * @author Gustav Gesar <gustav.gesar@gibon.se>
	 *
	 * @version 1.1.0
	 * @since 1.10.0
	 */
	class Cli {
		/**
		 * Forces WordPress to check for updates again
		 *
		 * Resets the transient 'update_plugins' to force WordPress to check for updates again
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.1.1
		 * @since 1.7.0
		 */
		public function recheck() {
			$update_plugins_object = get_site_transient( 'update_plugins' );

			if ( is_object( $update_plugins_object ) ) {
				if ( property_exists( $update_plugins_object, 'last_checked' ) ) {
					$update_plugins_object->last_checked = 0;
				}
				if ( property_exists( $update_plugins_object, 'checked' ) ) {
					$update_plugins_object->checked = 0;
				}
				set_site_transient( 'update_plugins', $update_plugins_object );
				\WP_CLI::success( 'Transient object "update_plugins" is updated' );
			} else {
				\WP_CLI::error( 'Transient "update_plugins" is not updated' );
			}
		}

		/**
		 * Updates the Managed Hosting widget in the admin area
		 *
		 * Updates the name and the date of the one who has done the managements.
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @param array $args The name of the one who did the last management of the site.
		 *
		 * @version 1.0.1
		 * @since 1.7.0
		 */
		public function update_managed_hosting( $args ) {
			$date        = time();
			$result_date = update_option( 'g_lib_mh_updated_date', $date );
			$result_name = update_option( 'g_lib_mh_updated_name', $args[0] );

			if ( $result_date || $result_name ) {
				\WP_CLI::success( 'Managed Hosting date and name updated' );
			} else {
				\WP_CLI::error( 'Managed Hosting date and name is not updated' );
			}
		}

		/**
		 * Returns information about the storage
		 *
		 * Returns the storage left at the account or if the storage is below a given threshold.
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @param array $args The first key is the amount of MB to compare against the server storage.
		 *
		 * @version 1.1.1
		 * @since 1.42.0
		 */
		public function storage( $args ) {
			if ( ! is_production_server() ) {
				\WP_CLI::error( 'The server is not compatible with the storage check' );
			}
			$storage_data = Storage::get_instance();
			if ( $storage_data->valid ) {
				$threshold = 0;
				if ( empty( $args ) ) {
					$threshold = Settings::get_instance()->get( 'storage_threshold' );
				} else {
					$threshold = intval( $args[0] );
				}
				if ( 0 === $threshold ) {
					$storage_left        = $storage_data->get_storage_left();
					$storage_left_string = 'The account has ' . $storage_left . ' MB left';
					\WP_CLI::success( $storage_left_string );
				} elseif ( $threshold > 0 ) {
					$storage_left = $storage_data->get_storage_left();
					if ( $storage_left < $threshold ) {
						\WP_CLI::success( 'The account has less than ' . $threshold . ' MB left. It has ' . $storage_left . ' MB left' );
					} else {
						\WP_CLI::success( 'The account has more than ' . $threshold . ' MB left. It has ' . $storage_left . ' MB left' );
					}
				} else {
					\WP_CLI::error( 'The argument needs to be of value "Int"' );
				}
			} else {
				\WP_CLI::error( 'No data available' );
			}
		}

		/**
		 * Cleans up ACF fields
		 *
		 * @return void
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.0.0
		 */
		public function clean_acf_fields() {

			/**
			 * Cleans up a field
			 *
			 * @param array  $field      The field to clean.
			 * @param int    $post_id    The post ID.
			 * @param string $group_name The name of the group.
			 *
			 * @return void
			 *
			 * @author Gustav Gesar <gustav.gesar@gibon.se>
			 *
			 * @version 1.0.0
			 */
			function clean_acf_field( $field, $post_id, $group_name = null ) {
				if ( empty( $field['value'] ) || ! is_array( $field['value'] ) ) {
					return;
				}
				$number_of_values = count( $field['value'] );
				$values           = array_unique( $field['value'] );
				$valid_values     = array();
				foreach ( $values as $value ) {
					if ( get_post_status( $value ) ) {
						$valid_values[] = $value;
					} else {
						\WP_CLI::line( sprintf( __( 'Found invalid value: %s', 'gital-library' ), $value ) );
					}
				}

				if ( count( $valid_values ) !== count( $field['value'] ) ) {
					$removed_values = $number_of_values - count( $valid_values );
					if ( $group_name ) {
						$group_values                   = get_field( $group_name, $post_id );
						$group_values[ $field['name'] ] = $valid_values;
						\WP_CLI::line( sprintf( __( "Updated the field '%1\$s' in the group '%2\$s' for post ID %3\$d (removed %4\$d values out of %5\$d)", 'gital-library' ), $field['label'], $group_name, $post_id, $removed_values, $number_of_values ) );
						update_field( $group_name, $group_values, $post_id );
					} else {
						\WP_CLI::line( sprintf( __( "Updated the field '%1\$s' for post ID %2\$d (removed %3\$d values out of %4\$d)", 'gital-library' ), $field['name'], $post_id, $removed_values, $number_of_values ) );
						update_field( $field['key'], $valid_values, $post_id );
					}
				}
			}

			/**
			 * Cleans up ACF sub fields
			 *
			 * @param array  $sub_fields  The sub fields to clean.
			 * @param array  $values      The values to clean.
			 * @param int    $post_id     The post ID.
			 * @param string $parent_name The name of the parent.
			 *
			 * @return void
			 *
			 * @author Gustav Gesar <gustav.gesar@gibon.se>
			 *
			 * @version 1.0.0
			 */
			function clean_acf_sub_fields( $sub_fields, $values, $post_id, $parent_name = null ) {
				foreach ( $sub_fields as $sub_field ) {
					if ( 'relationship' === $sub_field['type'] ) {
						if ( isset( $values[ $sub_field['name'] ] ) ) {
							$sub_field['value'] = $values[ $sub_field['name'] ];
							clean_acf_field( $sub_field, $post_id, $parent_name );
						}
					} elseif ( 'group' === $sub_field['type'] ) {
						if ( isset( $values[ $sub_field['name'] ] ) && is_array( $values[ $sub_field['name'] ] ) ) {
							clean_acf_sub_fields( $sub_field['sub_fields'], $values[ $sub_field['name'] ], $post_id, $sub_field['name'] );
						}
					} elseif ( 'repeater' === $sub_field['type'] ) {
						if ( isset( $values[ $sub_field['name'] ] ) && is_array( $values[ $sub_field['name'] ] ) ) {
							foreach ( $values[ $sub_field['name'] ] as $row_index => $row_value ) {
								clean_acf_sub_fields( $sub_field['sub_fields'], $row_value, $post_id, $sub_field['name'] );
							}
						}
					} elseif ( 'flexible_content' === $sub_field['type'] ) {
						if ( isset( $values[ $sub_field['name'] ] ) && is_array( $values[ $sub_field['name'] ] ) ) {
							foreach ( $values[ $sub_field['name'] ] as $layout_index => $layout_value ) {
								if ( is_array( $layout_value ) && isset( $layout_value['acf_fc_layout'] ) ) {
									foreach ( $sub_field['layouts'] as $layout ) {
										if ( $layout['name'] === $layout_value['acf_fc_layout'] ) {
											clean_acf_sub_fields( $layout['sub_fields'], $layout_value, $post_id, $sub_field['name'] );
										}
									}
								}
							}
						}
					}
				}
			}

			/**
			 * Cleans up ACF fields in blocks
			 *
			 * @param array $blocks  The blocks to clean.
			 * @param int   $post_id The post ID.
			 *
			 * @return void
			 *
			 * @author Gustav Gesar <gustav.gesar@gibon.se>
			 *
			 * @version 1.0.0
			 */
			function clean_acf_fields_in_blocks( $blocks, $post_id ) {
				foreach ( $blocks as $block ) {
					if ( isset( $block['attrs']['data'] ) && is_array( $block['attrs']['data'] ) ) {
						$block_data = $block['attrs']['data'];
						foreach ( $block_data as $field_key => $field_value ) {
							if ( strpos( $field_key, '_' ) === 0 ) {
								continue;
							}

							$field_key_name = '_' . $field_key;
							if ( isset( $block_data[ $field_key_name ] ) ) {
								$field = get_field_object( $block_data[ $field_key_name ] );
								if ( $field ) {
									if ( 'relationship' === $field['type'] ) {
										$field['value'] = $field_value;
										clean_acf_field( $field, $post_id );
									} elseif ( 'group' === $field['type'] ) {
										if ( is_array( $field_value ) ) {
											clean_acf_sub_fields( $field['sub_fields'], $field_value, $post_id, $field['name'] );
										}
									} elseif ( 'repeater' === $field['type'] ) {
										if ( is_array( $field_value ) ) {
											foreach ( $field_value as $row_index => $row_value ) {
												clean_acf_sub_fields( $field['sub_fields'], $row_value, $post_id, $field['name'] );
											}
										}
									} elseif ( 'flexible_content' === $field['type'] ) {
										if ( is_array( $field_value ) ) {
											foreach ( $field_value as $layout_index => $layout_value ) {
												if ( is_array( $layout_value ) && isset( $layout_value['acf_fc_layout'] ) ) {
													foreach ( $field['layouts'] as $layout ) {
														if ( $layout['name'] === $layout_value['acf_fc_layout'] ) {
															clean_acf_sub_fields( $layout['sub_fields'], $layout_value, $post_id, $field['name'] );
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}

					if ( isset( $block['innerBlocks'] ) && is_array( $block['innerBlocks'] ) ) {
						clean_acf_fields_in_blocks( $block['innerBlocks'], $post_id );
					}
				}
			}

			$post_types = get_post_types( array( 'public' => true ), 'names' );
			unset( $post_types['attachment'] );

			foreach ( $post_types as $post_type ) {
				$posts = get_posts(
					array(
						'post_type'   => $post_type,
						'numberposts' => -1,
						'post_status' => 'any',
						'fields'      => 'ids',
					)
				);

				foreach ( $posts as $post_id ) {
					$fields = get_field_objects( $post_id );

					if ( $fields ) {
						foreach ( $fields as $field ) {
							if ( 'group' === $field['type'] ) {
								$group_values = get_field( $field['name'], $post_id );
								if ( is_array( $group_values ) ) {
									clean_acf_sub_fields( $field['sub_fields'], $group_values, $post_id, $field['name'] );
								}
							} elseif ( 'relationship' === $field['type'] ) {
								$field['value'] = get_field( $field['name'], $post_id );
								clean_acf_field( $field, $post_id );
							} elseif ( 'repeater' === $field['type'] ) {
								$repeater_values = get_field( $field['name'], $post_id );
								if ( is_array( $repeater_values ) ) {
									foreach ( $repeater_values as $row_index => $row_value ) {
										clean_acf_sub_fields( $field['sub_fields'], $row_value, $post_id, $field['name'] );
									}
								}
							} elseif ( 'flexible_content' === $field['type'] ) {
								$flexible_values = get_field( $field['name'], $post_id );
								if ( is_array( $flexible_values ) ) {
									foreach ( $flexible_values as $layout_index => $layout_value ) {
										if ( is_array( $layout_value ) && isset( $layout_value['acf_fc_layout'] ) ) {
											foreach ( $field['layouts'] as $layout ) {
												if ( $layout['name'] === $layout_value['acf_fc_layout'] ) {
													clean_acf_sub_fields( $layout['sub_fields'], $layout_value, $post_id, $field['name'] );
												}
											}
										}
									}
								}
							}
						}
					}

					$blocks = parse_blocks( get_post_field( 'post_content', $post_id ) );
					clean_acf_fields_in_blocks( $blocks, $post_id );
				}
			}
		}
	}
}
