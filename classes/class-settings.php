<?php
/**
 * Settings
 *
 * @package Gital Library
 */

namespace gital_library;

use function get_plugin_data;

if ( ! class_exists( 'Settings' ) ) {
	/**
	 * Settings
	 *
	 * @author Gustav Gesar <gustav.gesar@gibon.se>
	 *
	 * @version 1.0.2
	 * @since 3.13.0
	 */
	class Settings extends Singleton {
		use Settings_Handler;

		/**
		 * Init
		 *
		 * @return void
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.0.0
		 */
		public function init() {
			if ( ! function_exists( 'get_plugin_data' ) ) {
				require_once ABSPATH . 'wp-admin/includes/plugin.php';
			}
			$this->define_settings();
		}

		/**
		 * Define settings
		 *
		 * @return void
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.0.0
		 */
		private function define_settings(): void {
			// Options.
			$this->set( 'additional_admin_css', defined( 'ADDITIONAL_ADMIN_CSS' ) ? ADDITIONAL_ADMIN_CSS : '' );
			$this->set(
				'additional_production_server',
				defined( 'ADDITIONAL_PRODUCTION_SERVER' )
					? ( is_array( ADDITIONAL_PRODUCTION_SERVER )
						? ADDITIONAL_PRODUCTION_SERVER
						: ( ADDITIONAL_PRODUCTION_SERVER === ''
							? array()
							: array( ADDITIONAL_PRODUCTION_SERVER ) ) )
					: array()
			);
			$this->set( 'additional_super_admins', defined( 'ADDITIONAL_SUPER_ADMINS' ) ? ADDITIONAL_SUPER_ADMINS : array() );
			$this->set( 'allowed_gutenberg_blocks', defined( 'ALLOWED_GUTENBERG_BLOCKS' ) ? ALLOWED_GUTENBERG_BLOCKS : array( 'core/columns', 'core/table' ) );
			$this->set( 'alter_image_threshold', defined( 'ALTER_IMAGE_THRESHOLD' ) ? ALTER_IMAGE_THRESHOLD : 2560 );
			$this->set( 'disable_admin_styling', defined( 'DISABLE_ADMIN_STYLING' ) ? DISABLE_ADMIN_STYLING : false );
			$this->set( 'disable_authenticate_users_in_rest', defined( 'DISABLE_AUTHENTICATE_USERS_IN_REST' ) ? DISABLE_AUTHENTICATE_USERS_IN_REST : false );
			$this->set( 'disable_generator_removal', defined( 'DISABLE_GENERATOR_REMOVAL' ) ? DISABLE_GENERATOR_REMOVAL : false );
			$this->set( 'disable_login_styling', defined( 'DISABLE_LOGIN_STYLING' ) ? DISABLE_LOGIN_STYLING : false );
			$this->set( 'disable_sanitize_filename', defined( 'DISABLE_SANITIZE_FILENAME' ) ? DISABLE_SANITIZE_FILENAME : false );
			$this->set( 'disable_support_page', defined( 'DISABLE_SUPPORT_PAGE' ) ? DISABLE_SUPPORT_PAGE : false );
			$this->set( 'disabled_gutenberg_blocks', defined( 'DISABLED_GUTENBERG_BLOCKS' ) ? DISABLED_GUTENBERG_BLOCKS : false );
			$this->set( 'disallow_file_mods_for_non_super_admins', defined( 'DISALLOW_FILE_MODS_FOR_NON_SUPER_ADMINS' ) ? DISALLOW_FILE_MODS_FOR_NON_SUPER_ADMINS : false );
			$this->set( 'enable_block_patterns', defined( 'ENABLE_BLOCK_PATTERNS' ) ? ENABLE_BLOCK_PATTERNS : false );
			$this->set( 'enable_comments', defined( 'ENABLE_COMMENTS' ) ? ENABLE_COMMENTS : false );
			$this->set( 'enable_emoji', defined( 'ENABLE_EMOJI' ) ? ENABLE_EMOJI : false );
			$this->set( 'enable_image_editor', defined( 'ENABLE_IMAGE_EDITOR' ) ? ENABLE_IMAGE_EDITOR : false );
			$this->set( 'enable_jquery_migrate', defined( 'ENABLE_JQUERY_MIGRATE' ) ? ENABLE_JQUERY_MIGRATE : false );
			$this->set( 'enable_svg_duotone_filters_and_global_styles', defined( 'ENABLE_SVG_DUOTONE_FILTERS_AND_GLOBAL_STYLES' ) ? ENABLE_SVG_DUOTONE_FILTERS_AND_GLOBAL_STYLES : false );
			$this->set( 'enable_wp_embed', defined( 'ENABLE_WP_EMBED' ) ? ENABLE_WP_EMBED : false );
			$this->set( 'hide_admin_menu_items', defined( 'HIDE_ADMIN_MENU_ITEMS' ) ? HIDE_ADMIN_MENU_ITEMS : false );
			$this->set( 'hide_posts', defined( 'HIDE_POSTS' ) ? HIDE_POSTS : false );
			$this->set( 'seo_analyse_content_fields', defined( 'SEO_ANALYSE_CONTENT_FIELDS' ) ? SEO_ANALYSE_CONTENT_FIELDS : array() );
			$this->set( 'show_all_block_patterns', defined( 'SHOW_ALL_BLOCK_PATTERNS' ) ? SHOW_ALL_BLOCK_PATTERNS : true );
			$this->set( 'show_all_gutenberg_blocks', defined( 'SHOW_ALL_GUTENBERG_BLOCKS' ) ? SHOW_ALL_GUTENBERG_BLOCKS : false );
			$this->set( 'show_widgets', defined( 'SHOW_WIDGETS' ) ? SHOW_WIDGETS : false );
			$this->set( 'fatal_error_filter', defined( 'FATAL_ERROR_FILTER' ) ? FATAL_ERROR_FILTER : false );
			$this->set( 'storage_threshold', defined( 'STORAGE_THRESHOLD' ) ? STORAGE_THRESHOLD : 300 );

			// Environmental.
			$this->set( 'production_server', '46.16.234.63' );
			$this->set(
				'production_servers',
				array_merge(
					array( $this->get( 'production_server' ) ),
					$this->get( 'additional_production_server' )
				)
			);
			$this->set(
				'allowed_servers',
				array_merge(
					array(
						$this->get( 'production_server' ),
						'192.168.50.5',
						'127.0.0.1',
					),
					$this->get( 'additional_production_server' )
				)
			);
			$this->set( 'super_admins', array_merge( array( 'gibonadmin' ), $this->get( 'additional_super_admins' ) ) );
			$this->set( 'dev_name', 'Gibon Webb' );
			$this->set( 'hosting_url', 'https://www.gibon.se/it-tjanster/webbhosting/' );
			$this->set( 'production_url', 'https://www.gibon.se/it-tjanster/webbproduktion-2/' );
			$this->set( 'referral_url', str_replace( array( 'http://', 'https://', '/wp' ), '', get_bloginfo( 'wpurl' ) ) );
			$this->set( 'support_url', get_admin_url( null, 'admin.php?page=gibon-webb-support' ) );
			$this->set( 'external_support_url', 'https://webbuppsala.gibon.se/support/?utm_source=' . $this->get( 'referral_url' ) . '&utm_medium=referral' );
			$this->set( 'contact_url', 'https://www.gibon.se/vara-kontor/?utm_source=' . $this->get( 'referral_url' ) . '&utm_medium=referral' );
			$this->set( 'contact_phone', '018-99 90 110' );
			$this->set( 'contact_email', 'webb@gibon.se' );
			$this->set( 'contact_uppsala_url', 'https://www.gibon.se/vara-kontor/gibon-uppsala/?utm_source=' . $this->get( 'referral_url' ) . '&utm_medium=referral' );
			$this->set( 'google_maps_query_url', 'https://www.google.com/maps/search/?api=1&query=Google&query_place_id=' );

			$this->set( 'managed_hosting_from_name', 'Gibon Webb Managed Hosting' );
			$this->set( 'managed_hosting_from_email', 'noreply@gibon.se' );

			$this->set( 'correct_theme', mb_strpos( wp_get_theme()->get( 'Name' ), 'Gital Theme' ) !== false );
			$this->set( 'theme_version', wp_get_theme()->get( 'Version' ) );
			$this->set( 'blocks_installed', file_exists( WP_PLUGIN_DIR . '/gital-blocks/gital-blocks.php' ) );
			$this->set( 'blocks_version', $this->get( 'blocks_installed' ) ? get_plugin_data( WP_PLUGIN_DIR . '/gital-blocks/gital-blocks.php' )['Version'] : '' );

			// Paths.
			$this->set( 'url_root', plugins_url( '', __DIR__ ) );
			$this->set( 'url_assets', $this->get( 'url_root' ) . '/assets' );
			$this->set( 'url_fallbacks', $this->get( 'url_root' ) . '/fallbacks' );
			$this->set( 'url_patches', $this->get( 'url_root' ) . '/patches' );
			$this->set( 'url_resources', $this->get( 'url_root' ) . '/assets/resources' );
			$this->set( 'url_gibon_logo', $this->get( 'url_resources' ) . '/logo_gibon.svg' );
			$this->set( 'url_gibon_icon', $this->get( 'url_resources' ) . '/icon_gibon.svg' );
			$this->set( 'url_gibon_wallpaper', $this->get( 'url_resources' ) . '/wallpaper_gibon.svg' );
			$this->set( 'path_root', plugin_dir_path( __DIR__ ) );
			$this->set( 'path_assets', $this->get( 'path_root' ) . 'assets/' );
			$this->set( 'path_functions', $this->get( 'path_root' ) . 'functions/' );
			$this->set( 'path_fallbacks', $this->get( 'path_root' ) . 'fallbacks/' );
			$this->set( 'path_classes', $this->get( 'path_root' ) . 'classes/' );
			$this->set( 'path_views', $this->get( 'path_root' ) . 'views/' );
			$this->set( 'path_vendor', $this->get( 'path_root' ) . 'vendor/' );
			$this->set( 'path_components', $this->get( 'path_root' ) . 'components/' );
		}
	}
}
