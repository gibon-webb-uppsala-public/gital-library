<?php
/**
 * Cleans up and optimizes WordPress
 *
 * @package Gital Library
 */

namespace gital_library;

if ( ! class_exists( 'Optimize' ) ) {
	/**
	 * Cleans up and optimizes WordPress
	 *
	 * @author Gustav Gesar <gustav.gesar@gibon.se>
	 *
	 * @version 1.10.3
	 * @since 1.6.0
	 */
	class Optimize extends Singleton {
		/**
		 * Settings
		 *
		 * @var object $settings Settings object.
		 */
		protected $settings;

		/**
		 * Init
		 *
		 * @return void
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.0.0
		 */
		public function init() {
			$this->settings = Settings::get_instance();

			if ( ! $this->settings->get( 'enable_emoji' ) ) {
				$this->disable_emojis();
			}

			if ( ! $this->settings->get( 'enable_wp_embed' ) ) {
				add_action( 'wp_footer', array( $this, 'remove_wp_embed' ) );
			}

			if ( ! $this->settings->get( 'show_all_gutenberg_blocks' ) ) {
				add_filter( 'allowed_block_types_all', array( $this, 'remove_default_blocks' ), 10, 2 );
			}

			if ( ! $this->settings->get( 'enable_jquery_migrate' ) ) {
				add_action( 'wp_default_scripts', array( $this, 'remove_jquery_migrate' ) );
			}

			if ( $this->settings->get( 'enable_block_patterns' ) ) {
				if ( ! $this->settings->get( 'show_all_block_patterns' ) ) {
					add_action( 'init', array( $this, 'remove_block_patterns' ), 100 );
				}
			}

			if ( ! $this->settings->get( 'enable_svg_duotone_filters_and_global_styles' ) ) {
				remove_action( 'wp_enqueue_scripts', 'wp_enqueue_global_styles' );
				remove_action( 'wp_body_open', 'wp_global_styles_render_svg_filters' );
			}

			if ( $this->settings->exists( 'alter_image_threshold' ) ) {
				add_filter( 'big_image_size_threshold', array( $this, 'alter_image_threshold' ), 999, 1 );
			}

			if ( is_admin() ) {
				add_action( 'enqueue_block_editor_assets', array( $this, 'disable_editor_fullscreen_by_default' ) );
				add_filter( 'wp_kses_allowed_html', array( $this, 'enable_tags_in_acf_kses' ), 10, 2 );

				if ( $this->settings->get( 'hide_posts' ) ) {
					add_action( 'admin_menu', array( $this, 'remove_posts_menu' ) );
					add_action( 'admin_bar_menu', array( $this, 'remove_new_post_top_menu' ), 999 );
					add_action( 'wp_dashboard_setup', array( $this, 'remove_new_post_dashboard' ), 999 );
				}
			}

			if ( ! $this->settings->get( 'enable_comments' ) ) {
				add_action( 'admin_init', array( $this, 'remove_comments_and_trackbacks_support' ) );
				add_action( 'admin_init', array( $this, 'remove_comments_metabox' ) );
				add_action( 'admin_init', array( $this, 'redirect_from_comments_page' ) );
				add_action( 'admin_menu', array( $this, 'remove_comments_menu_page' ) );
				add_action( 'init', array( $this, 'remove_comments_menu_from_admin_bar' ) );
				add_action( 'wp_before_admin_bar_render', array( $this, 'remove_comments_from_admin_bar' ) );
				add_filter( 'comments_open', '__return_false', 20, 2 );
				add_filter( 'pings_open', '__return_false', 20, 2 );
				add_filter( 'comments_array', '__return_empty_array', 10, 2 );
			}
		}

		/**
		 * Disables the WordPress emojis
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.0.0
		 * @since 1.6.0
		 */
		public function disable_emojis() {
			remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
			remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
			remove_action( 'wp_print_styles', 'print_emoji_styles' );
			remove_action( 'admin_print_styles', 'print_emoji_styles' );
		}

		/**
		 * Remove wp-embed
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.0.0
		 * @since 1.6.0
		 */
		public function remove_wp_embed() {
			wp_dequeue_script( 'wp-embed' );
		}

		/**
		 * Remove default blocks from the Gutenberg editor
		 *
		 * @return @registered_blocks
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 2.10.2
		 * @since 0.1.4
		 */
		public function remove_default_blocks( $block_editor_context, $editor_context ) {
			$registered_blocks      = \WP_Block_Type_Registry::get_instance()->get_all_registered();
			$registered_blocks_keys = array_keys( $registered_blocks );
			$blocks_to_unset        = array();

			foreach ( $registered_blocks_keys as $registered_block_key ) {
				if ( preg_match( '/^(core|ninja-forms|sbi|woocommerce|wpseopress)\//', $registered_block_key ) ) {
					$blocks_to_unset[] = $registered_block_key;
				}
			}

			if ( ! $this->settings->get( 'enable_block_patterns' ) ) {
				unset( $blocks_to_unset['core/block'] );
			}

			// Remove keys from array.

			$blocks_to_unset = array_diff(
				$blocks_to_unset,
				array(
					'core/heading',
					'core/image',
					'core/gallery',
					'core/list',
					'core/paragraph',
					'core/spacer',
				)
			);

			if ( $this->settings->get( 'disabled_gutenberg_blocks' ) ) {
				$blocks_to_unset = array_merge( $blocks_to_unset, $this->settings->get( 'disabled_gutenberg_blocks' ) );
			}

			if ( $this->settings->get( 'allowed_gutenberg_blocks' ) ) {
				$blocks_to_unset = array_diff( $blocks_to_unset, $this->settings->get( 'allowed_gutenberg_blocks' ) );
			}
			$allowed_blocks = array_diff(
				$registered_blocks_keys,
				$blocks_to_unset
			);

			$allowed_blocks = apply_filters( 'g_lib_registered_blocks', $allowed_blocks, $editor_context );

			$allowed_blocks = array_unique( $allowed_blocks );

			$allowed_blocks = array_values( $allowed_blocks );

			return $allowed_blocks;
		}

		/**
		 * Disables the default fullscreen mode from wp-admin
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.0.0
		 * @since 1.6.0
		 */
		public function disable_editor_fullscreen_by_default() {
			$script = "jQuery( window ).load(function() { const isFullscreenMode = wp.data.select( 'core/edit-post' ).isFeatureActive( 'fullscreenMode' ); if ( isFullscreenMode ) { wp.data.dispatch( 'core/edit-post' ).toggleFeature( 'fullscreenMode' ); } });";
			wp_add_inline_script( 'wp-blocks', $script );
		}

		/**
		 * Remove jquery-migrate
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.0.0
		 * @since 1.6.0
		 */
		public function remove_jquery_migrate( $scripts ) {
			if ( ! is_admin() && ! empty( $scripts->registered['jquery'] ) ) {
				$scripts->registered['jquery']->deps = array_diff(
					$scripts->registered['jquery']->deps,
					array( 'jquery-migrate' )
				);
			}
		}

		/**
		 * Removes Gutenberg Block Patterns
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.0.0
		 * @since 1.6.0
		 */
		public function remove_block_patterns() {
			remove_theme_support( 'core-block-patterns' );
		}

		/**
		 * Hide posts
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.0.0
		 * @since 1.6.0
		 */
		public function remove_posts_menu() {
			remove_menu_page( 'edit.php' );
		}

		/**
		 * Removes the new post option for posts in the top menu
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.0.0
		 * @since 2.15.0
		 */
		public function remove_new_post_top_menu( $wp_admin_bar ) {
			$wp_admin_bar->remove_node( 'new-post' );
		}

		/**
		 * Removes the new post option for posts in dashboard
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.0.0
		 * @since 2.15.0
		 */
		public function remove_new_post_dashboard() {
			remove_meta_box( 'dashboard_quick_press', 'dashboard', 'side' );
		}

		/**
		 * Enable the <style> tag in ACF-description
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.0.0
		 * @since 1.31.0
		 */
		public function enable_tags_in_acf_kses( $tags, $context ) {
			if ( $context === 'acf' ) {
				$tags['style'] = array();
			}

			return $tags;
		}

		/**
		 * Alter the image threshold
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @param mixed $threshold The threshold
		 *
		 * @return mixed The altered threshold
		 *
		 * @version 1.0.1
		 * @since 2.7.0
		 */
		public function alter_image_threshold( $threshold ) {
			if ( is_numeric( $this->settings->get( 'alter_image_threshold' ) ) ) {
				return (int) $this->settings->get( 'alter_image_threshold' );
			}
			return $threshold;
		}

		/**
		 * Removes comments from admin bar
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.0.0
		 * @since 1.6.0
		 */
		public function remove_comments_from_admin_bar() {
			global $wp_admin_bar;
			$wp_admin_bar->remove_menu( 'comments' );
		}

		/**
		 * Redirect users if accesing comments section directly
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.0.0
		 *
		 * @since 2.41.0
		 */
		public function redirect_from_comments_page() {
			global $pagenow;

			if ( $pagenow === 'edit-comments.php' ) {
				wp_safe_redirect( admin_url() );
				exit;
			}
		}

		/**
		 * Remove comment menu page
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.0.0
		 *
		 * @since 2.41.0
		 */
		public function remove_comments_menu_page() {
			remove_menu_page( 'edit-comments.php' );
		}

		/**
		 * Remove comments and trackbacks support
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.0.0
		 *
		 * @since 2.41.0
		 */
		public function remove_comments_and_trackbacks_support() {
			foreach ( get_post_types() as $post_type ) {
				if ( post_type_supports( $post_type, 'comments' ) ) {
					remove_post_type_support( $post_type, 'comments' );
					remove_post_type_support( $post_type, 'trackbacks' );
				}
			}
		}

		/**
		 * Remove comments metabox
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.0.0
		 *
		 * @since 2.41.0
		 */
		public function remove_comments_metabox() {
			remove_meta_box( 'dashboard_recent_comments', 'dashboard', 'normal' );
		}

		/**
		 * Remove comments menu from admin bar
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.0.0
		 *
		 * @since 2.41.0
		 */
		public function remove_comments_menu_from_admin_bar() {
			if ( is_admin_bar_showing() ) {
				remove_action( 'admin_bar_menu', 'wp_admin_bar_comments_menu', 60 );
			}
		}
	}
}
