<?php
/**
 * Managed Hosting
 *
 * @package Gital Library
 */

namespace gital_library;

if ( ! class_exists( 'Managed_Hosting' ) ) {
	/**
	 * Managed Hosting
	 *
	 * Controls if the site has managed hosting and displays a message
	 *
	 * @author Gustav Gesar <gustav.gesar@gibon.se>
	 *
	 * @version 1.6.0
	 * @since 1.14.0
	 */
	class Managed_Hosting extends Singleton {
		/**
		 * Settings
		 *
		 * @var object $settings Settings object.
		 */
		protected $settings;

		/**
		 * A list of IP-addresses that is listed as Gibon Webb Managed Hosting Servers
		 *
		 * @var array
		 */
		private array $allowed_servers;

		/**
		 * Init
		 *
		 * @return void
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.0.0
		 */
		public function init() {
			$this->settings        = Settings::get_instance();
			$this->allowed_servers = $this->settings->get( 'allowed_servers' );
			add_filter( 'auto_plugin_update_email', array( $this, 'mail_home_about_automatic_updates' ) );
			register_shutdown_function( array( $this, 'shutdown_handler' ) );
			add_action( 'plugins_loaded', array( $this, 'maybe_check_litespeed' ) );
			add_action( 'init', array( $this, 'check_storage_cron_event_init' ) );
			register_deactivation_hook( __FILE__, array( $this, 'deactivate_cron_events' ) );
		}

		/**
		 * Check storage cron event init
		 *
		 * @return void
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.1.0
		 */
		public function check_storage_cron_event_init() {
			if ( is_production_server() ) {
				if ( ! wp_next_scheduled( 'g_lib_check_storage' ) ) {
					wp_schedule_event( time(), 'hourly', 'g_lib_check_storage' );
				}
				add_action( 'g_lib_check_storage', array( $this, 'check_storage' ) );
			} else {
				wp_clear_scheduled_hook( 'g_lib_check_storage' );
			}
		}

		/**
		 * Check storage
		 *
		 * @return void
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.1.0
		 */
		public function check_storage() {
			$threshold    = $this->settings->get( 'storage_threshold' );
			$storage_data = Storage::get_instance();
			$storage_left = $storage_data->get_storage_left();

			if ( $storage_left > $threshold ) {
				return;
			}

			$message  = '<h1>' . sprintf( 'Low storage detected on %s', get_bloginfo( 'name' ) ) . '</h1>';
			$message .= '<p>' . sprintf( 'The storage on %s (%s) is getting low.', get_bloginfo( 'name' ), get_bloginfo( 'wpurl' ) ) . '</p>';
			$message .= '<p>' . sprintf( 'The storage left is now %s MB and the threshold is set to %s MB.', $storage_left, $threshold ) . '</p>';

			$to      = $this->settings->get( 'contact_email' );
			$subject = sprintf( 'Low storage detected on %s', get_bloginfo( 'name' ) );
			$body    = $message;
			$headers = array( 'Content-Type: text/html; charset=UTF-8', 'From: ' . $this->settings->get( 'managed_hosting_from_name' ) . ' <' . $this->settings->get( 'managed_hosting_from_email' ) . '>' );

			wp_mail( $to, $subject, $message, $headers );
		}

		/**
		 * Return list of IP-addresses that is listed as Gibon Webb Managed Hosting Servers
		 *
		 * @return array List of IP-addresses
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @since 1.15.0
		 * @version 1.0.0
		 */
		public function get_allowed_servers() {
			return $this->allowed_servers;
		}

		/**
		 * Control if the notice is disabled
		 *
		 * @return bool True if it is disabled
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @since 1.14.0
		 * @version 1.0.0
		 */
		public function control_if_disabled() {
			if ( 'empty' !== get_option( 'g_lib_disable_mh_notice', 'empty' ) && get_option( 'g_lib_disable_mh_notice' ) ) {
				return true;
			} else {
				return false;
			}
		}

		/**
		 * Render a notice
		 *
		 * @return string An HTML-string with a WP-notice warning
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @since 1.14.0
		 * @version 1.0.0
		 */
		public function render_notice() {
			$return  = '<div style="margin-left: 0;" id="message" class="notice notice-warning">';
			$return .= '<p>' . __( 'You do not seems to have Managed Hosting by Gibon Web Activated. Please make sure that the web site is taken care of elsewhere.', 'gital-library' ) . '</br>';
			$return .= __( 'The most important part is that the site is backed up regularly and that the security updates is applied.', 'gital-library' ) . '</p>';
			$return .= '<p>' . __( "If you'd like to read more about Managed Hosting by Gibon Web", 'gital-library' ) . ', <a target="_blank" href="' . $this->settings->get( 'hosting_url' ) . '">' . __( 'please click here', 'gital-library' ) . '.</a></p>';
			$return .= '</div>';
			return $return;
		}

		/**
		 * Mail home about automatic updates
		 *
		 * @param  array $email The email object.
		 *
		 * @return array The outgoing email.
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 * @author Sandra Lövqvist <sandra.lovqvist@gibon.se>
		 *
		 * @since 3.11.0
		 * @version 1.0.0
		 */
		public function mail_home_about_automatic_updates( $email ) {
			if ( ! is_array( $email ) ) {
				return false;
			}

			if ( ! is_production_server() ) {
				return false;
			}

			$updated_plugins = get_option( 'recently_activated' );
			$plugin_list     = '';

			if ( $updated_plugins && is_array( $updated_plugins ) ) {
				$plugin_list .= __( 'The following plugins have been updated automatically', 'gital-library' ) . ":\n\n";

				foreach ( $updated_plugins as $plugin => $time ) {
					if ( file_exists( WP_PLUGIN_DIR . '/' . $plugin ) ) {
						$plugin_data  = get_plugin_data( WP_PLUGIN_DIR . '/' . $plugin );
						$plugin_list .= $plugin_data['Name'] . ' - ' . __( 'Version', 'gital-library' ) . ': ' . $plugin_data['Version'] . "\n";
					}
				}
			}

			if ( empty( $plugin_list ) ) {
				return false;
			}

			$email = array_merge(
				$email,
				array(
					'to'      => $this->settings->get( 'contact_email' ),
					'subject' => __( 'Plugins have been updated', 'gital-library' ),
					'body'    => __( 'Hello', 'gital-library' ) . "!\n\n" . $plugin_list,
					'headers' => array( 'Content-Type: text/html; charset=UTF-8', 'From: ' . $this->settings->get( 'managed_hosting_from_name' ) . ' <' . $this->settings->get( 'managed_hosting_from_email' ) . '>' ),
				)
			);

			return $email;
		}

		/**
		 * Shutdown handler
		 *
		 * Set the setting "fatal_error_filter" to true to turn of the function, to false to allow all fatal errors or to an array out specific errors. The array should be structured like this:
		 *
		 * array(
		 *      'positive' => array(
		 *          'gital',
		 *      ),
		 *      'negative' => array(
		 *          'advanced-custom-fields',
		 *      ),
		 * );
		 *
		 * @return void
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.1.0
		 */
		public function shutdown_handler() {
			if ( ! is_production_server() ) {
				return;
			}

			$fatal_error_filter = $this->settings->get( 'fatal_error_filter' );
			$error              = error_get_last();

			if ( true === $fatal_error_filter || null === $error || E_ERROR !== $error['type'] ) {
				return;
			}

			if ( is_array( $fatal_error_filter ) && ! empty( $fatal_error_filter ) ) {
				if ( isset( $fatal_error_filter['negative'] ) && is_array( $fatal_error_filter['negative'] ) ) {
					$contains_negative = array_filter(
						$fatal_error_filter['negative'],
						function ( $word ) use ( $error ) {
							return str_contains( $error['message'], $word );
						}
					);

					if ( ! empty( $contains_negative ) ) {
						return;
					}
				}

				if ( isset( $fatal_error_filter['positive'] ) && is_array( $fatal_error_filter['positive'] ) ) {
					$contains_positive = array_filter(
						$fatal_error_filter['positive'],
						function ( $word ) use ( $error ) {
							return str_contains( $error['message'], $word );
						}
					);

					if ( empty( $contains_positive ) ) {
						return;
					}
				}
			}

			$message  = '<h1>' . sprintf( 'A fatal error has occurred on %s', get_bloginfo( 'name' ) ) . "</h1>\n";
			$message .= '<code style="font-size:12px"><span style="color:red;">Fatal Error: </span>' . esc_html( $error['message'] ) . "</code>\n";
			$message .= '<code style="font-size:12px">In file: ' . esc_html( $error['file'] ) . ' on line ' . esc_html( $error['line'] ) . '</code>';

			$to      = $this->settings->get( 'contact_email' );
			$subject = sprintf( 'Fatal error detected on %s', get_bloginfo( 'name' ) );
			$body    = $message;
			$headers = array( 'Content-Type: text/html; charset=UTF-8', 'From: ' . $this->settings->get( 'managed_hosting_from_name' ) . ' <' . $this->settings->get( 'managed_hosting_from_email' ) . '>' );

			wp_mail( $to, $subject, $body, $headers );
		}

		/**
		 * Maybe check litespeed
		 *
		 * @return void
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.0.0
		 */
		public function maybe_check_litespeed() {
			if ( is_plugin_active( 'litespeed-cache/litespeed-cache.php' ) ) {
				if ( ! wp_next_scheduled( 'g_lib_check_litespeed' ) ) {
					wp_schedule_event( time(), 'daily', 'g_lib_check_litespeed' );
				}
				add_action( 'g_lib_check_litespeed', array( $this, 'check_litespeed' ) );
			} else {
				wp_clear_scheduled_hook( 'g_lib_check_litespeed' );
			}
		}

		/**
		 * Deactivate cron events
		 *
		 * @return void
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.0.0
		 */
		public function deactivate_cron_events() {
			wp_clear_scheduled_hook( 'g_lib_check_litespeed' );
			wp_clear_scheduled_hook( 'g_lib_check_storage' );
		}

		/**
		 * Get litespeed directory info
		 *
		 * @return array|null An array with information about the litespeed directory or null if it does not exist
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.0.0
		 */
		private function get_litespeed_directory_info() {
			$base_path      = defined( 'WP_CONTENT_DIR' ) ? WP_CONTENT_DIR : ABSPATH . 'wp-content';
			$litespeed_path = $base_path . '/litespeed';

			if ( ! is_dir( $litespeed_path ) ) {
				return null;
			}

			$total_size = 0;
			$file_count = 0;
			$file_types = array();

			$iterator = new \RecursiveIteratorIterator( new \RecursiveDirectoryIterator( $litespeed_path ) );
			foreach ( $iterator as $file ) {
				if ( $file->isFile() ) {
					++$file_count;
					$total_size += $file->getSize();
					$extension   = $file->getExtension();
					if ( ! isset( $file_types[ $extension ] ) ) {
						$file_types[ $extension ] = 0;
					}
					++$file_types[ $extension ];
				}
			}

			$total_size_formatted = size_format( $total_size );

			return array(
				'total_size'           => $total_size,
				'total_size_formatted' => $total_size_formatted,
				'file_count'           => $file_count,
				'file_types'           => $file_types,
			);
		}

		/**
		 * Check litespeed folder for large folders and files
		 *
		 * @return void
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.0.0
		 */
		public function check_litespeed() {
			$info = $this->get_litespeed_directory_info();

			if ( null === $info ) {
				return;
			}

			if ( $info['total_size'] < 100 * 1024 * 1024 && $info['file_count'] < 1000 ) {
				return;
			}

			$message  = '<h1>' . sprintf( 'Large litespeed folder detected on %s', get_bloginfo( 'name' ) ) . '</h1>';
			$message .= '<p><strong>Total size: </strong>' . $info['total_size_formatted'] . '</p>';
			$message .= '<p><strong>File count: </strong>' . $info['file_count'] . '</p>';
			$message .= '<p><strong>File count per type:</strong></p>';
			$message .= '<ul>';
			foreach ( $info['file_types'] as $type => $count ) {
				$message .= '<li>' . $type . ': ' . $count . '</li>';
			}
			$message .= '</ul>';

			$to      = $this->settings->get( 'contact_email' );
			$subject = sprintf( 'Large litespeed folder detected on %s', get_bloginfo( 'name' ) );
			$body    = $message;
			$headers = array( 'Content-Type: text/html; charset=UTF-8', 'From: ' . $this->settings->get( 'managed_hosting_from_name' ) . ' <' . $this->settings->get( 'managed_hosting_from_email' ) . '>' );

			wp_mail( $to, $subject, $message, $headers );
		}
	}
}
