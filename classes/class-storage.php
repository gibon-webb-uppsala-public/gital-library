<?php
/**
 * Storage
 *
 * @package Gital Library
 */

namespace gital_library;

if ( ! class_exists( 'Storage' ) ) {
	/**
	 * Storage
	 *
	 * Adds a storage widget.
	 *
	 * @author Gustav Gesar <gustav.gesar@gibon.se>
	 *
	 * @version 1.0.1
	 * @since 1.40.0
	 */
	class Storage extends Singleton {
		/**
		 * Text
		 *
		 * @var string $text
		 */
		private string $html;

		/**
		 * Storage
		 *
		 * @var array $storage
		 */
		private array $storage;

		/**
		 * Valid
		 *
		 * @var bool $valid
		 */
		public bool $valid;

		public function init() {
			$this->valid = false;

			$output = shell_exec( 'uapi StatsBar get_stats display=\'diskusage\' --output=json' );

			if ( ! $output ) {
				return;
			}

			$this->storage = json_decode( $output, true );
			$this->storage = $this->storage['result']['data'][0] ?? false;

			if ( $this->storage ) {
				$defaults = array(
					'count'   => (string) '',
					'max'     => (string) '',
					'percent' => (int) 0,
					'_count'  => (int) 0,
					'_max'    => (int) 0,

				);
				$this->storage = wp_parse_args( $this->storage, $defaults );
				$this->valid   = true;
			}
		}

		/**
		 * Returns the storage left in MB.
		 *
		 * @return int The storage left.
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.0.0
		 * @since 1.42.0
		 */
		public function get_storage_left() {
			if ( $this->valid ) {
				$storage_left = $this->storage['_max'] - $this->storage['_count'];
				return $storage_left;
			} else {
				return false;
			}
		}

		/**
		 * Create and get HTML
		 *
		 * @return string A HTML string with the storage bar.
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.0.0
		 * @since 1.40.0
		 */
		public function get_html() {
			if ( $this->valid ) {
				$this->create_html();
			} else {
				$this->html = '';
			}
			return $this->html;
		}

		/**
		 * Create HTML
		 *
		 * @return string A HTML string with the storage bar.
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.0.0
		 * @since 1.40.0
		 */
		private function create_html() {
			$html       = '<div class="g-lib-mh-storage">'
							. '<p><strong>' . __( 'Used storage', 'gital-library' ) . '</strong></p>'
							. '<div class="g-lib-mh-storage__bar">'
								. '<div class="g-lib-mh-storage__inner_bar" style="width:' . $this->storage['percent'] . '%"></div>'
								. '<span class="g-lib-mh-storage__percentage">' . $this->storage['percent'] . '% / ' . $this->storage['count'] . '</span>'
							. '</div>'
							. '<span class="g-lib-mh-storage__min">0 GB</span>'
							. '<span class="g-lib-mh-storage__max">' . $this->storage['max'] . '</span>'
						. '</div>';
			$this->html = $html;
		}
	}
}
