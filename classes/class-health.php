<?php
/**
 * Health
 *
 * @package Gital Library
 */

namespace gital_library;

if ( ! class_exists( 'Health' ) ) {
	/**
	 * Health
	 *
	 * Adds health features
	 *
	 * @author Gustav Gesar <gustav.gesar@gibon.se>
	 *
	 * @version 1.0.2
	 * @since 2.32.0
	 */
	class Health extends Singleton {
		/**
		 * The managed hosting object.
		 *
		 * @var array
		 */
		private object $managed_hosting;

		public function init() {
			$this->managed_hosting = Managed_Hosting::get_instance();
			add_filter( 'site_status_tests', array( $this, 'remove_background_updates_test' ) );
		}

		/**
		 * Remove the background updates test from the health check if managed hosting is activated.
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.0.2
		 * @since 1.0.0
		 */
		public function remove_background_updates_test( $tests ) {
			if ( ! $this->managed_hosting->control_if_disabled() && is_allowed_server() ) {
				if ( isset( $tests['async']['background_updates'] ) ) {
					unset( $tests['async']['background_updates'] );
				}
			}
			return $tests;
		}
	}
}
