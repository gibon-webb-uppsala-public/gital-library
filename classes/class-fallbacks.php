<?php
/**
 * Fallbacks
 *
 * @package Gital Library
 */

namespace gital_library;

if ( ! class_exists( 'Fallbacks' ) ) {
	/**
	 * Fallbacks
	 *
	 * @author Gustav Gesar <gustav.gesar@gibon.se>
	 *
	 * @version 1.2.1
	 * @since 3.36.0
	 */
	class Fallbacks extends Singleton {
		/**
		 * Settings
		 *
		 * @var object $settings Settings object.
		 */
		protected $settings;

		/**
		 * Fallbacks constructor.
		 *
		 * @return void
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.0.0
		 */
		public function init() {
			$this->settings = Settings::get_instance();

			if ( $this->settings->get( 'correct_theme' ) ) {
				if ( version_compare( $this->settings->get( 'theme_version' ), '2.9.2', '<=' ) ) {
					require_once $this->settings->get( 'path_fallbacks' ) . 'fallback_theme_2_9_2.php';
				}
				if ( version_compare( $this->settings->get( 'theme_version' ), '2.9.0', '<=' ) ) {
					require_once $this->settings->get( 'path_fallbacks' ) . 'fallback_theme_2_9_0.php';
				}
				if ( version_compare( $this->settings->get( 'theme_version' ), '3.16.1', '<=' ) ) {
					require_once $this->settings->get( 'path_fallbacks' ) . 'fallback_theme_3_16_1.php';
				}
				if ( version_compare( $this->settings->get( 'theme_version' ), '3.28.1', '<=' ) ) {
					add_action( 'wp_enqueue_scripts', array( $this, 'fallback_theme_3_28_1' ) );
				}
				if ( version_compare( $this->settings->get( 'theme_version' ), '3.39.0', '<=' ) && file_exists( get_template_directory() . '/assets/scripts/gital.library.vendor.google-maps.js' ) && defined( 'GOOGLE_MAPS_API' ) && ! empty( GOOGLE_MAPS_API ) ) {
					add_action( 'wp_enqueue_scripts', array( $this, 'fallback_theme_3_39_0' ), 20 );
				}
				if ( version_compare( $this->settings->get( 'theme_version' ), '3.56.0', '<=' ) ) {
					require_once $this->settings->get( 'path_fallbacks' ) . 'fallback_theme_3_56_0.php';
				}
			}

			if ( $this->settings->get( 'blocks_installed' ) ) {
				if ( version_compare( $this->settings->get( 'blocks_version' ), '3.26.1', '<=' ) ) {
					require_once $this->settings->get( 'path_fallbacks' ) . 'fallback_blocks_3_26_1.php';
					add_action( 'wp_enqueue_scripts', array( $this, 'fallback_blocks_3_26_1' ) );
				}
			}
		}

		/**
		 * Fallback for Gital Theme 3.28.1
		 *
		 * @return void
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.0.0
		 */
		public function fallback_theme_3_28_1() {
			wp_register_style( 'g_lib_fallback_theme_style_3_28_1', $this->settings->get( 'url_fallbacks' ) . '/gital.library.fallback_theme_3_28_1.min.css', array(), '1.0.1' );
			wp_enqueue_style( 'g_lib_fallback_theme_style_3_28_1' );
		}

		/**
		 * Fallback for Gital Theme 3.39.0
		 *
		 * @return void
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.0.0
		 */
		public function fallback_theme_3_39_0() {
			wp_register_script( 'g_lib_fallback_theme_script_3_39_0_1', get_template_directory_uri() . '/assets/scripts/gital.library.vendor.google-maps.js', array( 'jquery' ), '1.0.0', true );
			wp_register_script( 'g_lib_fallback_theme_script_3_39_0_2', 'https://maps.googleapis.com/maps/api/js?key=' . GOOGLE_MAPS_API . '&callback=Function.prototype', array(), '1.0.0', true );
			wp_enqueue_script( 'g_lib_fallback_theme_script_3_39_0_1' );
			wp_enqueue_script( 'g_lib_fallback_theme_script_3_39_0_2' );

			wp_dequeue_script( 'g_lib_google_maps_script' );
			wp_dequeue_script( 'g_vendor_google_maps_external_script' );
		}

		/**
		 * Fallback for Gital Blocks 3.26.1
		 *
		 * @return void
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.0.0
		 */
		public function fallback_blocks_3_26_1() {
			wp_register_script( 'g_lib_fallback_blocks_script_3_26_1', $this->settings->get( 'url_fallbacks' ) . '/gital.library.fallback_blocks_3_26_1.min.js', array( 'g_lib_script' ), '1.0.0', true );
			wp_register_style( 'g_lib_fallback_blocks_style_3_26_1', $this->settings->get( 'url_fallbacks' ) . '/gital.library.fallback_blocks_3_26_1.min.css', array(), '1.0.0' );
			wp_enqueue_script( 'g_lib_fallback_blocks_script_3_26_1' );
			wp_enqueue_style( 'g_lib_fallback_blocks_style_3_26_1' );
		}
	}
}
