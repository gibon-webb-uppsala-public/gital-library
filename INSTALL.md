# Gital Plugin Boilerplate installation instructions

1. Rename the plugin folder (gital-boilerplate) to the name of the plugin.
2. Rename the root plugin file (gital-boilerplate.php) to the name of the plugin.
3. Rename the style and the script file to the name of the plugin.
4. Search and Replace (make sure to make a plan first and use "match case") in the following files:
   a. The root plugin file
   b. The webpack config file
   c. The composer config file.
5. Run composer install and npm install.
6. Run composer setup.
7. Test npm run production and make sure you get minified versions of the styles and the scripts.
8. Remove the .git folder and reinit your projects repository if not handled by parent project.
9. Remove this file.
