/**
 * Fullscreen Modal
 *
 * This object is a collection of methods to render and handle the fullscreen modal.
 *
 * @author Gustav Gesar <gustav.gesar@gibon.se>
 */
const fullscreen_modal = {
	/**
	 * init_modal
	 *
	 * The method to initialize the fullscreen modal
	 *
	 * @param {Object} current_image The element that initialized the fullscreen modal
	 * @param {string} modal_id      The id of the modal returned from Components:image_modal()
	 * @param {*}      navigation    False or the id of the images wrapper
	 *
	 * @author Gustav Gesar <gustav.gesar@gibon.se>
	 * @version 1.0.0
	 * @since 2.3.0
	 */
	init_modal(current_image, modal_id, navigation = false) {
		// Reach the img tag
		const modal_image = document.querySelector(
			'#' + modal_id + ' .g-image-modal__image'
		);
		if (modal_image) {
			// Load the current image and show the modal
			this.load_image(current_image, modal_image);
			showModalFromID(modal_id); // eslint-disable-line no-undef
		}
		if (navigation) {
			// Load navigation if required
			this.load_navigation(current_image, modal_image, navigation);
		} else {
			// Remove navigation if not
			this.remove_navigation(modal_id);
		}
	},

	/**
	 * load_image
	 *
	 * Loads the required image into the modal
	 *
	 * @param {Object} image_to_load The wrapper element of the image to load
	 * @param {Object} modal_image   The img tag of the modal image
	 */
	load_image(image_to_load, modal_image) {
		// Reset the img
		modal_image.src = '';
		modal_image.srcset = '';
		modal_image.sizes = '';

		// Load the attributes
		const src = image_to_load.dataset.fullscreenSrc;
		if (src) {
			modal_image.src = src;
		}
		const srcset = image_to_load.dataset.fullscreenSrcset;
		if (srcset) {
			modal_image.srcset = srcset;
		}
		const sizes = image_to_load.dataset.fullscreenSizes;
		if (sizes) {
			modal_image.sizes = sizes;
		}
	},

	/**
	 * load_navigation
	 *
	 * Creates the navigation buttons if they are not already created and initializes the navigation control
	 *
	 * @param {Object} current_image            The wrapper element of the image to load
	 * @param {Object} modal_image              The img tag of the modal image
	 * @param {string} current_image_wrapper_id The id of the wrapper of all the images
	 */
	load_navigation(current_image, modal_image, current_image_wrapper_id) {
		const current_image_wrapper = document.getElementById(
			current_image_wrapper_id
		);
		const modal_content_wrapper = modal_image.parentElement;

		let modal_navigation = modal_content_wrapper.querySelectorAll(
			'.g-image-modal__navigation'
		);

		if (0 === modal_navigation.length) {
			const modal_navigation_buttons = document.createElement('div');
			modal_navigation_buttons.setAttribute(
				'class',
				'g-image-modal__navigation-wrapper'
			);
			modal_navigation_buttons.innerHTML = `<button class="g-image-modal__navigation g-image-modal__navigation--left g-button g-button--primary g-button--arrow g-button--arrow-left"></button><button class="g-image-modal__navigation g-image-modal__navigation--right g-button g-button--primary g-button--arrow g-button--arrow-right"></button>`;
			modal_content_wrapper.appendChild(modal_navigation_buttons);
			modal_navigation = modal_content_wrapper.querySelectorAll(
				'.g-image-modal__navigation'
			);
		}

		this.control_navigation(
			current_image,
			modal_navigation,
			current_image_wrapper,
			modal_image
		);
	},

	/**
	 * control_navigation
	 *
	 * Controls the current image and enables/disables the navigation buttons
	 *
	 * @param {Object}   current_image         The wrapper element of the image to load
	 * @param {NodeList} modal_navigation      A list of the navigation buttons
	 * @param {Object}   current_image_wrapper The wrapper of all the images
	 * @param {Object}   modal_image           The img tag of the modal image
	 */
	control_navigation(
		current_image,
		modal_navigation,
		current_image_wrapper,
		modal_image
	) {
		// Resets the buttons
		modal_navigation.forEach((button) => {
			button.disabled = true;
		});
		//Getting the index of the current image
		const image_index = [...current_image_wrapper.children].indexOf(
			current_image.parentElement
		);
		// Getting the complete amount of images
		const number_of_images = current_image_wrapper.children.length;

		// Controls the buttons
		const control_button = (next) => {
			const targeted_image =
				current_image_wrapper.children[image_index + (next ? -1 : 1)];
			const targeted_image_is_image = targeted_image.querySelector('img')
				? true
				: false;
			if (targeted_image_is_image) {
				modal_navigation[next ? 0 : 1].disabled = false;
				// Enabling the onclick event
				modal_navigation[next ? 0 : 1].onclick = () => {
					const previous_image = targeted_image.firstElementChild;
					this.load_image(previous_image, modal_image);
					this.control_navigation(
						previous_image,
						modal_navigation,
						current_image_wrapper,
						modal_image
					);
				};
			}
		};

		// Controls the previous button
		if (0 !== image_index) {
			control_button(true);
		}

		// Controls the next button
		if (image_index + 1 !== number_of_images) {
			control_button(false);
		}
	},

	/**
	 * remove_navigation
	 *
	 * Removes the navigation buttons
	 *
	 * @param {string} modal_id The id of the modal returned from Components:image_modal()
	 */
	remove_navigation(modal_id) {
		const modal_navigation = document.querySelectorAll(
			'#' + modal_id + ' .g-image-modal__navigation'
		);

		if (0 !== modal_navigation.length) {
			modal_navigation.forEach((button) => {
				button.remove();
			});
		}
	},
};
window.g_fullscreen_modal = fullscreen_modal;
