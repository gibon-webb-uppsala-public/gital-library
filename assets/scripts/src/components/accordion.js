jQuery( function( $ ) {
	$( '.g-accordion__header' ).on( 'click', function() {
		if ( $( this ).hasClass( 'g-accordion__header--collapsed' ) ) {
			var toggleThis = true;
		} else {
			var toggleThis = false;
		}
		$( '.g-accordion__header--expanded' ).removeClass( 'g-accordion__header--expanded' ).addClass( 'g-accordion__header--collapsed' );
		$( '.g-accordion__content--expanded' ).removeClass( 'g-accordion__content--expanded' ).addClass( 'g-accordion__content--collapsed' ).slideUp();
		if ( toggleThis ) {
			$( this ).removeClass( 'g-accordion__header--collapsed' ).addClass( 'g-accordion__header--expanded' );
			$( this ).siblings().removeClass( 'g-accordion__content--collapsed' ).addClass( 'g-accordion__content--expanded' ).slideDown();
		}
	} );
} );
