// Set cookie
function set_cookie( cname, cvalue, exdays, samesite ) {
	let expires;
	let date;
	if ( exdays !== 0 ) {
		date = new Date();
		date.setTime( date.getTime() + exdays * 24 * 60 * 60 * 1000 );
		expires = 'expires=' + date.toUTCString() + ';';
	}
	document.cookie = cname + '=' + cvalue + ';' + expires + 'path=/;samesite=' + samesite;
}
window.gSetCookie = set_cookie;
window.setCookie = set_cookie;
window.g_set_cookie = set_cookie;

// Get cookie
function get_cookie( cname ) {
	var name = cname + '=';
	var decodedCookie = decodeURIComponent( document.cookie );
	var ca = decodedCookie.split( ';' );
	for ( var i = 0; i < ca.length; i++ ) {
		var c = ca[i];
		while ( c.charAt( 0 ) == ' ' ) {
			c = c.substring( 1 );
		}
		if ( c.indexOf( name ) == 0 ) {
			return c.substring( name.length, c.length );
		}
	}
	return '';
}
window.gGetCookie = get_cookie;
window.getCookie = get_cookie;
window.g_get_cookie = get_cookie;
