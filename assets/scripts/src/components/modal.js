// Global constants
const body = document.body;

// Defining events
const show_modal_event = new Event('showing');
const close_modal_event = new Event('closing');

/**
 * Show modal
 *
 * @param {Object} modal The modal to show
 */
const show_modal = (modal) => {
	// Return if modal is already open
	if (modal.classList.contains('g-modal--open')) {
		return;
	}

	// Defining constants
	const x_closer = modal.querySelector('.g-modal__x-closer');
	const outside = modal.querySelector('.g-modal__close-outside');

	// Dispatching synthetic event
	modal.dispatchEvent(show_modal_event);

	// Show the modal
	setTimeout(() => {
		modal.style.transform = 'scale(1)';
		modal.style.opacity = '1';
	}, 100);
	modal.style.display = 'flex';
	body.classList.add('g-body--modal-open');
	modal.classList.add('g-modal--open');

	// Close this modal by click handler
	const close_modal_click_handler = () => {
		close_modal(modal);
		remove_event_listeners();
	};

	// Close this modal by keydown handler
	const close_modal_keydown_handler = (event) => {
		if ('Escape' === event.key) {
			close_modal(modal);
			remove_event_listeners();
		}
	};

	// Close by X
	x_closer.addEventListener('click', close_modal_click_handler);

	// Close by Outside
	outside.addEventListener('click', close_modal_click_handler);

	// Close by Esc
	document.addEventListener('keydown', close_modal_keydown_handler);

	// Remove event listeners
	const remove_event_listeners = () => {
		document.removeEventListener('keydown', close_modal_keydown_handler);
		x_closer.removeEventListener('click', close_modal_click_handler);
		outside.removeEventListener('click', close_modal_click_handler);
	};
};

/**
 * Close modal
 *
 * @param {Object} modal The modal to close
 */
const close_modal = (modal) => {
	// Dispatching synthetic event
	modal.dispatchEvent(close_modal_event);

	// Close the modal
	modal.style.opacity = '';
	modal.style.transform = '';
	setTimeout(() => {
		modal.style.display = '';
	}, 500);
	body.classList.remove('g-body--modal-open');
	modal.classList.remove('g-modal--open');
};

/**
 * Show modal from ID
 *
 * @param {string} id The id of the modal to show
 */
const show_modal_from_id = (id) => {
	const modal = document.getElementById(id);
	if (modal) {
		show_modal(modal);
	}
};
window.g_show_modal_from_id = show_modal_from_id;

/**
 * Close modal from ID
 *
 * @param {string} id The id of the modal to close
 */
const close_modal_from_id = (id) => {
	const modal = document.getElementById(id);
	if (modal) {
		close_modal(modal);
	}
};
window.g_close_modal_from_id = close_modal_from_id;

// Fallbacks to former naming convention
window.showModalFromID = show_modal_from_id;
window.closeModalFromID = close_modal_from_id;
