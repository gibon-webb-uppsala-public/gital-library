/**
 * Fullscreen Modal
 *
 * This object is a collection of methods to render and handle the fullscreen modal.
 *
 * @author Gustav Gesar <gustav.gesar@gibon.se>
 */
const media_modal = {
    /**
     * init_modal
     *
     * The method to initialize the fullscreen modal
     *
     * @param {Object} current_element The element that initialized the fullscreen modal
     * @param {string} modal_id      The id of the modal returned from Components:image_modal()
     * @param {*}      navigation    False or the id of the images wrapper
     *
     * @author Gustav Gesar <gustav.gesar@gibon.se>
     * @version 2.0.0
     * @since 2.3.0
     */
    init_modal(current_element, modal_id, navigation = false) {
        const is_image = current_element.querySelector('img') !== null;
        const is_internal_video = current_element.querySelector('video') !== null;
        const is_external_video = current_element.querySelector('iframe') !== null;
        const modal = document.getElementById(modal_id);
        const original_state = modal.innerHTML;

        if (is_image) {
            this.load_image(current_element, modal);
            g_show_modal_from_id(modal_id); // es-lint-disable-line no-undef            
        } else if( is_internal_video ) {
            this.load_internal_video(current_element, modal);
            g_show_modal_from_id(modal_id); // es-lint-disable-line no-undef
        } else if(is_external_video) {
            this.load_internal_video(current_element, modal);
        }

        if (navigation) {
            this.load_navigation(current_element, modal, navigation);
        } else {
            this.remove_navigation(modal_id);
        }

        modal.addEventListener('closing', () => this.reset_modal( modal, original_state ) );
    },

    reset_modal( modal, original_state ) {
        setTimeout(() => {
            modal.innerHTML = original_state;
        }, 500);
    },

    load_image(image_to_load, modal) {
        const modal_video = modal.querySelector('.g-media-modal__item--video' );
        const modal_image = modal.querySelector('.g-media-modal__item--image' );
        const modal_iframe = modal.querySelector('.g-media-modal__item--iframe' );

        if (! modal_image || ! modal_video || ! modal_iframe) {
            return;
        }

        modal_image.style.display = 'block';
        modal_video.style.display = 'none';
        modal_iframe.style.display = 'none';

        modal_image.src = '';
        modal_image.srcset = '';
        modal_image.sizes = '';

        const src = image_to_load.dataset.fullscreenSrc;
        if (src) {
            modal_image.src = src;
        }
        const srcset = image_to_load.dataset.fullscreenSrcset;
        if (srcset) {
            modal_image.srcset = srcset;
        }
        const sizes = image_to_load.dataset.fullscreenSizes;
        if (sizes) {
            modal_image.sizes = sizes;
        }
    },

    load_internal_video(video_to_load, modal) {
        const modal_video = modal.querySelector('.g-media-modal__item--video' );
        const modal_image = modal.querySelector('.g-media-modal__item--image' );
        const modal_iframe = modal.querySelector('.g-media-modal__item--iframe' );

        if (! modal_image || ! modal_video || ! modal_iframe) {
            return;
        }

        const video_element = video_to_load.querySelector('video');

        modal_video.innerHTML = '';
        modal_video.removeAttribute('loop');
        modal_video.removeAttribute('autoplay');

        modal_image.style.display = 'none';
        modal_video.style.display = 'block';
        modal_iframe.style.display = 'none';

        const poster = video_element.getAttribute('poster');
        if (poster) {
            modal_video.poster = poster;
        }

        modal_video.src = video_element.currentSrc;

        if (video_element.hasAttribute('autoplay')) {
            modal_video.setAttribute('autoplay', '');

            if (video_element.hasAttribute('loop')) {
                modal_video.setAttribute('loop', '');
            }
            
            if (video_element.hasAttribute('controls')) {
                modal_video.setAttribute('controls', '');
                if (typeof g_plyr !== 'undefined') {
                    new g_plyr(modal_video);                  
                }
            }
        } else {
            modal_video.setAttribute('controls', '');
            
            if (typeof g_plyr !== 'undefined') {
                new g_plyr(modal_video);                  
            }
        }
    },

    load_external_video(video_to_load, modal) {
        const modal_video = modal.querySelector('.g-media-modal__item--video' );
        const modal_image = modal.querySelector('.g-media-modal__item--image' );
        const modal_iframe = modal.querySelector('.g-media-modal__item--iframe' );

        if (! modal_image || ! modal_video || ! modal_iframe) {
            return;
        }

        const iframe_element = modal_iframe.querySelector('iframe' );
        const iframe_src_element = video_to_load.querySelector('iframe');

        iframe_element.innerHTML = '';
        iframe_element.removeAttribute('src');

        modal_image.style.display = 'none';
        modal_video.style.display = 'none';
        modal_iframe.style.display = 'block';

        if (iframe_src_element.hasAttribute('src')) {
            iframe_element.setAttribute('src', iframe_src_element.getAttribute('src'));
            if (typeof g_plyr !== 'undefined') {
                new g_plyr(modal_iframe);
                modal_iframe.classList.add('g-media-modal__item--iframe', 'g-media-modal__item')
            }
        }
    },
    
    load_navigation(current_element, modal, current_element_wrapper_id) {
        const current_element_wrapper = document.getElementById(current_element_wrapper_id);
        let modal_navigation = modal.querySelectorAll('.g-image-modal__navigation');

        if (0 === modal_navigation.length ) {
            const modal_content_wrapper = modal.querySelector('.g-modal__content');
            const modal_navigation_buttons = document.createElement('div');
            modal_navigation_buttons.setAttribute('class', 'g-image-modal__navigation-wrapper');
            modal_navigation_buttons.innerHTML = `<button class="g-image-modal__navigation g-image-modal__navigation--left g-button g-button--primary g-button--arrow g-button--arrow-left"></button><button class="g-image-modal__navigation g-image-modal__navigation--right g-button g-button--primary g-button--arrow g-button--arrow-right"></button>`;
            modal_content_wrapper.appendChild(modal_navigation_buttons);
            modal_navigation = modal.querySelectorAll('.g-image-modal__navigation');
        }

        this.control_navigation(
            current_element,
            modal_navigation,
            current_element_wrapper,
            modal
        );
    },

    control_navigation(current_element, modal_navigation, current_element_wrapper, modal) {
        modal_navigation.forEach(button => button.disabled = true);
        const childrenArray = [...current_element_wrapper.children];
        let element_index = childrenArray.indexOf(current_element.parentElement);

        if (element_index === -1) {
            element_index = childrenArray.indexOf(current_element);
        }

        const number_of_elements = current_element_wrapper.children.length;
    
        const control_button = next => {
            const targeted_element = current_element_wrapper.children[element_index + (next ? -1 : 1)];
            
            if (targeted_element) {
                const is_image = targeted_element.querySelector('img') !== null;
                const is_internal_video = targeted_element.querySelector('video') !== null;
                const is_external_video = targeted_element.querySelector('iframe') !== null;
                modal_navigation[next ? 0 : 1].disabled = false;
                modal_navigation[next ? 0 : 1].onclick = () => {
                    const element_to_load = targeted_element.firstElementChild;
                    if (is_image) {
                        this.load_image(element_to_load, modal);
                    } else if( is_internal_video ) {
                        this.load_internal_video(element_to_load, modal);
                    } else if(is_external_video) {
                        this.load_external_video(element_to_load, modal);
                    }
                    this.control_navigation(element_to_load, modal_navigation, current_element_wrapper, modal);
                };
            }
        };

        if (element_index !== 0) {
            control_button(true);
        }
    
        if (element_index + 1 !== number_of_elements) {
            control_button(false);
        }
    },

    remove_navigation(modal_id) {
        const modal_navigation = document.querySelectorAll('#' + modal_id + ' .g-image-modal__navigation');
        const modal_video_navigation = document.querySelectorAll('#' + modal_id + ' .g-video-modal__navigation');

        if (modal_navigation.length > 0) {
            modal_navigation.forEach(button => button.remove());
        }

        if (modal_video_navigation.length > 0) {
            modal_video_navigation.forEach(button => button.remove());
        }
    }
};

window.g_media_modal = media_modal;
