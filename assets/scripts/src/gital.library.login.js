function gUpdateLogin( username, password ) {
	jQuery( '#user_pass' ).attr( 'placeholder', password );
	jQuery( '#user_login' ).attr( 'placeholder', username );
	jQuery( 'label[for=user_pass]' ).hide();
	jQuery( 'label[for=user_login]' ).hide();
}
window.gUpdateLogin = gUpdateLogin;
