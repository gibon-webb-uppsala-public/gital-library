const safe_email = ( id ) => {
	const email_object = document.getElementById( id );
	const email = email_object.getAttribute( 'username' ) + '@' + email_object.getAttribute( 'domain' );
	email_object.removeAttribute( 'username' );
	email_object.removeAttribute( 'domain' );
	email_object.removeAttribute( 'id' );
	email_object.setAttribute( 'href', 'mailto:' + email );
	email_object.innerText = email;
};
window.g_safe_email = safe_email;
