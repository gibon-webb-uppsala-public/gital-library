import SVGInject from '@iconfu/svg-inject';

const inject_svgs = () => {
	const elements_to_inject = document.querySelectorAll('[svg-inject]');
	if (elements_to_inject) {
		elements_to_inject.forEach((element) => {
			element.removeAttribute('width');
			element.removeAttribute('height');
			element.removeAttribute('style');
			element.removeAttribute('svg-inject');
			SVGInject(element);
		});
	}
};

window.addEventListener('DOMContentLoaded', inject_svgs);

window.g_inject_svgs = inject_svgs;
