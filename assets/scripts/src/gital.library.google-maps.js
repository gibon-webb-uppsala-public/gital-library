/*
 *  new_map
 *
 *  This function will render a Google Map onto the selected element
 */
function new_map(map_element) {
	const markers_elements = map_element.querySelectorAll(".marker");

	const args = {
		zoom: 16,
		center: new google.maps.LatLng(0, 0),
		mapTypeId: google.maps.MapTypeId.ROADMAP,
		zoomControl: true,
		mapTypeControl: false,
		scaleControl: true,
		streetViewControl: false,
		rotateControl: false,
		fullscreenControl: false,
		styles: [
			{
				featureType: "all",
				elementType: "labels.text.fill",
				stylers: [
					{
						saturation: 36,
					},
					{
						color: "#333333",
					},
					{
						lightness: 40,
					},
				],
			},
			{
				featureType: "all",
				elementType: "labels.text.stroke",
				stylers: [
					{
						visibility: "on",
					},
					{
						color: "#ffffff",
					},
					{
						lightness: 16,
					},
				],
			},
			{
				featureType: "all",
				elementType: "labels.icon",
				stylers: [
					{
						visibility: "off",
					},
				],
			},
			{
				featureType: "administrative",
				elementType: "geometry.fill",
				stylers: [
					{
						color: "#fefefe",
					},
					{
						lightness: 20,
					},
				],
			},
			{
				featureType: "administrative",
				elementType: "geometry.stroke",
				stylers: [
					{
						color: "#fefefe",
					},
					{
						lightness: 17,
					},
					{
						weight: 1.2,
					},
				],
			},
			{
				featureType: "landscape",
				elementType: "geometry",
				stylers: [
					{
						color: "#f5f5f5",
					},
					{
						lightness: 20,
					},
				],
			},
			{
				featureType: "poi",
				elementType: "geometry",
				stylers: [
					{
						color: "#f5f5f5",
					},
					{
						lightness: 21,
					},
				],
			},
			{
				featureType: "poi.park",
				elementType: "geometry",
				stylers: [
					{
						color: "#dedede",
					},
					{
						lightness: 21,
					},
				],
			},
			{
				featureType: "road.highway",
				elementType: "geometry.fill",
				stylers: [
					{
						color: "#ffffff",
					},
					{
						lightness: 17,
					},
				],
			},
			{
				featureType: "road.highway",
				elementType: "geometry.stroke",
				stylers: [
					{
						color: "#ffffff",
					},
					{
						lightness: 29,
					},
					{
						weight: 0.2,
					},
				],
			},
			{
				featureType: "road.arterial",
				elementType: "geometry",
				stylers: [
					{
						color: "#ffffff",
					},
					{
						lightness: 18,
					},
				],
			},
			{
				featureType: "road.local",
				elementType: "geometry",
				stylers: [
					{
						color: "#ffffff",
					},
					{
						lightness: 16,
					},
				],
			},
			{
				featureType: "transit",
				elementType: "geometry",
				stylers: [
					{
						color: "#f2f2f2",
					},
					{
						lightness: 19,
					},
				],
			},
			{
				featureType: "water",
				elementType: "geometry",
				stylers: [
					{
						color: "#e9e9e9",
					},
					{
						lightness: 17,
					},
				],
			},
		],
	};

	const map = new google.maps.Map(map_element, args);

	map.markers = [];

	markers_elements.forEach((markers_element) => {
		add_marker(markers_element, map);
	});

	center_map(map);

	return map;
}

/*
 *  add_marker
 *
 *  This function will add a marker to the selected Google Map
 */
function add_marker(markers_element, map) {
	const coordinates = new google.maps.LatLng(markers_element.getAttribute("data-lat"), markers_element.getAttribute("data-lng"));

	const newURL = window.location.protocol + "//" + window.location.host + "/app/themes/gital-theme/assets/resources/marker.png";

	const icon = {
		url: newURL,
		scaledSize: new google.maps.Size(32, 50), //
		origin: new google.maps.Point(0, 0),
		anchor: new google.maps.Point(16, 46),
	};

	const place_id = markers_element.getAttribute("data-id");

	const marker = new google.maps.Marker({
		position: coordinates,
		map,
		icon,
		id: place_id,
	});

	map.markers.push(marker);

	if ("" !== markers_element.innerHTML) {
		const infowindow = new google.maps.InfoWindow({
			content: markers_element.innerHTML,
		});

		google.maps.event.addListener(marker, "click", function () {
			infowindow.open(map, marker);
		});
	}
}

/*
 *  center_map
 *
 *  This function will center the map, showing all markers attached to this map
 */
function center_map(map) {
	const bounds = new google.maps.LatLngBounds();

	let marker_counter = 0;

	map.markers.forEach((marker) => {
		if (true === marker.visible) {
			const coordinates = new google.maps.LatLng(marker.position.lat(), marker.position.lng());
			bounds.extend(coordinates);
			marker_counter++;
		}
	});

	if (marker_counter === 1) {
		map.setCenter(bounds.getCenter());
		map.setZoom(12);
	} else if (marker_counter === 0) {
		const coordinates = new google.maps.LatLng("59.334591", "18.063240");
		bounds.extend(coordinates);
		map.setCenter(bounds.getCenter());
		map.setZoom(9);
	} else {
		map.setZoom(16);
		map.fitBounds(bounds);
	}
}

/**
 * init_google_maps
 *
 * Initializes the Google Maps script
 */
function init_google_maps() {
	const map_elements = document.querySelectorAll(".g-map");

	if (!map_elements) {
		return;
	}

	window.g_google_maps = [];

	map_elements.forEach((map_element, index) => {
		window.g_google_maps[index] = new_map(map_element);
	});
}
window.g_init_google_maps = init_google_maps;
