<?php
/**
 * Global functions
 *
 * @package Gital Library
 */

namespace gital_library;

/**
 * Checks if a given user or the current logged in user is a super admin.
 *
 * @param string|int|WP_User|bool $user Optional. User's ID, username, WP_User object, or false to check the current logged-in user. Default false.
 * @return bool True if the user is a super admin, false otherwise.
 *
 * @author Gustav Gesar <gustav.gesar@gibon.se>
 *
 * @since 1.6.0
 * @version 1.0.1
 */
function is_super_admin( $user = false ) {
	if ( ! is_user_logged_in() ) {
		return false;
	}

	if ( false === $user ) {
		$current_user = wp_get_current_user();
		$user_login   = $current_user->user_login;
	} else {
		$user_object = is_numeric( $user ) ? get_user_by( 'id', $user ) : get_user_by( 'slug', $user );

		if ( ! $user_object ) {
			return false;
		}

		$user_login = $user_object->user_login;
	}

	$settings = Settings::get_instance();

	return in_array( $user_login, $settings->get( 'super_admins' ), true );
}
