<?php
/**
 * Echo with ESI
 *
 * @package Gital Library
 */

namespace gital_library;

/**
 * Echo with ESI
 *
 * @param array $function An array of parts to the function and name spaces.
 * @param array $attributes The attributes to pass along to the function with keys and values.
 *
 * Examples - Start
 *
 * First, define a callback function.
 * function g_test($example_string, $example_string_2) {
 *  return $example_string . ' ' . $example_string_2;
 * }
 *
 * Then, run the function. If the callback is inside a namespace, use an array as seen below.
 *
 * Without namespace.
 * gital_library\echo_with_esi( 'g_test', array('example_string_2'=>'ESI 2', 'example_string' => 'ESI') );
 *
 * With namespace.
 * gital_library\echo_with_esi( array('gital_theme','g_test'), array('example_string_2'=>'ESI 2', 'example_string' => 'ESI') );
 *
 * Examples - End.
 *
 * @author Gustav Gesar <gustav.gesar@gibon.se>
 *
 * @version 1.0.0
 */
function echo_with_esi( $function, $attributes = array() ) {
	$attributes['function'] = implode( '&', (array) $function );
	if ( shortcode_exists( 'esi' ) ) {
		$attributes['ttl'] = 0;
		echo do_shortcode( '[esi g_echo_with_esi' . build_attributes( $attributes, true ) . ']' );
	} else {
		echo do_shortcode( '[g_echo_with_esi' . build_attributes( $attributes, true ) . ']' );
	}
}

/**
 * The shortcode intended to be used together with echo_with_esi.
 */
add_shortcode(
	'g_echo_with_esi',
	function( $atts ) {
		$atts['function'] = str_replace( '&amp;', '&', $atts['function'] );
		$atts['function'] = explode( '&', $atts['function'] );
		$function         = implode( '\\', $atts['function'] );
		unset( $atts['function'] );
		return $function( ...$atts );
	}
);
