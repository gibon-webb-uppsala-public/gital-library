<?php
/**
 * Extended has block
 *
 * @package Gital Library
 */

namespace gital_library;


/**
 * Recursively search for a specific Gutenberg block within nested blocks, particularly targeting reusable blocks.
 *
 * It checks each block within the provided `$blocks` array. If a block has inner blocks, it recurses into them to continue
 * the search. For reusable blocks (identified by 'core/block'), it specifically checks if the block name exists in the referenced entity.
 *
 * @param array  $blocks     An array of blocks where each block may contain nested 'innerBlocks'.
 * @param string $block_name The specific block name to search for.
 *
 * @return bool Returns true if the block name is found within the array or nested arrays; false otherwise.
 *
 * @author Gustav Gesar <gustav.gesar@gibon.se>
 *
 * @version 1.0.0
 *
 * @since 3.9.0
 */
function search_reusable_blocks_within_innerblocks( $blocks, $block_name ) {
	foreach ( $blocks as $block ) {
		if ( isset( $block['innerBlocks'] ) && ! empty( $block['innerBlocks'] ) ) {
			if ( search_reusable_blocks_within_innerblocks( $block['innerBlocks'], $block_name ) ) {
				return true;
			}
		} elseif ( 'core/block' === $block['blockName'] && ! empty( $block['attrs']['ref'] ) && has_block( $block_name, $block['attrs']['ref'] ) ) {
			return true;
		}
	}

	return false;
}

/**
 * Search for a Gutenberg block by name within the current post content, including any nested within reusable blocks.
 *
 * @param string $block_name The name of the block to look for.
 *
 * @return bool Returns true if the block (or nested block) is found; false otherwise.
 *
 * @author Gustav Gesar <gustav.gesar@gibon.se>
 *
 * @version 1.0.0
 *
 * @since 3.9.0
 */
function extended_has_block( $block_name ) {
	if ( has_block( $block_name ) ) {
		return true;
	}

	if ( has_block( 'core/block' ) ) {
		$content = get_post_field( 'post_content' );
		$blocks  = parse_blocks( $content );
		return search_reusable_blocks_within_innerblocks( $blocks, $block_name );
	}

	return false;
}
