<?php
/**
 * Verify Constants
 *
 * @package Gital Library
 */

namespace gital_library;

/**
 * Verifies that the Gital Library constants are defined
 *
 * @return string
 *
 * @author Gustav Gesar <gustav.gesar@gibon.se>
 *
 * @version 1.0.1
 * @since 1.4.0
 */
function verify_constants() {
	$constants = array(
		'ADDITIONAL_SUPER_ADMINS',
		'ALLOWED_GUTENBERG_BLOCKS',
		'ALTER_IMAGE_THRESHOLD',
		'DISABLE_ADMIN_STYLING',
		'DISABLE_AUTHENTICATE_USERS_IN_REST',
		'DISABLE_GENERATOR_REMOVAL',
		'DISABLE_LOGIN_STYLING',
		'DISABLE_SANITIZE_FILENAME',
		'DISABLE_SUPPORT_PAGE',
		'DISABLED_GUTENBERG_BLOCKS',
		'DISALLOW_FILE_MODS_FOR_NON_SUPER_ADMINS',
		'ENABLE_BLOCK_PATTERNS',
		'ENABLE_COMMENTS',
		'ENABLE_EMOJI',
		'ENABLE_IMAGE_EDITOR',
		'ENABLE_JQUERY_MIGRATE',
		'ENABLE_SVG_DUOTONE_FILTERS_AND_GLOBAL_STYLES',
		'ENABLE_WP_EMBED',
		'HIDE_ADMIN_MENU_ITEMS',
		'HIDE_POSTS',
		'SEO_ANALYZE_CONTENT_FIELDS',
		'SHOW_ALL_GUTENBERG_BLOCKS',
		'SHOW_WIDGETS',
	);

	$missing           = false;
	$missing_constants = array();

	foreach ( $constants as $constant ) {
		if ( ! defined( $constant ) ) {
			$missing             = true;
			$missing_constants[] = $constant;
		}
	}

	if ( $missing ) {
		$result = "These Gital Library constants are missing: \r\n";

		foreach ( $missing_constants as $missing_constant ) {
			$result .= $missing_constant . "\r\n";
		}
	} else {
		$result = 'All Gital Library constants are defined.';
	}

	return $result;
}
