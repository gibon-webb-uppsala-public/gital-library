<?php
/**
 * Global functions
 *
 * @package Gital Library
 */

namespace gital_library;

/**
 * Check if WooCommerce is activated
 *
 * @author Gustav Gesar <gustav.gesar@gibon.se>
 *
 * @version 1.0.0
 * @since 1.6.0
 *
 * @return bool True/False if Woocommerce is active or not
 */
function is_wc_activated() {
	if ( class_exists( 'woocommerce' ) ) {
		return true;
	} else {
		return false;
	}
}

/**
 * Iconfetcher
 *
 * @author Gustav Gesar <gustav.gesar@gibon.se>
 *
 * @version 2.1.0
 * @since 1.6.0
 *
 * @param string $icon The svg file that should be rendered.
 * @param bool   $inject_on_client Set to true to use SVGInject.
 * @param array  $additional_attributes Add more attributes to the svg (Is only utilized if the svg should be injected).
 * @param string $tag The tag of the initial element (Is only utilized if the svg should be injected).
 * @param bool   $custom_url Set to true to instead of an relative path to the icon from the icon directory in the theme adding the full path.
 *
 * @return string Source of the icon
 */
function icon( $icon, $inject_on_client = false, $additional_attributes = array(), $tag = 'svg', $custom_url = false ) {
	$icon_url = $custom_url ? $icon : get_template_directory_uri() . '/assets/icons/' . $icon;
	if ( $inject_on_client ) {
		$defaults     = array(
			'width'      => 1,
			'height'     => 1,
			'svg-inject' => true,
			'src'        => $icon_url,
		);
		$attributes   = wp_parse_args( $additional_attributes, $defaults );
		$icon_element = '<' . $tag . build_attributes( $attributes, true ) . '>' . ( 'img' !== $tag ? '</' . $tag . '>' : '' );
	} else {
		$icon_element = file_get_contents( $icon_url );  // phpcs:ignore
	}
	return $icon_element;
}

/**
 * Echoes out the body tag
 *
 * @author Gustav Gesar <gustav.gesar@gibon.se>
 *
 * @param mixed $additional_classes A string or array of additional body classes.
 *
 * @version 1.4.0
 * @since 1.6.0
 */
function body( $additional_classes = '' ) {
	$g_body = array( 'g-body' );

	if ( is_front_page() ) {
		$g_body[] = 'g-body--frontpage';
	}

	if ( is_page() ) {
		$g_body[] = 'g-body--page';
	}

	if ( is_archive() || is_home() ) {
		$g_body[] = 'g-body--archive';
	}

	if ( is_404() ) {
		$g_body[] = 'g-body--404';
	}

	if ( is_single() ) {
		$g_body[] = 'g-body--single';
	}

	if ( is_search() ) {
		$g_body[] = 'g-body--search';
	}

	if ( is_paged() ) {
		$g_body[] = 'paged';
	}

	if ( is_attachment() ) {
		$g_body[] = 'attachment';
	}

	if ( is_user_logged_in() ) {
		$g_body[] = 'logged-in';
	}

	if ( is_user_logged_in() ) {
		$g_body[] = 'g-body--logged-in';
	}

	if ( is_admin_bar_showing() ) {
		$g_body[] = 'g-body--admin-bar';
	}

	if ( is_wc_activated() ) {
		$g_body[] = 'g-body--woocommerce';

		if ( is_woocommerce() || is_cart() || is_checkout() ) {
			$g_body[] = 'g-body--shop';
		}

		if ( is_cart() ) {
			$g_body[] = 'g-body--cart';
		}

		if ( is_checkout() ) {
			$g_body[] = 'g-body--checkout';
		}

		if ( is_product_category() ) {
			$g_body[] = 'g-body--product-category';
		}

		if ( is_product_tag() ) {
			$g_body[] = 'g-body--product-tag';
		}

		if ( is_product() ) {
			$g_body[] = 'g-body--product';
		}

		if ( is_account_page() ) {
			$g_body[] = 'g-body--account';
		}
	}

	$g_body = clean_classes( $g_body );

	if ( ! empty( $additional_classes ) ) {
		$g_body .= clean_classes( $additional_classes, true );
	}

	$additional_injected_classes = apply_filters( 'g_lib_additional_body_classes', '' );

	if ( ! empty( $additional_injected_classes ) ) {
		$g_body .= clean_classes( $additional_injected_classes, true );
	}

	$g_body .= clean_classes( get_body_class(), true );

	echo 'class="' . $g_body . '"';  // phpcs:ignore
}

/**
 * Clean classes
 *
 * Cleans ut a string with classes and removes double spaces
 *
 * @author Gustav Gesar <gustav.gesar@gibon.se>
 *
 * @version 1.4.0
 * @since 1.15.3
 *
 * @param mixed $classes A string with classes.
 * @param bool  $space_prefix True if a space should be added as a prefix.
 *
 * @return string Source of the file
 */
function clean_classes( $classes, $space_prefix = false ) {
	if ( is_array( $classes ) ) {
		$classes = esc_attr( join( ' ', $classes ) );
	} elseif ( is_string( $classes ) ) {
		$classes = esc_attr( $classes );
	}

	if ( empty( $classes ) ) {
		return;
	}

	$classes = trim( preg_replace( '/\s+/', ' ', $classes ) );

	if ( $space_prefix ) {
		$classes = ' ' . $classes;
	}

	return $classes;
}

/**
 * Resourcefetcher
 *
 * @author Gustav Gesar <gustav.gesar@gibon.se>
 *
 * @version 1.0.0
 * @since 1.6.0
 *
 * @param mixed $file the path and filename of the resource to fetch.
 *
 * @return string Source of the file
 */
function resource( $file ) {
	$filesrc = file_get_contents( get_template_directory_uri() . '/assets/resources/' . $file );  // phpcs:ignore
	return $filesrc;
}

/**
 * Phone number formater
 *
 * @author Gustav Gesar <gustav.gesar@gibon.se>
 *
 * @version 2.1.0
 * @since 1.6.0
 *
 * @param int $number The phone number.
 *
 * @return string Formated number
 *
 * To alter the argument, simply add the overriding configuration constant OTHER_PHONE_ARGUMENTS. Here is an example:
 *
 * // Other phone arguments.
 * Config::define(
 *  'OTHER_PHONE_ARGUMENTS',
 *  array(
 *      'formats'             => array(
 *          10 => array(
 *              'pattern'     => '/([0-9]{2})([0-9]{3})([0-9]{3})([0-9]{2})/',
 *              'replacement' => '$1 - $2 $3 $4',
 *          ),
 *      ),
 *      'remove_leading_zero' => true,
 *      'prefix'              => '+46',
 *  ),
 * );
 */
function phone( $number ) {
	$clean_number     = preg_replace( '/[^0-9]/', '', $number );
	$formatted_number = $number;

	$phone_arguments = array(
		'formats'             => array(
			7  => array(
				'pattern'     => '/([0-9]{2})([0-9]{3})([0-9]{2})/',
				'replacement' => '$1 - $2 $3',
			),
			8  => array(
				'pattern'     => '/([0-9]{3})([0-9]{2})([0-9]{3})/',
				'replacement' => '$1 - $2 $3',
			),
			9  => array(
				'pattern'     => '/([0-9]{3})([0-9]{2})([0-9]{2})([0-9]{2})/',
				'replacement' => '$1 - $2 $3 $4',
			),
			10 => array(
				'pattern'     => '/([0-9]{3})([0-9]{2})([0-9]{2})([0-9]{3})/',
				'replacement' => '$1 - $2 $3 $4',
			),
		),
		'remove_leading_zero' => false,
		'prefix'              => '',
	);

	if ( defined( 'OTHER_PHONE_ARGUMENTS' ) ) {
		if ( array_key_exists( 'formats', OTHER_PHONE_ARGUMENTS ) ) {
			foreach ( OTHER_PHONE_ARGUMENTS['formats'] as $lenght => $values ) {
				$phone_arguments['formats'][ $lenght ] = $values;
			}
		}
		if ( array_key_exists( 'prefix', OTHER_PHONE_ARGUMENTS ) ) {
			$phone_arguments['prefix'] = OTHER_PHONE_ARGUMENTS['prefix'];
		}
		if ( array_key_exists( 'remove_leading_zero', OTHER_PHONE_ARGUMENTS ) ) {
			$phone_arguments['remove_leading_zero'] = OTHER_PHONE_ARGUMENTS['remove_leading_zero'];
		}
	}

	foreach ( $phone_arguments['formats'] as $lenght => $values ) {
		if ( strlen( $clean_number ) === $lenght ) {
			$formatted_number = preg_replace( $values['pattern'], $values['replacement'], $clean_number );
		}
	}

	if ( $phone_arguments['remove_leading_zero'] ) {
		if ( substr( $formatted_number, 0, 1 ) === '0' ) {
			$formatted_number = substr( $formatted_number, 1 );
		}
	}

	$formatted_number = ( $phone_arguments['prefix'] ?? '' ) . $formatted_number;

	return esc_html( $formatted_number );
}

/**
 * ZIP-code formater
 *
 * @author Gustav Gesar <gustav.gesar@gibon.se>
 *
 * @version 1.2.0
 * @since 1.6.0
 *
 * @param int $num The unformatted zip code.
 *
 * @return string Formated zip code
 */
function zip( $num ) {
	if ( ! intval( $num ) ) {
		return;
	}

	$num = preg_replace( '/[^0-9]/', '', $num );
	$num = preg_replace( '/([0-9]{3})([0-9]{2})/', '$1 $2', $num );
	return esc_html( $num );
}

/**
 * Console logger
 *
 * @author Gustav Gesar <gustav.gesar@gibon.se>
 *
 * @version 1.0.0
 * @since 1.6.0
 *
 * @param string $message Message to log to the console.
 *
 * @return string Script with console.info message
 */
function console_log( $message ) {
	$console_message  = '<script>console.info("';
	$console_message .= wp_json_encode( $message );
	$console_message .= '")</script>';

	return $console_message;
}

/**
 * Link to button
 *
 * @author Gustav Gesar <gustav.gesar@gibon.se>
 *
 * @version 1.2.1
 * @since 1.6.0
 *
 * @param array  $link The link array from ACF.
 * @param string $size Size of the button (mini, small, large).
 * @param string $type Type of button (primary, secondary, blank).
 * @param string $position Position of button (left, center, right).
 * @param string $button_tag The tag that the button is having.
 *
 * @return string The button
 */
function link_to_button( $link, $size = 'normal', $type = 'primary', $position = 'center', $button_tag = 'a' ) {
	$size           = 'normal' !== $size ? $size : '';
	$link['target'] = '_blank' === $link['target'];

	$button = new Button( esc_html( $link['title'] ), '', '', esc_url( $link['url'] ), esc_attr( $link['target'] ), esc_attr( $type ), esc_attr( $size ), '', esc_attr( $button_tag ) );
	$button->add_wrapper( esc_attr( $position ) );
	$button = $button->get_button();
	return $button;
}

/**
 * Turn array of taxonomy obects to an string with links
 *
 * @author Gustav Gesar <gustav.gesar@gibon.se>
 *
 * @version 1.0.0
 * @since 1.6.0
 *
 * @param array  $objects An array of taxonomy objects.
 * @param string $title The title of the string.
 *
 * @return string The string with links
 */
function taxonomies( $objects, $title = '' ) {
	if ( empty( $objects ) ) {
		return;
	}
	$objects_count   = count( $objects );
	$objects_current = 1;
	$objects_out     = '<div class="g-taxonomy__item"><span>' . esc_html( $title ) . '</span>';
	foreach ( $objects as $object ) {
		$objects_out .= '<a href="';
		$objects_out .= esc_url( get_term_link( $object->term_id ) );
		$objects_out .= '">';
		$objects_out .= esc_html( $object->name );
		$objects_out .= '</a>';
		if ( $objects_count !== $objects_current ) {
			$objects_out .= ' / ';
		}
		++$objects_current;
	}
	$objects_out .= '</div>';
	return $objects_out;
}

/**
 * Wraps content inside html
 *
 * @author Gustav Gesar <gustav.gesar@gibon.se>
 *
 * @version 1.1.0
 * @since 1.6.0
 *
 * @param string $content The content to wrap.
 * @param string $tag The tag.
 * @param mixed  $classes The classes in an array or string.
 * @param string $id The ID.
 *
 * @return string The content
 */
function w( $content, $tag = 'div', $classes = '', $id = '' ) {
	if ( ! empty( $classes ) ) {
		$classes = ' class="' . clean_classes( $classes ) . '"';
	}
	if ( ! empty( $id ) ) {
		$id = ' id="' . esc_attr( $id ) . '"';
	}
	return '<' . esc_html( $tag ) . $id . $classes . '>' . $content . '</' . esc_html( $tag ) . '>';
}

/**
 * Returns opening hours from an array of days
 *
 * @author Gustav Gesar <gustav.gesar@gibon.se>
 *
 * @version 1.0.0
 * @since 1.6.0
 *
 * @param array  $days The acf array.
 * @param string $page The option page to get the days from.
 * @param string $closed_string The string to be returnd if closed.
 *
 * @return string The days
 */
function hours( $days, $page, $closed_string ) {
	if ( have_rows( $days, $page ) ) {
		$hours = '<p>';
		while ( have_rows( $days, $page ) ) {
			the_row();

			$hours .= '<span class="label days">' . esc_html( get_sub_field( 'day' ) ) . '</span>';

			if ( get_sub_field( 'open' ) ) {
				$hours .= '<span class="value">' . esc_html( get_sub_field( 'from' ) );
				$hours .= ' - ';
				$hours .= esc_html( get_sub_field( 'until' ) ) . '</span>';
			} else {
				$hours .= '<span class="value">' . esc_html( $closed_string ) . '</span>';
			}
			$hours .= '<br>';
		}
		$hours .= '</p>';
	}

	return $hours;
}

/**
 * Returns an URL of the image from an ID
 *
 * @author Gustav Gesar <gustav.gesar@gibon.se>
 *
 * @version 1.1.1
 * @since 1.22.0
 *
 * @param mixed  $images The media ID of the image or images. Accepts both a single image or an array of images.
 * @param string $size The size to be returned.
 * @param bool   $placeholder Bool or IF to the placeholder image.
 * @param bool   $bool_if_empty Return false if empty.
 *
 * @return mixed The URL to the image, false or ''
 */
function image_src( $images, $size = 'full', $placeholder = false, $bool_if_empty = false ) {

	// Control if $images is just a single int.
	if ( is_int( $images ) ) {
		// Converts the single int to an array.
		$images = array( $images );
	}

	// Control whether $images is an array of images.
	if ( is_array( $images ) ) {
		// Tries each image one by one and return a URL if one of them is valid.
		foreach ( $images as $image ) {
			if ( is_int( $image ) ) {
				$image_src_array = image_downsize( $image, $size );

				if ( is_array( $image_src_array ) ) {
					return esc_url( $image_src_array[0] );
				}
			}
		}
	}

	// If placeholder is true and DEFAULT_IMAGE_PLACEHOLDER is defined, try to get the placeholder from DEFAULT_IMAGE_PLACEHOLDER.
	if ( $placeholder && defined( 'DEFAULT_IMAGE_PLACEHOLDER' ) && is_int( DEFAULT_IMAGE_PLACEHOLDER ) ) {
		$image_src_array = image_downsize( DEFAULT_IMAGE_PLACEHOLDER, $size );

		if ( is_array( $image_src_array ) ) {
			return esc_url( $image_src_array[0] );
		}
	}

	// If all other failed, return false or ''.
	if ( $bool_if_empty ) {
		return false;
	} else {
		return '';
	}
}

/**
 * Returns the file type of the given meme-type
 *
 * @author Gustav Gesar <gustav.gesar@gibon.se>
 *
 * @version 1.0.0
 * @since 1.32.0
 *
 * @param string $meme_type The meme type.
 *
 * @return string The file type
 */
function file_type( $meme_type ) {
	switch ( $meme_type ) {
		case ( strpos( $meme_type, 'wordprocessing' ) !== false ):
			return 'text';
		case ( strpos( $meme_type, 'text' ) !== false ):
			return 'text';
		case ( strpos( $meme_type, 'pages' ) !== false ):
			return 'text';
		case ( strpos( $meme_type, 'csv' ) !== false ):
			return 'sheet';
		case ( strpos( $meme_type, 'sheet' ) !== false ):
			return 'sheet';
		case ( strpos( $meme_type, 'numbers' ) !== false ):
			return 'sheet';
		case ( strpos( $meme_type, 'presentation' ) !== false ):
			return 'presentation';
		case ( strpos( $meme_type, 'keynote' ) !== false ):
			return 'presentation';
		case ( strpos( $meme_type, 'image' ) !== false ):
			return 'image';
		case ( strpos( $meme_type, 'video' ) !== false ):
			return 'video';
		case ( strpos( $meme_type, 'zip' ) !== false ):
			return 'compressed';
		case ( strpos( $meme_type, 'tar' ) !== false ):
			return 'compressed';
		case ( strpos( $meme_type, 'tar' ) !== false ):
			return 'compressed';
		default:
			return 'document';
	}
}

/**
 * Converts an array to HTML attributes
 *
 * The function also handles deeper arrays for certain attributes such as style and class.
 *
 * @author Gustav Gesar <gustav.gesar@gibon.se>
 *
 * @version 2.1.0
 * @since 1.39.0
 *
 * @param array $attributes An array with attribute pairs. The value of each pair can be an array or a string.
 * @param bool  $space_prefix Adds a space before the attributes.
 *
 * @return string The HTML attributes.
 *
 * Example:
 * $attributes = array(
 *     'id'    => 'example',
 *     'class' => array(
 *         'example',
 *         'example_2',
 *         'example_3',
 *      ),
 *      'style' => array(
 *         'display' => 'block',
 *         'color'   => 'green',
 *      ),
 * );
 */
function build_attributes( $attributes, $space_prefix = false ) {
	if ( empty( $attributes ) || ! is_array( $attributes ) ) {
		return '';
	}

	$attribute_pairs = array();

	foreach ( $attributes as $key => $value ) {
		if ( is_int( $key ) && ! empty( $value ) && is_string( $value ) ) {
			$attribute_pairs[] = $value;
			continue;
		}

		if ( is_string( $key ) && ( is_int( $value ) || is_string( $value ) || is_bool( $value ) ) ) {
			if ( 'id' === $key && empty( $value ) ) {
				continue;
			}
			$attribute_pairs[] = esc_attr( $key ) . "='" . esc_attr( $value ) . "'";
			continue;
		}

		if ( is_array( $value ) && ! empty( $value ) ) {
			if ( 'style' === $key ) {
				$style = '';
				foreach ( $value as $css_property => $css_value ) {
					if ( is_int( $css_property ) || empty( $css_property ) || empty( $css_value ) && 0 !== $css_value ) {
						continue;
					}
					$css_value = str_replace( "'", '"', $css_value );
					$style    .= esc_attr( $css_property ) . ':' . esc_attr( $css_value ) . ';';
				}
				if ( empty( $style ) ) {
					continue;
				}
				$attribute_pairs[] = "style='" . $style . "'";
				continue;
			}
			$attribute_pairs[] = esc_attr( $key ) . "='" . clean_classes( $value ) . "'";
		}
	}

	$html_attributes = join( ' ', $attribute_pairs );

	if ( $space_prefix ) {
		$html_attributes = ' ' . $html_attributes;
	}

	return $html_attributes;
}

/**
 * Log to debug.log
 *
 * @param mixed $content The log message.
 *
 * @author Gustav Gesar <gustav.gesar@gibon.se>
 *
 * @version 1.0.1
 * @since 2.12.0
 */
function log( $content ) {
	if ( true === WP_DEBUG ) {
		if ( is_array( $content ) || is_object( $content ) ) {
			error_log( print_r( $content, true ) ); // phpcs:ignore
		} else {
			error_log( $content );  // phpcs:ignore
		}
	}
}

/**
 * Render an mailto: e-mail in a spam safe way
 *
 * @param string $mail The mail adress.
 * @param string $additional_classes Additional classes.
 *
 * @author Gustav Gesar <gustav.gesar@gibon.se>
 *
 * @version 1.1.0
 * @since 2.17.0
 */
function safe_email( $mail, $additional_classes = '' ) {
	if ( ! is_email( $mail ) ) {
		return;
	}
	$mail_address_parts = explode( '@', $mail );
	$id                 = 'g-safe-email--' . wp_rand( 1000, 9999 );
	$content            = '<a id="' . $id . '" class="g-safe-email' . clean_classes( $additional_classes, true ) . '" username="' . esc_attr( $mail_address_parts[0] ) . '" domain="' . esc_attr( $mail_address_parts[1] ) . '">' . __( 'Spam protected', 'gital-library' );
	$content           .= '<script>window.addEventListener("load", () => { g_safe_email("' . esc_attr( $id ) . '") });</script>';
	$content           .= '</a>';
	return $content;
}

/**
 * Get all field values from an ACF Group for the current post
 *
 * @param string $field_group The field group key or ID.
 * @param bool $filter Set to tru if empty values should be filtered out.
 *
 * @author Gustav Gesar <gustav.gesar@gibon.se>
 *
 * @version 1.2.0
 * @since 2.59.0
 */
function get_field_group_values( $field_group, $filter = false, $id = false ) {
	$fields = acf_get_fields( $field_group );
	$values = array();
	if ( ! is_array( $fields ) ) {
		return;
	}
	foreach ( $fields as $field ) {
		$values[ $field['name'] ?? '' ] = get_field( $field['key'] ?? '', $id );
	}
	$values = $filter ? array_filter( $values ) : $values;
	return $values;
}

/**
 * Produces cleaner filenames for uploads
 *
 * @author Gustav Gesar <gustav.gesar@gibon.se>
 *
 * @param string $filename The filename to sanitize.
 *
 * @return string
 *
 * @version 1.0.0
 * @since 3.3.0
 */
function sanitize_file_name( $filename ) {
	// Remove accents from the filename.
	$sanitized_filename = remove_accents( $filename );

	// Replace spaces, %20, and underscores with hyphens in one step.
	$sanitized_filename = str_replace( array( ' ', '%20', '_' ), '-', $sanitized_filename );

	// Remove all characters that are not alphanumeric, dots, hyphens or spaces.
	$sanitized_filename = preg_replace( '/[^A-Za-z0-9-\s\.]/', '', $sanitized_filename );

	// Replace multiple consecutive dots with a single dot, except for the last one (file extension).
	$sanitized_filename = preg_replace( '/\.(?=.*\.)/', '', $sanitized_filename );

	// Replace multiple consecutive hyphens with a single hyphen.
	$sanitized_filename = preg_replace( '/-+/', '-', $sanitized_filename );

	// Ensure no hyphen directly before a dot.
	$sanitized_filename = str_replace( '-.', '.', $sanitized_filename );

	// Convert the filename to lowercase.
	$sanitized_filename = strtolower( $sanitized_filename );

	return $sanitized_filename;
}

/**
 * Parse args.
 *
 * This function is similar to wp_parse_args with the difference that it'll handle array recursively.
 *
 * @param array|string $args Value to merge with $defaults.
 * @param array        $defaults Array that serves as the defaults.
 *
 * @return array Merged user defined values with defaults.
 *
 * @author Gustav Gesar <gustav.gesar@gibon.se>
 *
 * @version 1.0.0
 * @since 3.10.0
 */
function parse_args( $args, $defaults ) {
	$new_args = (array) $defaults;

	foreach ( $args as $key => $value ) {
		if ( is_array( $value ) && isset( $new_args[ $key ] ) ) {
			$new_args[ $key ] = parse_args( $value, $new_args[ $key ] );
		} else {
			$new_args[ $key ] = $value;
		}
	}

	return $new_args;
}

/**
 * Get the server IP
 *
 * @return string|bool The server IP or false if the server IP could not be determined.
 *
 * @author Gustav Gesar <gustav.gesar@gibon.se>
 *
 * @version 1.0.0
 */
function get_server_ip() {
	$server_ip = false;

	if ( isset( $_SERVER['SERVER_ADDR'] ) && ! empty( $_SERVER['SERVER_ADDR'] ) ) {
		$server_ip = $_SERVER['SERVER_ADDR'];
	} else {
		$hostname_ip = gethostbyname( gethostname() );
		if ( ! empty( $hostname_ip ) ) {
			$server_ip = $hostname_ip;
		}
	}

	if ( $server_ip && ! filter_var( $server_ip, FILTER_VALIDATE_IP ) ) {
		\gital_library\log( print_r( 'Invalid server IP address: ' . $server_ip, true ) );
		return false;
	}

	return $server_ip;
}

/**
 * Check if the server is an allowed server
 *
 * @return boolean True if the server is an allowed server.
 *
 * @author Gustav Gesar <gustav.gesar@gibon.se>
 *
 * @version 1.0.0
 */
function is_allowed_server(): bool {
	$server_ip       = get_server_ip();
	$allowed_servers = Settings::get_instance()->get( 'allowed_servers' );
	return $server_ip && $allowed_servers && in_array( $server_ip, $allowed_servers, true );
}

/**
 * Check if the server is a production server
 *
 * @return boolean True if the server is a production server.
 *
 * @author Gustav Gesar <gustav.gesar@gibon.se>
 *
 * @version 1.0.0
 */
function is_production_server(): bool {
	$server_ip          = get_server_ip();
	$production_servers = Settings::get_instance()->get( 'production_servers' );
	return $server_ip && $production_servers && in_array( $server_ip, $production_servers, true );
}
