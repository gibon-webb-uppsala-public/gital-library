<?php
/**
 * Media modal component
 *
 * @package Gital Library
 */

namespace gital_library;

/**
 * Media Modal
 * Author: Gustav Gesar
 *
 * @version 1.2.0
 * @since 2.3.0
 */
class Media_Modal {
	/**
	 * The id of the media modal
	 *
	 * @var string
	 */
	protected $id;

	/**
	 * Media modal
	 *
	 * This component will return the id of the modal and can then be used by the js function load_media_modal();
	 *
	 * @param string $additional_classes Additional classes.
	 *
	 * @author Gustav Gesar <gustav.gesar@gibon.se>
	 *
	 * @version 1.1.0
	 * @since 1.0.0
	 */
	public function __construct( $additional_classes = '' ) {
		$this->id = 'g-media-modal--' . wp_rand( 1000, 9999 );
		$content  = '<video class="g-media-modal__item g-media-modal__item--video" style="display:none;"></video>';
		$content .= '<img class="g-media-modal__item g-media-modal__item--image" style="display:none;">';
		$content .= '<div class="g-media-modal__item g-media-modal__item--iframe"><iframe allowfullscreen allowtransparency allow="autoplay" style="display:none;"></iframe></div>';
		$modal    = new Modal( $content, $this->id, clean_classes( 'g-media-modal ' . $additional_classes, true ), '', true, true, false );
		$modal->the_modal_to_footer();
	}

	/**
	 * Returns the modal id
	 *
	 * @return $this->id The ID of the modal.
	 *
	 * @author Gustav Gesar <gustav.gesar@gibon.se>
	 *
	 * @version 1.0.0
	 * @since 1.0.0
	 */
	public function get_id() {
		return $this->id;
	}
}
