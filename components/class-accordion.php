<?php
/**
 * Accordion component
 *
 * @package Gital Library
 */

namespace gital_library;

/**
 * Accordion
 * Author: Gustav Gesar
 *
 * @param string $id The id of the accordion.
 * @param mixed  $class Additional classes to the accordion.
 *
 * @version 1.3.1
 * @since 1.8.0
 *
 * Example usage:
 * $accordion = new gital_library\Accordion('ID', 'Class');
 * $accordion->add_row($title, $content);
 * $accordion->get_accordion();
 */
class Accordion {
	/**
	 * The variables of the accordion
	 *
	 * @var string
	 */
	protected $variables;

	/**
	 * The opening of the wrapper to the accordion
	 *
	 * @var string
	 */
	protected $wrapper_start;

	/**
	 * The content of the the accordion
	 *
	 * @var string
	 */
	protected $content;

	/**
	 * The closing of the wrapper to the accordion
	 *
	 * @var string
	 */
	protected $wrapper_end;

	/**
	 * Construct
	 *
	 * @param string $id The ID of the accordion wrapper.
	 * @param string $class The class of the accordion wrapper.
	 *
	 * @author Gustav Gesar <gustav.gesar@gibon.se>
	 *
	 * @version 1.0.0
	 */
	public function __construct( $id = '', $class = '' ) {
		if ( ! empty( $id ) ) {
			$this->variables['id'] = $id;
		}
		if ( ! empty( $class ) ) {
			$this->variables['class'] = clean_classes( array( 'g-accordion', $class ), true );
		}
		$this->wrapper_start = '<div' . build_attributes( $this->variables, true ) . '>';
		$this->content       = '';
		$this->wrapper_end   = '</div>';
	}

	/**
	 * Adds a row to the accordion
	 *
	 * @param string $title The title of the accordion row.
	 * @param string $content The content.
	 * @param array  $attributes Attributes of the accordion row.
	 *
	 * @author Gustav Gesar <gustav.gesar@gibon.se>
	 *
	 * @version 1.3.1
	 * @since 1.8.0
	 */
	public function add_row( $title, $content, $attributes = array() ) {
		$this->content .= '<div class="g-accordion__item"' . build_attributes( $attributes, true ) . '>';

		$this->content .= '<div class="g-accordion__header g-accordion__header--collapsed"><h3>' . wp_kses( $title, 'post' ) . '</h3>' . icon( 'arrows/arrow_right.svg' ) . '</div>';
		$this->content .= '<div class="g-accordion__content g-accordion__content--collapsed" style="display:none;">' . wp_kses( $content, 'post' ) . '</div>';

		$this->content .= '</div>';
	}

	/**
	 * Renders the accordion
	 *
	 * @author Gustav Gesar <gustav.gesar@gibon.se>
	 *
	 * @version 1.0.1
	 * @since 1.8.0
	 */
	public function the_accordion() {
		echo $this->get_accordion(); // phpcs:ignore
	}

	/**
	 * Returns the accordion
	 *
	 * @author Gustav Gesar <gustav.gesar@gibon.se>
	 *
	 * @version 1.0.1
	 * @since 1.8.0
	 */
	public function get_accordion() {
		$accordion = $this->wrapper_start . $this->content . $this->wrapper_end;
		return $accordion;
	}
}
