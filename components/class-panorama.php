<?php
/**
 * Panorama component
 *
 * @package Gital Library
 */

namespace gital_library;

/**
 * Panorama
 *
 * Defines and renders a panorama
 *
 * @author Gustav Gesar <gustav.gesar@gibon.se>
 *
 * @version 1.2.0
 * @since 1.6.0
 */
class Panorama {
	/**
	 * Construct
	 *
	 * @param string $title The title of the panorama.
	 * @param string $image The image of the panorama.
	 * @param string $subtitle The subtitle of the panorama.
	 * @param string $additional Additional text/content of the panorama.
	 *
	 * @author Gustav Gesar <gustav.gesar@gibon.se>
	 *
	 * @version 1.0.0
	 */
	public function __construct( $title = '', $image = '', $subtitle = '', $additional = '' ) {
		$settings = Settings::get_instance();

		$classes    = array(
			'g-panorama',
			'g-align-full',
		);
		$attributes = array();

		if ( empty( $image ) ) {
			$classes[] = 'g-panorama--no-image';
		} else {
			$classes[]           = 'g-panorama--image';
			$attributes['style'] = 'background-image: url(' . $image . ')';
		}

		$attributes['class'] = clean_classes( $classes );

		include $settings->get( 'path_views' ) . 'panorama.php';
	}
}
