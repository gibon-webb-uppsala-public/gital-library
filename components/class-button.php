<?php
/**
 * Button component
 *
 * @package Gital Library
 */

namespace gital_library;

/**
 * Button
 *
 * @author Gustav Gesar <gustav.gesar@gibon.se>
 *
 * @version 1.3.0
 * @since 0.1.6
 *
 * Example usage:
 * $button = new gital_library\Button('Text', 'ID', 'Class', 'www.gibon.se', true, 'primary', 'standard');
 * $button->add_button('Text', 'ID', 'Class', 'www.gibon.se', true, 'primary', 'standard');
 * $button->add_wrapper('right', 'g-margin-top');
 * $button->the_button();
 */
class Button {

	/**
	 * Content
	 *
	 * @var string $content
	 */
	protected $content;

	/**
	 * Construct
	 *
	 * @param string $text The text for the button.
	 * @param string $id The ID of the button.
	 * @param mixed  $class Additional classes to the button.
	 * @param string $link The link of the button.
	 * @param bool   $new_tab Adds target _blank if its a link.
	 * @param string $style Adds a style to the button.
	 * @param string $size Adds a size to the button.
	 * @param string $on_click Adds an onClick event.
	 * @param string $button_tag The tag of the button, defaults to a.
	 *
	 * @author Gustav Gesar <gustav.gesar@gibon.se>
	 *
	 * @version 1.0.0
	 */
	public function __construct( $text, $id = '', $class = '', $link = '', $new_tab = false, $style = '', $size = '', $on_click = '', $button_tag = 'a' ) {
		$this->add_button( $text, $id, $class, $link, $new_tab, $style, $size, $on_click, true, $button_tag );
	}

	/**
	 * Creates a (new) button
	 *
	 * @param string  $text The text for the button.
	 * @param string  $id The ID of the button.
	 * @param mixed   $class Additional classes to the button.
	 * @param string  $link The link of the button.
	 * @param bool    $new_tab Adds target _blank if its a link.
	 * @param string  $style Adds a style to the button.
	 * @param string  $size Adds a size to the button.
	 * @param string  $on_click Adds an onClick event.
	 * @param boolean $construct If true, the button overwrites the content of the button object.
	 * @param string  $button_tag The tag of the button, defaults to a.
	 *
	 * @author Gustav Gesar <gustav.gesar@gibon.se>
	 *
	 * @version 1.1.0
	 * @since 0.1.6
	 */
	public function add_button( $text, $id = '', $class = '', $link = '', $new_tab = 'false', $style = '', $size = '', $on_click = '', $construct = false, $button_tag = 'a' ) {
		$classes    = array( 'g-button' );
		$attributes = array();

		if ( ! empty( $class ) ) {
			$classes[] = $class;
		}

		if ( ! empty( $style ) ) {
			$classes[] = 'g-button--' . $style;
		}

		if ( ! empty( $size ) ) {
			$classes[] = 'g-button--' . $size;
		}

		if ( ! empty( $link ) ) {
			$attributes['href'] = esc_url( $link );

			if ( $new_tab ) {
				$attributes['target'] = '_blank';
			}
		}

		if ( ! empty( $on_click ) ) {
			$attributes['onclick'] = $on_click;
		}

		if ( ! empty( $id ) ) {
			$attributes['id'] = $id;
		}

		$attributes['class'] = clean_classes( $classes );

		$button  = '<' . esc_html( $button_tag ) . build_attributes( $attributes, true ) . '>';
		$button .= esc_html( $text );
		$button .= '</' . esc_html( $button_tag ) . '>';

		if ( $construct ) {
			$this->content = $button;
		} else {
			$this->content .= $button;
		}
	}

	/**
	 * Creates a wrapper
	 *
	 * @param string $position The position of the content, left, right or center. Defaults to center.
	 * @param string $class Additional classes to the wrapper.
	 *
	 * @author Gustav Gesar <gustav.gesar@gibon.se>
	 *
	 * @version 1.1.0
	 * @since 0.1.6
	 */
	public function add_wrapper( $position = 'center', $class = '' ) {
		$button  = '<div class="' . clean_classes( array( 'g-buttons', 'g-buttons--' . $position, $class ) ) . '">';
		$button .= $this->content;
		$button .= '</div>';

		$this->content = $button;
	}

	/**
	 * Renders the button(s)
	 *
	 * @author Gustav Gesar <gustav.gesar@gibon.se>
	 *
	 * @version 1.1.0
	 * @since 0.1.6
	 */
	public function the_button() {
		echo $this->get_button(); // phpcs:ignore
	}

	/**
	 * Returns the button(s)
	 *
	 * @return $this->content
	 *
	 * @author Gustav Gesar <gustav.gesar@gibon.se>
	 *
	 * @version 1.0.0
	 * @since 0.1.6
	 */
	public function get_button() {
		return $this->content;
	}

	// Aliases for backwards compatibility.
	public function addButton( ...$args ) {
		$this->add_button( ...$args );
	}

	public function addWrapper( ...$args ) {
		$this->add_wrapper( ...$args );
	}

	public function getButton() {
		return $this->get_button();
	}

	public function theButton() {
		$this->the_button();
	}
}
