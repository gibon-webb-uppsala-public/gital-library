<?php
/**
 * Files component
 *
 * @package Gital Library
 */

namespace gital_library;

/**
 * Files
 * Author: Gustav Gesar
 *
 * @param string $id
 * @param string $class
 *
 * @version 1.2.1
 * @since 1.32.0
 *
 * Example usage:
 * $files = new gital_library\Files('ID', 'Class');
 * $files->add_file( $file_id );
 * $files->get_files();
 */
class Files {
	/**
	 * The variables of the files
	 *
	 * @var string
	 */
	protected $variables;

	/**
	 * The opening of the wrapper to the files
	 *
	 * @var string
	 */
	protected $wrapper_start;

	/**
	 * The content of the the files
	 *
	 * @var string
	 */
	protected $content;

	/**
	 * The closing of the wrapper to the files
	 *
	 * @var string
	 */
	protected $wrapper_end;

	/**
	 * Construct
	 *
	 * @param string $id The ID of the files wrapper.
	 * @param string $class The class of the files wrapper.
	 *
	 * @author Gustav Gesar <gustav.gesar@gibon.se>
	 *
	 * @version 1.0.1
	 */
	public function __construct( $id = '', $class = '' ) {
		if ( ! empty( $id ) ) {
			$this->variables['id'] = $id;
		}
		if ( ! empty( $class ) ) {
			$this->variables['class'] = clean_classes( array( 'g-files', $class ), true );
		}
		$this->wrapper_start = '<div' . build_attributes( $this->variables, true ) . '>';
		$this->content       = '';
		$this->wrapper_end   = '</div>';
	}

	/**
	 * Adds a row to the file
	 *
	 * @param int   $file_id The ID of the file.
	 * @param array $args Additional arguments to the file.
	 *
	 * @author Gustav Gesar <gustav.gesar@gibon.se>
	 *
	 * @version 1.2.1
	 * @since 1.32.0
	 */
	public function add_file( $file_id, $args = array() ) {
		$args_defaults = array(
			'target_blank' => false,
			'download'     => false,
		);
		$settings      = wp_parse_args( $args, $args_defaults );
		$attributes    = array(
			'href'  => esc_url( wp_get_attachment_url( $file_id ) ),
			'class' => clean_classes(
				array(
					'g-files__item',
					'g-file',
				)
			),
		);
		if ( $settings['target_blank'] ) {
			$attributes['target'] = '_blank';
		}
		if ( $settings['download'] ) {
			$attributes['download'] = '';
		}

		$file_path       = get_attached_file( $file_id );
		$file_title      = get_the_title( $file_id );
		$file_name       = basename( $file_path );
		$file_icon_idle  = icon( 'file-types/' . file_type( get_post_mime_type( $file_id ) ) . '.svg' );
		$file_icon_hover = icon( 'general/download.svg' );
		$file_size       = size_format( filesize( get_attached_file( $file_id ) ), 2 );

		$this->content .= '<a' . build_attributes( $attributes, true ) . '>';
		$this->content .= w( $file_icon_idle, 'div', 'g-file__icon g-file__icon--idle' );
		$this->content .= w( $file_icon_hover, 'div', 'g-file__icon g-file__icon--hover' );
		$this->content .= w( esc_html( $file_title ), 'h4', 'g-file__title' );
		$this->content .= w( esc_html( $file_name ), 'span', 'g-file__name' );
		$this->content .= w( esc_html( $file_size ), 'small', 'g-file__size' );
		$this->content .= '</a>';
	}

	/**
	 * Renders the files
	 *
	 * @author Gustav Gesar <gustav.gesar@gibon.se>
	 *
	 * @version 1.1.0
	 * @since 1.32.0
	 */
	public function the_files() {
		echo $this->get_files(); // phpcs:ignore
	}

	/**
	 * Returns the files
	 *
	 * @author Gustav Gesar <gustav.gesar@gibon.se>
	 *
	 * @version 1.0.0
	 * @since 1.32.0
	 */
	public function get_files() {
		$files = $this->wrapper_start . $this->content . $this->wrapper_end;
		return $files;
	}
}
