<?php
/**
 * Modal component
 *
 * @package Gital Library
 */

namespace gital_library;

/**
 * Modal
 * Author: Gustav Gesar
 *
 * @version 1.8.2
 * @since 1.6.0
 *
 * Example usage:
 * $modal = new gital_library\Modal('Content', 'ID', 'Class', 'Title', true, true, false);
 * $modal->the_modal_to_footer();
 */
class Modal {

	/**
	 * Content
	 *
	 * @var string $content
	 */
	protected $content;

	/**
	 * Construct
	 *
	 * @param string $content The content of the modal. Beware, this string is not escaped.
	 * @param string $id The ID of the modal.
	 * @param mixed  $class The classes of the modal.
	 * @param string $title The title of the modal.
	 * @param bool   $close_by_x Close the modal with the X.
	 * @param bool   $close_by_outside Close the modal by clicking outside.
	 * @param bool   $show_on_load Show the modal when the page loads.
	 *
	 * @author Gustav Gesar <gustav.gesar@gibon.se>
	 *
	 * @version 1.1.1
	 */
	public function __construct( $content, $id, $class = '', $title = '', $close_by_x = true, $close_by_outside = true, $show_on_load = false ) {
		$attributes = array(
			'id'    => $id,
			'style' => 'display: none;',
			'class' => clean_classes( array( $class, 'g-modal' ) ),
		);

		$modal = '<div' . build_attributes( $attributes, true ) . '>';
		if ( $close_by_outside ) {
			$modal .= '<div class="g-modal__close-outside"></div>';
		}
		$modal .= '<div class="g-modal__content">';
		$modal .= ! empty( $title ) ? '<h2>' . $title . '</h2>' : '';
		if ( $close_by_x ) {
			$modal .= '<div class="g-modal__x-closer"></div>';
		}
		$modal .= $content;
		$modal .= '</div></div>';

		if ( $show_on_load ) {
			$modal .= '<script>window.addEventListener("load", () => { g_show_modal_from_id("' . esc_attr( $id ) . '") } );</script>';
		}

		$this->content = $modal;
	}

	/**
	 * Renders the modal
	 *
	 * @author Gustav Gesar <gustav.gesar@gibon.se>
	 *
	 * @version 1.1.1
	 * @since 1.6.0
	 */
	public function the_modal() {
		echo $this->get_modal(); // phpcs:ignore
	}

	/**
	 * Returns the modal
	 *
	 * @author Gustav Gesar <gustav.gesar@gibon.se>
	 *
	 * @version 1.0.0
	 * @since 1.6.0
	 */
	public function get_modal() {
		return $this->content;
	}

	/**
	 * Renders the modal in the footer
	 *
	 * @author Gustav Gesar <gustav.gesar@gibon.se>
	 *
	 * @version 1.0.0
	 * @since 1.6.0
	 */
	public function the_modal_to_footer() {
		add_action( 'wp_footer', array( $this, 'the_modal' ), 100 );
	}

	// Aliases for backwards compatibility.
	public function echoModal() {
		$this->the_modal();
	}

	public function theModal() {
		$this->the_modal();
	}

	public function toFooter() {
		$this->the_modal_to_footer();
	}

	public function theModalToFooter() {
		$this->the_modal_to_footer();
	}
}
