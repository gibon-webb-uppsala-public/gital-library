=== Gital Library ===
Contributors: gibonadmin
Requires at least: 5.0
Tested up to: 6.4
Requires PHP: 7.4
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

The Gital Library is made with passion in Uppsala, Sweden. The plugin is adding a library of functions to the page, essential for the page to work. If you'd like support, please contact us at webb@gibon.se.

== Description ==
The Gital Library is made with passion in Uppsala, Sweden. The plugin is adding a library of functions to the page, essential for the page to work. If you'd like support, please contact us at webb@gibon.se.

== Installation ==
Add the plugin and add the settings given by Gibon to the config file.

== Changelog ==

= 3.19.0 - 2024.09.05 =
* The storage check now only applies to production servers.

= 3.18.4 - 2024.09.04 =
* The plugin now utilizes the new repo.

= 3.17.3 - 2024.09.03 =
* Update: The storage check cli function now takes the STORAGE_THRESHOLD as a fallback.
* Update: Added cron event to mail home if the storage is getting low. It uses the STORAGE_THRESHOLD setting.
* Update: Added FATAL_ERROR_FILTER setting to filter the fatal errors.

= 3.16.1 - 2024.09.02 =
* Update: New function get_server_ip.
* Update: New function is_production_server.
* Update: New function is_allowed_server.
* Update: Renamed class Clean_And_Dequeue to Optimize.

= 3.15.0 - 2024.08.30 =
* Update: Added error handler to send email home on Fatal errors if the server is managed.

= 3.14.0 - 2024.08.29 =
* Update: New CLI function to clean the ACF fields.

= 3.13.8 - 2024.08.19 =
* Update: Introduction to the new Settings class to make defining settings faster and easier.

= 3.12.3 - 2024.08.07 =
* Update: Upgraded media modal that deprecates the images modal. The new system now handles internal and external video.

= 3.11.3 - 2024.06.20 =
* Update: Added function to notify home if any plugins gets automatically updated.

= 3.10.1 - 2024.06.17 =
* Bugfix: Image modal does not use dynamic properties anymore.

= 3.10.0 - 2024.06.05 =
* Update: Added function parse_args.

= 3.9.1 - 2024.05.07 =
* Update: The svg injector now loads with DOMContentLoaded instead of load.

= 3.9.0 - 2024.05.03 =
* Update: Added extended_has_block.

= 3.8.0 - 2024.04.30 =
* Update: Added more conditions to the body function.

= 3.7.1 - 2024.04.29 =
* Update: Added the option to enable block patterns but still hide the core block patterns.

= 3.6.0 - 2024.04.23 =
* Update: Added functionality for additional css in the admin area.

= 3.5.3 - 2024.04.08 =
* Bugfix: Cleaned up the modal script and added corrections to remove event listeners.

= 3.5.2 - 2024.04.08 =
* Bugfix: The duplicate function did not get the metadata correctly.

= 3.5.1 - 2024.04.04 =
* Bugfix: Forgot to compile 3.4.1.

= 3.5.0 - 2024.03.28 =
* Update: The get_field_group_values function now accepts an ID.

= 3.4.1 - 2024.03.16 =
* Update: The SVG Injector can now be reached through window.g_inject_svgs().
* Update: The icon function now supports full URLs instead of just relative paths to the theme.

= 3.3.1 - 2024.03.05 =
* Bugfix: Renamed the constant DISABLE_AUTENTICATE_USERS_IN_REST to DISABLE_AUTHENTICATE_USERS_IN_REST.
* Update: Moved the file sanitation function to global and the filter to the security class.

= 3.2.0 - 2024.02.16 =
* Bugfix: The duplicate post function is not forcing a slug when cloning anymore.
* Update: The duplicate post function now uses native wp functions instead of wpdb when cloning metadata.

= 3.1.1 - 2024.02.02 =
* Update: Added function to disable the WP Image Editor. It can be enabled again with ENABLE_IMAGE_EDITOR.

= 3.0.4 - 2024.02.01 =
* Bugfix: The support form view did not use get_instance of Managed_Hosting.

= 3.0.3 - 2024.01.30 =
* Bugfix: The CLI commands where not registered after the plugin started to utilize Autoload.

= 3.0.2 - 2024.01.29 =
* Bugfix: Old versions of the theme could not reach g_button, g_panorama and g_modal.

= 3.0.1 - 2024.01.25 =
* Bugfix: class-files.php had a bug with an unwanted ".

= 3.0.0 - 2024.01.16 =
* Update: Added Autoload.
* Update: Added Singleton feature to the classes.

= 2.63.0 - 2024.01.15 =
* Update: Added cache management.

= 2.62.1 - 2024.01.08 =
* Bugfix: The image_modal function is now using firstElementChild istället för firstChild.

= 2.62.0 - 2023.12.20 =
* Update: Updated styling to make the ACF Buttons, Button Groups and Switch blend in.

= 2.61.0 - 2023.12.08 =
* Update: The duplicate post function is now disabled on acf-field-group.

= 2.60.0 - 2023.12.01 =
* Update: Added filter function to get_field_group_values().

= 2.59.0 - 2023.12.01 =
* Update: Added function get_field_group_values().

= 2.58.0 - 2023.11.09 =
* Update: Updated list with blocks to unset.

= 2.57.0 - 2023.09.22 =
* Update: Function build_attributes and clean_classes is updated with minor optimizations.

= 2.56.0 - 2023.09.20 =
* Update: The icon fetcher now offer SVG Inject as an alternative.

= 2.55.1 - 2023.09.12 =
* Update: The build_attributes function is updated to version 2.0 and accepts arrays as value in the pairs.

= 2.54.1 - 2023.08.30 =
* Update: Updated style and script for the image modal.

= 2.53.0 - 2023.08.29 =
* Update: Added function to disable the custom css.

= 2.52.0 - 2023.08.24 =
* Update: New function echo_with_esi().

= 2.51.2 - 2023.08.23 =
* Bugfix: Fixed a bug in the modal where the class g-modal where not added if no additonal class exists.

= 2.51.1 - 2023.08.22 =
* Update: Added Polyfills for PHP 8.0-8.2.

= 2.50.0 - 2023.08.21 =
* Update: Added PHP 8.2 support.

= 2.49.1 - 2023.08.17 =
* Bugfix: The accordion component did not render the attributes correctly and the arrow could shrink if the title where too big.

= 2.49.0 - 2023.08.16 =
* Update: Added the settings constant ADDITIONAL_PRODUCTION_SERVER.

= 2.48.0 - 2023.08.16 =
* Update: Added patch for file upload overflow in iOS Safari.

= 2.47.0 - 2023.08.16 =
* Update: Added patch for the modal flicker.

= 2.46.0 - 2023.08.08 =
* Update: Added support to WC 8.0.
* Update: Updated dependencies.

= 2.45.1 - 2023.08.08 =
* Bugfix: The duration property of the css variable where corrupt.

= 2.45.0 - 2023.07.21 =
* Update: Added the patch class.
* Update: Added a patch to unhide the crucial UI elements of Google Maps.
* Update: The UI elements of Google Maps is now hidden from JS instead of CSS.

= 2.44.2 - 2023.06.29 =
* Update: The modal script now declares the body constant with document.body.

= 2.44.0 - 2023.06.27 =
* Update: New class Duplicate_Post.

= 2.43.0 - 2023.06.20 =
* Update: New function echo_with_esi that makes using ESI much easier.
* Update: The body() now has a filter.

= 2.42.3 - 2023.06.19 =
* Bugfix: The Accordion title now uses wp_kses post instead of esc_html.

= 2.42.2 - 2023.06.19 =
* Bugfix: The link_to_button function did not work.

= 2.42.1 - 2023.06.14 =
* Update: Added better escaping functions.
* Update: Made the code more dry. Used build_attributes and clean_classes more efficiently.

= 2.41.2 - 2023.06.09 =
* Bugfix: Fixed a bug that caused the hide animation on the modal to glitch.

= 2.41.1 - 2023.06.09 =
* Bugfix: Fixed a bug that caused the icons on the accordion header to shrink if the title grew to large.

= 2.41.0 - 2023.05.26 =
* Update: The disableing of comments is now more robust.

= 2.40.1 - 2023.05.04 =
* Bugfix: Fix error caused by version 2.36.5 that caused the Info Window to not appear.

= 2.39.3 - 2023.04.19 =
* Update: The function to hide menu items in wp-admin is now also respecting G_LIB_SUPER_ADMINS.

= 2.38.0 - 2023.04.18 =
* Update: The phone function can now take arguments.

= 2.37.0 - 2023.04.17 =
* Update: New function to disallow the file mod capabilities if the user is not super admin.

= 2.36.8 - 2023.04.11 =
* Bugfix: Exclusion of the Google Maps script from Litespeed Optim.

= 2.36.5 - 2023.04.06 =
* Update: Created the fallbacks class.
* Update: Rebuilt Google Maps script. It's now not depending on jQuery and it is minified.

= 2.35.1 - 2023.03.31 =
* Update: Minor bugfix in the file styling.

= 2.35.0 - 2023.03.30 =
* Update: Added filter g_lib_registered_blocks.

= 2.34.0 - 2023.03.24 =
* Update: Added function to remove the generator meta tags.

= 2.33.1 - 2023.03.23 =
* Update: Added fallback patch for the textfield whitespace bug.

= 2.32.5 - 2023.03.17 = 
* Bugfix: Fallback if str_contains() is not defined.

= 2.32.4 - 2023.03.13 = 
* Update: Added the Health class.

= 2.31.1 - 2023.03.09 = 
* Update: Added the SEO class witch initially adds the gital blocks content to the SEO Press analysis.

= 2.30.0 - 2023.02.22 = 
* Update: Updated the body() function with cleaner code and more classes.

= 2.29.0 - 2023.02.10 = 
* Update: Added constant for production server.

= 2.28.2 - 2023.01.27 = 
* Update: Added args to the file block.

= 2.27.0 - 2023.01.25 = 
* Update: Added NOOP callback to the Google Maps script.

= 2.26.2 - 2023.01.23 = 
* Update: Better structure for fallbacks.
* Update: Added fallback conversion to the old css variables.

= 2.25.1 - 2023.01.05 = 
* Update: Fix for the external support URL.

= 2.25.0 - 2022.12.02 = 
* Update: Changed Gibon Webb Uppsala to Gibon Webb and webb.uppsala@gibon.se to webb@gibon.se.

= 2.24.0 - 2022.11.29 = 
* Update: The Meow apps dashboard is now hidden for other users than gibonadmin.

= 2.23.0 - 2022.11.28 = 
* Update: The comment block is now hidden.

= 2.22.0 - 2022.11.04 = 
* Update: Added support for setting session cookies in set_cookie().

= 2.21.0 - 2022.11.04 = 
* Update: New modal method, get_modal() that returns the modal markup.

= 2.20.0 - 2022.10.17 = 
* Update: Changed the sass variables to the new semantic.

= 2.19.0 - 2022.10.14 = 
* Update: Removed new blocks.

= 2.18.6 - 2022.10.13 = 
* Bugfix: The file component had a faulty grid column setting.

= 2.18.5 - 2022.10.07 = 
* Update: Added function (safe_email) to safely render emails.

= 2.17.0 - 2022.10.05 = 
* Update: Raised the priority of the wp_dashboard_setup-hook that unsets the other widgets.

= 2.16.0 - 2022.09.15 = 
* Update: Added new Security class.
* Update: Added new Security function to not expose Rest API endpoint for users to not logged in users.

= 2.15.0 - 2022.09.14 = 
* Update: If the default post should be hidden, now, the option in the top menu is also removed.

= 2.14.1 - 2022.09.13 = 
* Update: Image modal min widht and min height removed.
* Update: Modal does not render a H2 if the title is empty.

= 2.13.0 - 2022.09.02 = 
* Update: Removed new blocks.

= 2.12.0 - 2022.09.01 = 
* Update: New log function log().

= 2.11.0 - 2022.08.24 = 
* Update: Updated PUC to 4.13.

= 2.10.0 - 2022.08.24 = 
* Update: The modal is now rewritten in ES6.
* Update: The modal now fires events on show and close.

= 2.9.2 - 2022.08.11 = 
* Update: The recheck CLI command is updated.

= 2.7.2 - 2022.07.01 = 
* Bugfix: The acf seamless postboxes should not have a shadow.

= 2.7.1 - 2022.06.16 = 
* Update: Added function to disable the SVG Duotone filters and global styles.
* Update: Added function to alter the image threshold.

= 2.6.4 - 2022.06.09 = 
* Bugfix: The conditions for the fallbacks for the namespace where wrongly stated.

= 2.6.3 - 2022.06.09 = 
* Bugfix: The function g_body did not pass through any arguments.

= 2.6.2 - 2022.05.31 = 
* Update: Removed the g_ prefix from functions inside of a namespace. A fallback file will be required if the gital-theme version is below 3.17.0.

= 2.5.0 - 2022.05.25 = 
* Update: Added support to WP 6.0.

= 2.4.0 - 2022.05.18 = 
* Update: Added the G_LIB_GOOGLE_MAPS_QUERY_URL constant.

= 2.3.4 - 2022.05.17 = 
* Bugfix: The modal now uses % instead of viewport values.

= 2.3.1 - 2022.05.06 = 
* Bugfix: If the modal was shown on load, it was not possible to close it, now it is.

= 2.3.0 - 2022.04.06 = 
* Update: New Image_Modal component with styling and scripts.
* Bugfix: The modal is now faster.

= 2.2.0 - 2022.02.21 = 
* Update: The managed hosting functions to control if the service is disabled is updated.

= 2.1.0 - 2022.01.27 = 
* Update: Moved to the new Gital Plugin Boilerplate.
* Update: Additional styling for WP5.9.

= 1.42.0 - 2022.01.25 = 
* Update: New function get_storage_left added to the Storage class.
* Update: New cli function storage.

= 1.41.4 - 2022.01.24 = 
* Update: Restyled the dashboard widget.

= 1.40.1 - 2022.01.24 = 
* Update: Added storage dashboard widget.

= 1.39.0 - 2022.01.20 = 
* Update: Added global function "build_attributes".

= 1.38.1 - 2022.01.05 = 
* Bugfix: The modal component had a bug in the show_on_load function.

= 1.38.0 - 2021.12.30 = 
* Update: Added support to WP 5.9.

= 1.37.0 - 2021.12.22 = 
* Update: Added support to WC 6.

= 1.36.2 - 2021.12.14 = 
* Bugfix: update_managed_hosting function now returns Success if timestamp is updated.

= 1.36.1 - 2021.12.01 = 
* Bugfix: Modal component show on load could not be closed.

= 1.36.0 - 2021.11.16 = 
* Update: Added Reply-To to the Support form.

= 1.35.0 - 2021.11.15 = 
* Update: Rename the Ninja Forms plugin in the list.

= 1.34.1 - 2021.10.26 = 
* Update: Updated server list.

= 1.33.0 - 2021.09.15 = 
* Update: Hide the NF "Append a form".

= 1.32.1 - 2021.09.14 = 
* Update: New function g_file_type() to determine what file type a file_id is.
* Update: New component Files.

= 1.31.0 - 2021.09.03 = 
* Update: Added function to enable the <style> tag in ACF-description.

= 1.30.2 - 2021.09.02 = 
* Bugfix: Clean_And_Dequeue interfered with Ninja Forms.

= 1.30.1 - 2021.08.27 = 
* Bugfix: Modal now har viewport width and height instead of right and bottom.

= 1.30.0 - 2021.08.26 = 
* Update: Increased security in functions. They now always return escaped values.

= 1.29.0 - 2021.08.25 = 
* Update: Updated all npm packages
* Bugfix: In the modal component. clode_all... changed to close_all.

= 1.28.0 - 2021.08.19 = 
* Update: 'allowed_block_types' hook has been deprecated and is now replaced with 'allowed_block_types_all' instead.

= 1.27.0 - 2021.08.18 = 
* Update: Removed security.

= 1.26.0 - 2021.08.16 = 
* Update: Added new Blocks to the clean-and-dequeue class.

= 1.25.2 =
* Update: Added new Security class.
* Update: Added new Security function to not expose Rest API to not logged in users.

= 1.24.3 =
* Update: Added new constants: G_LIB_PRODUCTION_URL, G_LIB_DEV_NAME

= 1.24.1 =
* Bugfix: Finetuning of g_image_src. 

= 1.24.0 =
* Update: g_image_src is updated to 1.1.0. The function now accepts both an int or an array of images as fallbacks. 

= 1.23.0 =
* Update: The Google Map script now has a global map variable and the function center_map is now able to update the map

= 1.22.2 =
* Bugfix: $show_on_load on modal did not started event listeners to listen for close actions

= 1.22.1 =
* Update: Updated all npm packages.
* Update: Updated to math.div in Sass instead of /.

= 1.22.0 =
* Update: Added function g_image_src().

= 1.21.0 =
* Update: Just an aesthetic update to the login page.

= 1.20.0 =
* Update: g_clean_classes with $space_prefix.

= 1.19.1 =
* Update: Added the new server IP to the allowed servers array.

= 1.19.0 =
* Update: Cleaned up all the files, classes and function.

= 1.18.0 =
* Update: Updated the login av confirm admin email styling.
* Update: Updated script handling.

= 1.17.3 =
* Update: g_clean_classes updated

= 1.17.1 =
* Update: Added styling to the Gibon Menu
* Update: Added constants for the urls

= 1.16.0 =
* Update: Added function to rename Ninja Forms to Forms with admin_styling()

= 1.15.4 =
* Update: Updated g_link_to_button with $button_tag

= 1.15.3 =
* Update: g_body now accepts additonal classes
* Update: New function g_clean_classes that cleans a string of classes

= 1.15.2 =
* Bugfix: g_link_to_button's positon dig not work

= 1.15.1 =
* Update: Added constant G_LIB_ALLOWED_SERVERS

= 1.15.0 =
* Update: Added function get_allowed_servers to Managed Hosting class

= 1.14.3 =
* Update: Added Managed Hosting class

= 1.13.3 =
* Update: Hides the "Inactivate" buttons on the Gital Plugins if the user is not gibonadmin
* Update: Removed SEOPress FAQ-block
* Update: Removed Ads from NF

= 1.13.1 =
* Update: The modal now supports "Close by Esc" and has a smoother animation
* Update: New function to hide Litespeed, Tools and Plugins

= 1.12.1 =
* Bugfix: Changed addWrapper and addButton to add_wrapper and add_button

= 1.12.0 =
* Update: Updated accordion

= 1.11.2 =
* Bugfix: Updated components to Wordpress standard

= 1.11.1 =
* Bugfix: Admin and Login class had a bug

= 1.11.0 =
* Update: The Gital Library is now cleaned and adapted to the Wordpress standard.

= 1.10.1 =
* Update: Panorama now does not render empty tags

= 1.10.0 =
* Update: $show_on_load added to modal

= 1.9.0 =
* Update: Major cleanup of classnames, constants and functions

= 1.8.5 =
* Bugfix: Fallback index on the accordion

= 1.8.4 =
* Bugfix: Updated js to the accordion

= 1.8.3 =
* Bugfix: class_item did not render on accordion

= 1.8.2 =
* Bugfix: Added the wrapper to the accordion

= 1.8.1 =
* Update: Added accordion attributes

= 1.8.0 =
* Update: Added accordion component

= 1.7.4 =
* Update: Updated Webpack and npm packages

= 1.7.3 =
* Update: G_CleanAndDequeue::remove_posts_menu had the wrong name

= 1.7.2 =
* Update: g_modal::echoModal refered to the wrong method get_modal. Should be the_modal

= 1.7.1 =
* Update: $newTab is now Bool and button variables is cleaned

= 1.7.0 =
* Update: Added wp cli command gital
* Update: Added wp cli command gital recheck

= 1.6.1 =
* Major Update: Moved several functions to classes and improved documentation

= 1.5.3 =
* Update: g_button now accepts the parameter $button_tag

= 1.5.0 =
* Update: Dequeue/g_lib_remove_default_blocks updated to match WP 5.6

= 1.4.2 =
* Bugfix: "Default turn on" constants is now fixed

= 1.4.1 =
* Update: DISABLE_SANITIZE_FILENAME are now default turned on

= 1.4.0 =
* Update: New constants DISABLE_SANITIZE_FILENAME, DISABLE_ADMIN_STYLING, DISABLE_SUPPORT_PAGE
* Update: Dequeue and Hide functions are now mainly as default turned off
* Update: New function to control the constants: g_lib_get_constants

= 1.3.4 =
* Bugfix: Changed get_template_directory() to get_template_directory_uri() so it's possible to use a custom map

= 1.3.3 =
* Update: Added css-variables fallback

= 1.3.2 =
* Update: Moved plugin-update-checker to composer

= 1.2.4 =
* Update: Added From to the headers in g_register_support_page

= 1.2.1 =
* Bugfix: Changed the e-mail on the support form at g_register_support_page

= 1.2.0 =
* Update: Added support ticket form inside of wp-admin

= 1.1.3 =
* Bugfix: Admin CSS-bug in the order list fixed

= 1.1.2 =
* Bugfix: g_lib_map_resources name error fixed

= 1.1.1 =
* Update: Cleaned up functions names

= 1.0.0 =
* Update: Added control to styles and scripts
* Update: Renamed Google Maps script

= 0.9.0 =
* Update: Buttons to support and contact Gibon added to the dashboard

= 0.8.0 =
* Update: New function closeModalFromID()

= 0.7.3 =
* Bugfix: Modal overflow scroll

= 0.7.2 =
* Bugfix: WC Buttons in admin had text-shadow

= 0.7.1 =
* Update: Readded function to remove jQuery Migrate due to WP v5.5.

= 0.6.0 =
* Update: Unsets all embed blocks from gutenberg

= 0.5.3 =
* Bugfix: Removed box-shadow and text-shadow from buttons in admin

= 0.5.2 =
* Bugfix: Not demanding gital external script anymore

= 0.5.1 =
* Updated the panorama gradient to look more smooth
* Stripped down the scss variables

= 0.5.0 =
* Updated setCookie and getCookie. Now they handle "sameSite". They are also renamed to gSetCookie and gGetCookie

= 0.4.0 =
* Removed function to remove jQuery Migrate due to WP v5.5.

= 0.3.25 =
* Made the setCookie and getCookie available in window.

= 0.3.22 =
* Bugfix Corrected fatal error in _global.php on row 98.

= 0.3.22 =
* Updated _global.php with a few more woocommerce body classes.

= 0.3.21 =
* Updated _google_maps.php with functionality to enqueue the Google Script from the theme simply by adding it to the 'Scripts' folder.

= 0.3.20 =
* Updated g_hours()

= 0.3.19 =
* Updated g_hours()

= 0.3.18 =
* wp_get_current_user()->user_login instead of wp_get_current_user()->data->user_login

= 0.3.17 =
* Bugfix

= 0.3.16 =
* Bugfix with colorpicker

= 0.3.14 =
* add_action('plugins_loaded'...) instead of add_action('init'...)

= 0.3.13 =
* Updated css for the login

= 0.3.12 =
* Composer test

= 0.3.11 =
* Added composer support

= 0.3.10 =
* Bugfix with overflow that caused the elements with 100vw to overflow.

= 0.3.9 =
* Changed author

= 0.3.8 =
* Bugfixes

= 0.3.7 =
* Bugfixes

= 0.3.6 =
* Bugfixes

= 0.3.5 =
* Bugfixes

= 0.3.4 =
* Bugfixes

= 0.3.3 =
* Bugfixes

= 0.3.2 =
* Bugfixes
* Remove comments functin

= 0.3.1 =
* Bugfixes

= 0.3.0 =
* Added updater
* Moved icons to theme
* Cut the cord to the _variables

= 0.2.5 =
* Added function g_hours

= 0.2.4 =
* Updated panorama css
* Bugfix: g_link_to_button did always render new tab link

= 0.2.3 =
* Disabled new blocks from WP 5.5
* Disabled new block patterns from WP 5.5

= 0.2.2 =
* Added function g_w()

= 0.2.1 =
* Moved icons from theme to library
* Updated the marker for gm
* Updated the login style for screens with low height

= 0.2.0 =
* Added function to modal: showModalFromID
* Fixed bugs on css from 0.1.9
* Added onClick on button class

= 0.1.9 =
* Updated login and admin

= 0.1.8 =
* Added sanitize filenames

= 0.1.7 =
* Added more body types to g-body()

= 0.1.6 =
* New module for button

= 0.1.5 =
* Added Internationalization
* Lifted the backend experience
* Added dashboard widget
* Modal is now class

= 0.1.4 =
* Added function g_remove_default_blocks()

= 0.1.3 =
* Added function g_modal()

= 0.1.2 =
* Moved CSS for Login and Admin to Sass-files

= 0.1.1 =
* Added function g_taxonomies()

= 0.1.0 =
* Added modules

= 0.0.9 =
* Added global script
* Added cookie script

= 0.0.8 =
* Added dequeueu

= 0.0.7 =
* Cleaned up files

= 0.0.6 =
* Added function g_body()
* Added function g_phone()
* Added function g_resource()
* Added function g_zip()

= 0.0.5 =
* Styling for Admin

= 0.0.4 =
* Added more constants
* Added Google Maps

= 0.0.3 =
* Added constants and moved logo to plugin

= 0.0.2 =
* Added Login styling
* Added function Icon fetcher
* Added function WC Active
