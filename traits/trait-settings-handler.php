<?php
/**
 * Settings Handler
 *
 * @package Gital Library
 */

namespace gital_library;

/**
 * Settings Handler
 */
trait Settings_Handler {
	/**
	 * Settings Handler
	 *
	 * @var array $settings.
	 */
	protected $settings;

	/**
	 * Check if key exists
	 *
	 * @param string $key Key to check.
	 *
	 * @return bool True if key exists, false if not.
	 *
	 * @author Gustav Gesar <gustav.gesar@gibon.se>
	 *
	 * @version 1.0.0
	 */
	public function exists( string $key ) {
		return isset( $this->settings[ $key ] );
	}

	/**
	 * Get value
	 *
	 * @param string $key Key to get.
	 * @param mixed  $fallback Default value if key does not exist.
	 *
	 * @return mixed Value of key or fallback value.
	 *
	 * @author Gustav Gesar <gustav.gesar@gibon.se>
	 *
	 * @version 1.0.0
	 */
	public function get( string $key, $fallback = null ) {
		return $this->settings[ $key ] ?? $fallback;
	}

	/**
	 * Set value
	 *
	 * @param string $key Key to set.
	 * @param mixed  $value Value to set.
	 *
	 * @return void
	 *
	 * @author Gustav Gesar <gustav.gesar@gibon.se>
	 *
	 * @version 1.0.0
	 */
	public function set( string $key, $value ): void {
		$this->settings[ $key ] = $value;
	}
}
