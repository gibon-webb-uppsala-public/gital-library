<?php
use gital_library\Settings;

$settings = Settings::get_instance();
?>
<style type="text/css">
#wpadminbar #wp-admin-bar-wp-logo>.ab-item .ab-icon:before {
	background-image: url('<?php echo $settings->get( 'url_gibon_icon' ); ?>') !important;
	background-position: center;
	background-size: contain;
	background-repeat: no-repeat;
	color: rgba(0, 0, 0, 0);
}

#wpadminbar #wp-admin-bar-wp-logo.hover>.ab-item .ab-icon {
	background-position: center;
}
</style>
