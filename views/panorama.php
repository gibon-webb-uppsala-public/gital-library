<?php
/**
 * Panorama template
 *
 * @package Gital Library
 */

namespace gital_library;

?>
<div<?php echo build_attributes( $attributes, true ); ?>>
	<div class="g-panorama__content g-margin-ver">
		<?php if ( ! empty( $subtitle ) ) { ?>
			<h3 class="g-panorama__subtitle"><?php echo esc_html( $subtitle ); ?></h3>
			<?php
		}
		if ( ! empty( $title ) ) {
			?>
			<h1 class="g-panorama__title"><?php echo esc_html( $title ); ?></h1>
			<?php
		}
		if ( ! empty( $additional ) ) {
			?>
			<div class="g-panorama__additional"><?php echo esc_html( $additional ); ?></div>
		<?php } ?>
	</div>
</div>
