<?php
/**
 * Support form
 *
 * @package Gital Library
 */

namespace gital_library;

$settings               = Settings::get_instance();
$current_logged_in_user = wp_get_current_user();

?>
<div class="wrap">
	<h1 class="wp-heading-inline"><?php echo __( 'Support', 'gital-library' ); ?></h1>
	<?php
	$managed_hosting = Managed_Hosting::get_instance();
	if ( $managed_hosting->control_if_managed() ) {
		?>
		<p><?php echo __( 'With Managed Hosting from Gibon Webb, support is just around the corner.', 'gital-library' ); ?></p>
		<?php
	} else {
		echo $managed_hosting->render_notice();
	}
	?>
	<p><?php echo __( 'For support, contact Gibon Webb at:', 'gital-library' ); ?></p>
	<p>
		<?php echo __( 'E-Mail:', 'gital-library' ) . ' ' . $settings->get( 'contact_email' ); ?>
		<br>
		<?php echo __( 'Phone:', 'gital-library' ) . ' ' . $settings->get( 'contact_phone' ); ?>
	</p>
	<?php
	if ( isset( $_GET['success'] ) ) {
		if ( 'true' === $_GET['success'] ) {
			echo '<p>' . __( "Thank you! Your support ticket has been posted. We'll get back to you as soon as possible", 'gital-library' ) . '</p>';
		} else {
			echo '<p>' . __( 'Something went wrong and your support ticket has not been sent. Please contact us through mail or phone instead. You can also contact us through ', 'gital-library' ) . '<a href="https://webbuppsala.gibon.se/support/?utm_source=' . $settings->get( 'referral_url' ) . '&utm_medium=referral">' . __( 'our own support form.', 'gital-library' ) . '</a></p>';
		}
	} else {
		?>
		<div class="form-wrapper">
			<p><?php echo __( 'Or, just fill out the form below.', 'gital-library' ); ?></p>
			<form name="support" method="POST" action="">
				<table class="form-table" role="presentation">
					<tbody>
						<tr>
							<th scope="row"><?php echo __( 'Name', 'gital-library' ); ?></th>
							<td><input type="text" name="name" value="<?php echo $current_logged_in_user->user_nicename; ?>" required></td>
						</tr>
						<tr>
							<th scope="row"><?php echo __( 'E-Mail', 'gital-library' ); ?></th>
							<td><input type="email" name="email" value="<?php echo $current_logged_in_user->user_email; ?>" required></td>
						</tr>
						<tr>
							<th scope="row"><?php echo __( 'What can we help you with', 'gital-library' ); ?></th>
							<td><textarea name="description" rows="5" cols="30" required></textarea></td>
						</tr>
						<tr>
							<th scope="row"><?php echo __( 'How do you want us to handle this matter', 'gital-library' ); ?></th>
							<td>
								<select name="handle">
									<option selected="selected"><?php echo __( 'Get started as soon as possible', 'gital-library' ); ?></option>
									<option><?php echo __( 'Get started right away, but contact me if you think it will take more than 1 hour', 'gital-library' ); ?></option>
									<option><?php echo __( 'I want an estimate first', 'gital-library' ); ?></option>
								</select>
							</td>
						</tr>
					</tbody>
				</table>
				<p class="submit"><input type="submit" name="support" class="button button-primary" value="<?php echo __( 'Send the ticket', 'gital-library' ); ?>"></p>
			</form>
		</div>
		<?php
	}
	?>
</div>
