<?php
/**
 * Plugin Name: Gital Library
 * Author: Gibon Webb
 * Version: 3.19.0
 *
 * Author URI: https://gibon.se/
 * Description: The Gital Library is made with passion in Uppsala, Sweden. The plugin is adding and maintaining the code library for the Gital products. If you'd like support, please contact us at webb@gibon.se.
 *
 * @package Gital Library
 */

namespace gital_library;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Load textdomain and languages
 *
 * @author Gustav Gesar <gustav.gesar@gibon.se>
 */
function textdomain() {
	load_plugin_textdomain( 'gital-library', false, dirname( plugin_basename( __FILE__ ) ) . '/languages' );
}
add_action( 'plugins_loaded', 'gital_library\textdomain' );

// Autoloader.
require plugin_dir_path( __FILE__ ) . 'vendor/autoload.php';

// Load settings.
$settings = Settings::get_instance();

// PHP Polyfills.
require_once $settings->get( 'path_vendor' ) . 'symfony/polyfill-php80/bootstrap.php';
require_once $settings->get( 'path_vendor' ) . 'symfony/polyfill-php81/bootstrap.php';
require_once $settings->get( 'path_vendor' ) . 'symfony/polyfill-php82/bootstrap.php';

// Init the updater.
use YahnisElsts\PluginUpdateChecker\v5\PucFactory;
$myUpdateChecker = PucFactory::buildUpdateChecker(
	'https://packages.gital.se/wordpress/gital-library.json',
	__FILE__,
	'gital-library'
);

/**
 * Enqueue public scripts
 *
 * @author Gustav Gesar <gustav.gesar@gibon.se>
 */
function recourses() {
	$settings = Settings::get_instance();

	wp_register_style( 'g_lib_style', $settings->get( 'url_assets' ) . '/styles/gital.library.min.css', array(), '1.3.0' );
	wp_enqueue_style( 'g_lib_style' );

	wp_register_script( 'g_lib_script', $settings->get( 'url_assets' ) . '/scripts/gital.library.min.js', array( 'jquery' ), '1.2.0', true );
	wp_enqueue_script( 'g_lib_script' );
}
add_action( 'wp_enqueue_scripts', 'gital_library\recourses' );

// Functions.
require_once $settings->get( 'path_functions' ) . 'constants.php';
require_once $settings->get( 'path_functions' ) . 'echo-with-esi.php';
require_once $settings->get( 'path_functions' ) . 'global.php';
require_once $settings->get( 'path_functions' ) . 'is-super-admin.php';
require_once $settings->get( 'path_functions' ) . 'extended-has-block.php';

// Classes.
Admin_And_Login::get_instance();
Cache_Handling::get_instance();
Optimize::get_instance();
Dashboard::get_instance();
Duplicate_Post::get_instance();
Fallbacks::get_instance();
Health::get_instance();
Patches::get_instance();
Security::get_instance();
SEO::get_instance();

if ( defined( 'GOOGLE_MAPS_API' ) && ! empty( GOOGLE_MAPS_API ) ) {
	Google_Maps::get_instance();
}

if ( $settings->get( 'disable_support_page' ) ) {
	Support::get_instance();
}

/**
 * Registers the CLI commands.
 *
 * @author Gustav Gesar < gustav . gesar@gibon . se >
 *
 * @version 1.0.0
 * @since  1.7.0
 */
function register_cli() {
	\WP_CLI::add_command( 'gital', 'gital_library\Cli' );
}
add_action( 'cli_init', 'gital_library\register_cli' );
