<?php
/**
 * Singleton
 *
 * @package Gital Library
 */

namespace gital_library;

/**
 * Singleton
 */
abstract class Singleton {

	/**
	 * Instance.
	 *
	 * @var array $instance
	 */
	protected static $instance = array();

	/**
	 * Get the current instance.
	 *
	 * @author Gustav Gesar <gustav.gesar@gibon.se>
	 *
	 * @version 1.0.0
	 */
	final public static function get_instance() {
		$class = get_called_class();
		if ( ! isset( self::$instance[ $class ] ) || ! self::$instance[ $class ] instanceof $class ) {
			self::$instance[ $class ] = new static();
		}
		return static::$instance[ $class ];
	}

	final private function __construct() {
		$this->init();
	}

	/**
	 * Prevent instantiation.
	 *
	 * @author Gustav Gesar <gustav.gesar@gibon.se>
	 *
	 * @version 1.0.0
	 */
	private function __clone() {
	}

	/**
	 * Fallback if no init is defined
	 *
	 * @author Gustav Gesar <gustav.gesar@gibon.se>
	 *
	 * @version 1.0.0
	 */
	protected function init() {
	}
}
