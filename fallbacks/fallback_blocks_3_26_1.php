<?php
/**
 * Image modal component
 *
 * @package Gital Library
 */

namespace gital_library;

/**
 * Image Modal
 * Author: Gustav Gesar
 *
 * @version 1.2.0
 * @since 2.3.0
 */
class Image_Modal {
	/**
	 * The id of the image modal
	 *
	 * @var string
	 */
	protected $id;

	/**
	 * Image modal
	 *
	 * This component will return the id of the modal and can then be used by the js function load_image_modal();
	 *
	 * @param string $additional_classes Additional classes.
	 *
	 * @author Gustav Gesar <gustav.gesar@gibon.se>
	 *
	 * @version 1.1.0
	 * @since 1.0.0
	 */
	public function __construct( $additional_classes = '' ) {
		$this->id = 'g-image-modal--' . wp_rand( 1000, 9999 );

		$content  = '<video class="g-image-modal__video g-media-modal__item g-media-modal__item--video" style="display:none;"></video>';
		$content .= '<img class="g-image-modal__image g-media-modal__item g-media-modal__item--image" src="">';
		$content .= '<div class="g-image-modal__iframe g-media-modal__item g-media-modal__item--iframe"><iframe allowfullscreen allowtransparency allow="autoplay" style="display:none;"></iframe></div>';

		$modal = new Modal( $content, $this->id, clean_classes( 'g-image-modal g-media-modal ' . $additional_classes, true ), '', true, true, false );
		$modal->the_modal_to_footer();
	}

	/**
	 * Returns the modal id
	 *
	 * @return $this->id The ID of the modal.
	 *
	 * @author Gustav Gesar <gustav.gesar@gibon.se>
	 *
	 * @version 1.0.0
	 * @since 1.0.0
	 */
	public function get_id() {
		return $this->id;
	}
}
