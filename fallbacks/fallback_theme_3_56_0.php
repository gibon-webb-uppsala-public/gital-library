<?php
/**
 * Fallback functions to deprecated functions inside the namespace
 *
 * @package Gital Library
 */

namespace gital_library;

$settings = Settings::get_instance();

define( 'G_LIB_PRODUCTION_SERVER', $settings->get( 'production_server' ) );
define( 'G_LIB_ALLOWED_SERVERS', $settings->get( 'allowed_servers' ) );
define( 'G_LIB_SUPER_ADMINS', $settings->get( 'super_admins' ) );
define( 'G_LIB_DEV_NAME', $settings->get( 'dev_name' ) );
define( 'G_LIB_HOSTING_URL', $settings->get( 'hosting_url' ) );
define( 'G_LIB_PRODUCTION_URL', $settings->get( 'production_url' ) );
define( 'G_LIB_REFERRAL_URL', $settings->get( 'referral_url' ) );
define( 'G_LIB_SUPPORT_URL', $settings->get( 'support_url' ) );
define( 'G_LIB_EXTERNAL_SUPPORT_URL', $settings->get( 'external_support_url' ) );
define( 'G_LIB_CONTACT_URL', $settings->get( 'contact_url' ) );
define( 'G_LIB_CONTACT_PHONE', $settings->get( 'contact_phone' ) );
define( 'G_LIB_CONTACT_EMAIL', $settings->get( 'contact_email' ) );
define( 'G_LIB_CONTACT_UPPSALA_URL', $settings->get( 'contact_uppsala_url' ) );
define( 'G_LIB_GOOGLE_MAPS_QUERY_URL', $settings->get( 'google_maps_query_url' ) );

define( 'G_LIB_ROOT', $settings->get( 'url_root' ) );
define( 'G_LIB_ASSETS', $settings->get( 'url_assets' ) );
define( 'G_LIB_FALLBACKS', $settings->get( 'url_fallbacks' ) );
define( 'G_LIB_PATCHES', $settings->get( 'url_patches' ) );
define( 'G_LIB_RESOURCES', $settings->get( 'url_resources' ) );
define( 'G_LIB_GIBON_LOGO', $settings->get( 'url_gibon_logo' ) );
define( 'G_LIB_GIBON_ICON', $settings->get( 'url_gibon_icon' ) );
define( 'G_LIB_GIBON_WALLPAPER', $settings->get( 'url_gibon_wallpaper' ) );
define( 'G_LIB_ROOT_PATH', $settings->get( 'path_root' ) );
define( 'G_LIB_ASSETS_PATH', $settings->get( 'path_assets' ) );
define( 'G_LIB_FUNCTIONS_PATH', $settings->get( 'path_functions' ) );
define( 'G_LIB_FALLBACKS_PATH', $settings->get( 'path_fallbacks' ) );
define( 'G_LIB_CLASSES_PATH', $settings->get( 'path_classes' ) );
define( 'G_LIB_VIEWS_PATH', $settings->get( 'path_views' ) );
define( 'G_LIB_VENDOR_PATH', $settings->get( 'path_vendor' ) );
define( 'G_LIB_COMPONENTS_PATH', $settings->get( 'path_components' ) );
