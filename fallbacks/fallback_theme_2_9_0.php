<?php
/**
 * Fallback functions to deprecated functions
 *
 * @package Gital Library
 */

if ( ! function_exists( 'g_panorama' ) ) {
	/**
	 * Fallback to g_panorama
	 *
	 * @deprecated 1.6.0
	 *
	 * @author Gustav Gesar <gustav.gesar@gibon.se>
	 */
	function g_panorama( ...$args ) {
		new gital_library\Panorama( ...$args );
	}
}

if ( ! function_exists( 'g_is_wc_activated' ) ) {
	/**
	 * Fallback to g_is_wc_activated
	 *
	 * @deprecated 1.11.0
	 *
	 * @author Gustav Gesar <gustav.gesar@gibon.se>
	 */
	function g_is_wc_activated() {
		return gital_library\is_wc_activated();
	}
}

if ( ! function_exists( 'g_icon' ) ) {
	/**
	 * Fallback to g_icon
	 *
	 * @deprecated 1.11.0
	 *
	 * @author Gustav Gesar <gustav.gesar@gibon.se>
	 */
	function g_icon( ...$args ) {
		return gital_library\icon( ...$args );
	}
}

if ( ! function_exists( 'g_body()' ) ) {
	/**
	 * Fallback to g_body()
	 *
	 * @deprecated 1.11.0
	 *
	 * @author Gustav Gesar <gustav.gesar@gibon.se>
	 */
	function g_body() {
		return gital_library\body();
	}
}

if ( ! function_exists( 'g_w()' ) ) {
	/**
	 * Fallback to g_w()
	 *
	 * @deprecated 1.11.0
	 *
	 * @author Gustav Gesar <gustav.gesar@gibon.se>
	 */
	function g_w( ...$args ) {
		return gital_library\w( ...$args );
	}
}

if ( ! function_exists( 'g_link_to_button()' ) ) {
	/**
	 * Fallback to g_link_to_button()
	 *
	 * @deprecated 1.11.0
	 *
	 * @author Gustav Gesar <gustav.gesar@gibon.se>
	 */
	function g_link_to_button( ...$args ) {
		return gital_library\link_to_button( ...$args );
	}
}

if ( ! function_exists( 'g_resource()' ) ) {
	/**
	 * Fallback to g_resource()
	 *
	 * @deprecated 1.11.0
	 *
	 * @author Gustav Gesar <gustav.gesar@gibon.se>
	 */
	function g_resource( ...$args ) {
		return gital_library\resource( ...$args );
	}
}

if ( ! function_exists( 'g_phone()' ) ) {
	/**
	 * Fallback to g_phone()
	 *
	 * @deprecated 1.11.0
	 *
	 * @author Gustav Gesar <gustav.gesar@gibon.se>
	 */
	function g_phone( ...$args ) {
		return gital_library\phone( ...$args );
	}
}

if ( ! function_exists( 'g_zip()' ) ) {
	/**
	 * Fallback to g_zip()
	 *
	 * @deprecated 1.11.0
	 *
	 * @author Gustav Gesar <gustav.gesar@gibon.se>
	 */
	function g_zip( ...$args ) {
		return gital_library\zip( ...$args );
	}
}

if ( ! function_exists( 'g_log()' ) ) {
	/**
	 * Fallback to g_log()
	 *
	 * @deprecated 1.11.0
	 *
	 * @author Gustav Gesar <gustav.gesar@gibon.se>
	 */
	function g_log( ...$args ) {
		return gital_library\log( ...$args );
	}
}

if ( ! function_exists( 'g_taxonomies()' ) ) {
	/**
	 * Fallback to g_taxonomies()
	 *
	 * @deprecated 1.11.0
	 *
	 * @author Gustav Gesar <gustav.gesar@gibon.se>
	 */
	function g_taxonomies( ...$args ) {
		return gital_library\taxonomies( ...$args );
	}
}

if ( ! function_exists( 'g_hours()' ) ) {
	/**
	 * Fallback to g_hours()
	 *
	 * @deprecated 1.11.0
	 *
	 * @author Gustav Gesar <gustav.gesar@gibon.se>
	 */
	function g_hours( ...$args ) {
		return gital_library\hours( ...$args );
	}
}
