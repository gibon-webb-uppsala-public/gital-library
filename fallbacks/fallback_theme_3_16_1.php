<?php
/**
 * Fallback functions to deprecated functions inside the namespace
 *
 * @package Gital Library
 */

namespace gital_library;

if ( ! function_exists( 'gital_library\g_is_wc_activated' ) ) {
	/**
	 * Fallback to g_is_wc_activated
	 *
	 * @deprecated 2.6.0
	 *
	 * @author Gustav Gesar <gustav.gesar@gibon.se>
	 */
	function g_is_wc_activated() {
		return is_wc_activated();
	}
}

if ( ! function_exists( 'gital_library\g_icon' ) ) {
	/**
	 * Fallback to g_icon
	 *
	 * @deprecated 2.6.0
	 *
	 * @author Gustav Gesar <gustav.gesar@gibon.se>
	 */
	function g_icon( ...$args ) {
		return icon( ...$args );
	}
}

if ( ! function_exists( 'gital_library\g_body()' ) ) {
	/**
	 * Fallback to g_body()
	 *
	 * @deprecated 2.6.0
	 *
	 * @author Gustav Gesar <gustav.gesar@gibon.se>
	 */
	function g_body( ...$args ) {
		return body( ...$args );
	}
}

if ( ! function_exists( 'gital_library\g_w()' ) ) {
	/**
	 * Fallback to g_w()
	 *
	 * @deprecated 2.6.0
	 *
	 * @author Gustav Gesar <gustav.gesar@gibon.se>
	 */
	function g_w( ...$args ) {
		return w( ...$args );
	}
}

if ( ! function_exists( 'gital_library\g_link_to_button()' ) ) {
	/**
	 * Fallback to g_link_to_button()
	 *
	 * @deprecated 2.6.0
	 *
	 * @author Gustav Gesar <gustav.gesar@gibon.se>
	 */
	function g_link_to_button( ...$args ) {
		return link_to_button( ...$args );
	}
}

if ( ! function_exists( 'gital_library\g_resource()' ) ) {
	/**
	 * Fallback to g_resource()
	 *
	 * @deprecated 2.6.0
	 *
	 * @author Gustav Gesar <gustav.gesar@gibon.se>
	 */
	function g_resource( ...$args ) {
		return resource( ...$args );
	}
}

if ( ! function_exists( 'gital_library\g_phone()' ) ) {
	/**
	 * Fallback to g_phone()
	 *
	 * @deprecated 2.6.0
	 *
	 * @author Gustav Gesar <gustav.gesar@gibon.se>
	 */
	function g_phone( ...$args ) {
		return phone( ...$args );
	}
}

if ( ! function_exists( 'gital_library\g_zip()' ) ) {
	/**
	 * Fallback to g_zip()
	 *
	 * @deprecated 2.6.0
	 *
	 * @author Gustav Gesar <gustav.gesar@gibon.se>
	 */
	function g_zip( ...$args ) {
		return zip( ...$args );
	}
}

if ( ! function_exists( 'gital_library\g_log()' ) ) {
	/**
	 * Fallback to g_log()
	 *
	 * @deprecated 2.6.0
	 *
	 * @author Gustav Gesar <gustav.gesar@gibon.se>
	 */
	function g_log( ...$args ) {
		return log( ...$args );
	}
}

if ( ! function_exists( 'gital_library\g_taxonomies()' ) ) {
	/**
	 * Fallback to g_taxonomies()
	 *
	 * @deprecated 2.6.0
	 *
	 * @author Gustav Gesar <gustav.gesar@gibon.se>
	 */
	function g_taxonomies( ...$args ) {
		return taxonomies( ...$args );
	}
}

if ( ! function_exists( 'gital_library\g_hours()' ) ) {
	/**
	 * Fallback to g_hours()
	 *
	 * @deprecated 2.6.0
	 *
	 * @author Gustav Gesar <gustav.gesar@gibon.se>
	 */
	function g_hours( ...$args ) {
		return hours( ...$args );
	}
}

if ( ! function_exists( 'gital_library\g_image_src()' ) ) {
	/**
	 * Fallback to g_image_src()
	 *
	 * @deprecated 2.6.0
	 *
	 * @author Gustav Gesar <gustav.gesar@gibon.se>
	 */
	function g_image_src( ...$args ) {
		return image_src( ...$args );
	}
}

if ( ! function_exists( 'gital_library\g_file_type()' ) ) {
	/**
	 * Fallback to g_file_type()
	 *
	 * @deprecated 2.6.0
	 *
	 * @author Gustav Gesar <gustav.gesar@gibon.se>
	 */
	function g_file_type( ...$args ) {
		return file_type( ...$args );
	}
}

if ( ! function_exists( 'gital_library\g_build_attributes()' ) ) {
	/**
	 * Fallback to g_build_attributes()
	 *
	 * @deprecated 2.6.0
	 *
	 * @author Gustav Gesar <gustav.gesar@gibon.se>
	 */
	function g_build_attributes( ...$args ) {
		return build_attributes( ...$args );
	}
}

if ( ! function_exists( 'gital_library\g_clean_classes()' ) ) {
	/**
	 * Fallback to g_clean_classes()
	 *
	 * @deprecated 2.6.0
	 *
	 * @author Gustav Gesar <gustav.gesar@gibon.se>
	 */
	function g_clean_classes( ...$args ) {
		return clean_classes( ...$args );
	}
}
