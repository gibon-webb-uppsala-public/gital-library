<?php
/**
 * Fallback functions to deprecated functions
 *
 * @package Gital Library
 */

class_alias( 'gital_library\Button', 'g_button' );
class_alias( 'gital_library\Modal', 'g_modal' );
class_alias( 'gital_library\Panorama', 'G_Panorama' );
